from datetime import datetime

from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

TRIES = 3


class InfluxInterface:
    def __init__(self, host, port, token, org, buck_name):
        self._db = InfluxDBClient(f"http://{host}:{port}", token, buck_name)
        self._write_api = self._db.write_api(write_options=SYNCHRONOUS)
        self._org = org
        self._buck_name = buck_name

    def insert_point(self, measurement, tags, fields):
        point = Point(measurement) \
            .tag(*tags) \
            .field(*fields) \
            .time(datetime.utcnow(), WritePrecision.NS)

        self._write_api.write(self._buck_name, self._org, point)

    # def query(self, measurement, tags={}):
    #     data = self._db.query(f"select * from {measurement}")
    #     points = list(data.get_points(measurement=measurement, tags=tags))
    #     return points


if __name__ == "__main__":
    import random

    print(f"Staring influx db test")

    db = InfluxInterface('192.168.1.89', 8086, 'grafana', 'grafana_password', 'test_luminance')
    # for i in range(10):
    #     db.insert_point("luminance", {"device": "meter", "location": "home"}, {"luminance": random.randint(0, 100)})

    data = db.query("luminance")
    print(data)
    print("end!")
