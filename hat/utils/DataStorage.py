"""
# Filename:     DataStorage.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   26/04/2021
# Last Update:  26/04/2021
#
# Version:      1.0
# Filetype:     
# Description:
# STATUS:       Stable
# Limitation:   None
"""
import os
import time
import logging
import pandas as pd

LOG_FILENAME = 'data_storage.txt'

log = logging.getLogger(__name__)
stream_handler = logging.StreamHandler()
stream_handler.setLevel("DEBUG")
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
stream_handler.setFormatter(formatter)
# add ch to logger
log.addHandler(stream_handler)

log.setLevel("DEBUG")


class DataStorage:
    ROBOT_LIBRARY_SCOPE = 'SUITE'

    def __init__(self):
        log.info("Creating Datastorage")

    def open(self, filename, directory=".", timestamp_fmt="%Y%m%d_%H%M_", save_file=True):
        log.info(f"Opening data storage with filename: {filename}, id{id(self)}")
        if timestamp_fmt is not None:
            path, name = os.path.split(filename)
            filename = os.path.join(path, time.strftime(timestamp_fmt) + name)

        if not os.path.exists(directory):
            os.makedirs(directory)

        self._filename = os.path.join(directory, filename)
        self._data = pd.DataFrame()
        self._index = 0
        self._save_file = save_file

    def start_next_test(self, print_last=True):
        if print_last:
            print(self._data.loc[self._index])

        self._index += 1

        if self._save_file:
            try:
                self._data.to_excel(self._filename)
            except Exception as e:
                log.exception("Could not save intermediate file, please check if open", e)

    def add_item(self, key, value, *args, new_line=False):
        """
        Add item
        """
        # log.info(f"data_storage id: {id(self)}")
        self[key] = value

        if len(args) % 2 != 0:
            raise Exception("Arguments need to be pair")

        for index in range(0, len(args), 2):
            self[args[index]] = args[index + 1]

        if new_line:
            self.start_next_test()

    def __setitem__(self, key, value):
        if isinstance(value, pd.Series):
            self._data[key] = value
        else:
            self._data.loc[self._index, key] = value

    def __getitem__(self, args):
        return self._data[args]

    def __str__(self):
        return str(self._data)

    def last_meas_str(self):
        return str(self._data.loc[self._index])

    def save(self, filename=None):
        if filename is None:
            filename = self._filename
        dirname, filename_part = os.path.split(filename)

        if not os.path.exists(dirname):
            os.mkdir(dirname)
        self._data.to_excel(filename)

    def close(self):
        log.info("Closing data storage")
        if self._save_file:
            self.save(self._filename)
