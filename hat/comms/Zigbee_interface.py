import time
import re
from hat.utils.utils import retry
from telnetlib import Telnet

import logging

log = logging.getLogger("Zigbee interface")
handler = logging.StreamHandler()
handler.setLevel("DEBUG")
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
handler.setFormatter(formatter)

# add ch to logger
log.addHandler(handler)

log.setLevel("DEBUG")

SLEEP_TIME = 0.5  # time to wait between commands

DEFAULT_TIMEOUT = 10  # default time for timeout
SLEEP_TIME = 0.1  # sleep between reads
N_RETRIES = 3

class SshClient():
    def __init__(self, hostname, port, user, password, prompt, timeout=DEFAULT_TIMEOUT):
        from paramiko import SSHClient, AutoAddPolicy  # workarround for opentap. needs improvement

        # store parameters
        self._hostname = hostname
        self._port = port
        self._prompt = prompt
        self._timeout = timeout

        # create client
        self._intf = SSHClient()
        self._intf.set_missing_host_key_policy(AutoAddPolicy())
        self._intf.connect(hostname, port, user, password)

        # shell
        self._shell = self._intf.invoke_shell()

        time.sleep(3)  # wait a bit for connection shell

        # receive connection data
        data = self.read_all()
        print(f"Data: {data}")

    def set_prompt(self, prompt):
        self._prompt = prompt

    def read_all(self):
        data = ""
        while self._shell.recv_ready():
            data += self._shell.recv(1024).decode("ascii")
        return data

    def read_until(self, command="", raise_exception=False):
        prompt_found = False
        data = ""
        start_time = time.time()
        while time.time() - start_time < self._timeout:
            data += self.read_all()
            # command below search and tried to remove prompt
            data, replace = re.subn(self._prompt, "", data)  # prompt found
            if replace:
                prompt_found = True
                break
            time.sleep(SLEEP_TIME)

        if not prompt_found:
            if raise_exception:
                raise Exception(f"No prompt found for command: {command} in response data {data}")
            else:
                print(f"No prompt found for command: {command} in response data {data}")

        # TODO: this should only replace command if it found on the beginning
        # clear command and new lines
        data = data.replace(command, "").strip()
        return data

    def write(self, command):
        res = self._shell.send(command)

    @retry(Exception, 4, 2, 1)
    def exec_command(self, command, raise_exception=True):
        self.write(command + ("\n" if command[-1] != "\n" else ""))
        data = self.read_until(command, raise_exception)

        return data

    def __del__(self):
        self._intf.close()

class TelnetClient():
    def __init__(self, hostname, port, user, password, prompt, timeout=DEFAULT_TIMEOUT, retries=N_RETRIES):
        # store parameters
        self._hostname = hostname
        self._port = port
        self._prompt = prompt
        self._timeout = timeout

        for i in range(N_RETRIES):
            try:
                # create client
                self._intf = Telnet(hostname, port=port, timeout=timeout)

                time.sleep(1)  # wait a bit for connection shell
                break
            except Exception as e:
                print(f"Exception while connection to {hostname}, message: {e}")
                time.sleep(3)

        # receive connection data
        data = self.read_all()
        print(f"Data: {data}")

    def set_prompt(self, prompt):
        self._prompt = prompt

    def read_all(self):
        data = self._intf.read_eager().decode("ascii")
        return data

    def read_until(self, command="", raise_exception=False):
        prompt_found = False
        data = ""
        start_time = time.time()
        while time.time() - start_time < self._timeout:
            data += self.read_all()
            # command below search and tried to remove prompt
            match = re.search(self._prompt, data)  # prompt found
            if match:
                prompt_found = True
                break
            time.sleep(SLEEP_TIME)

        if not prompt_found:
            if raise_exception:
                raise Exception(f"No prompt found for command: {command} in response data {data}")
            else:
                print(f"No prompt found for command: {command} in response data {data}")

        # TODO: this should only replace command if it found on the beginning
        # clear command and new lines
        # data = data.replace(command, "").strip()
        return data

    def write(self, command):
        res = self._intf.write(command.encode("ascii"))

    @retry(Exception, 4, 2, 1)
    def exec_command(self, command, raise_exception=True):
        self.write(command + ("\n" if command[-1] != "\n" else ""))
        data = self.read_until(command, raise_exception)

        return data

    def __del__(self):
        self._intf.close()



PYTHON_CMD = "py OW3_CLI.py"


class SecoInterfaceTelnet(TelnetClient):
    def __init__(self, hostname, port=23, user="root", password="nightshift", prompt=">", timeout=DEFAULT_TIMEOUT):
        super().__init__(hostname, port, user, password, prompt, timeout)
        self.set_prompt("\)>")
        res = self.exec_command(PYTHON_CMD)

        if "Starting recieve thread" not in res or "Starting transmit thread" not in res:
            raise Exception("Could not open transmit and receive threads")

        # log.info(f"Connected to Seco on {hostname}")

    def get_nodes(self):
        response = self.exec_command("!nodes")

        match = re.search(r"-+\r\n(.*)OW3", response, re.DOTALL)
        if not match:
            raise ValueError("Not possible to find nodes")

        node_matchs = re.finditer(r"(.{20})\s(.{12})\s\[(.{4})\]!\s\s\[(.{23})\]!\s\s(.+)$", match.group(1),
                                  re.MULTILINE)
        if not node_matchs:
            raise ValueError("Not possible to match nodes")

        nodes = []
        for node_match in node_matchs:
            node = {
                "label": node_match.group(1).strip(),
                "type": node_match.group(2).strip(),
                "short_address": node_match.group(3),
                "extended": node_match.group(4),
                "hops": int(node_match.group(5))
            }
            nodes.append(node)

        return nodes

    def set_node(self, address, check_presence=True):

        if check_presence:
            nodes = self.get_nodes()
            found = False
            for node in nodes:
                if address.upper() == node["extended"].upper():
                    found = True
                    break
            if not found:
                raise Exception(f"Node [{address}] not found in {nodes}")

        response = self.exec_command(f"!a{address}")
        return response

    def execute_command(self, command, address=None, check_presence=False):
        if address is not None:
            self.set_node(address, check_presence)

        response = self.exec_command(command)
        response += self.read_until()

        # match when there is infomation about the node that send data
        response_match = re.search(r"\[([\w:]+)\]!\s\(.*\)\s+(.+)\s+\[CLI\]>", response, re.DOTALL)
        if not response_match:
            # try different match, when there is no information about the node that send back the data
            response_match = re.search(r"\[([\w:]+)\]!.*?\n(.*)\s+\[CLI\]>" , response, re.DOTALL)
            if not response_match:
                raise Exception(f"Not match on response {response} for command {command}")
        node = response_match.group(1)
        cli_response = response_match.group(2).strip()
        return cli_response

    def __del__(self):
        try:
            print("Closing")
            self.write("!exit\n")
            time.sleep(0.2)
            self.write("quit\n")
            super().__del__()
        except:
            pass



class SecoInterfaceSSH(SshClient):
    def __init__(self, hostname, port=22, user="root", password="nightshift", prompt=">", timeout=DEFAULT_TIMEOUT):
        super().__init__(hostname, port, user, password, prompt, timeout)
        self.set_prompt("\)>")
        res = self.exec_command(PYTHON_CMD)

        if "Starting recieve thread" not in res or "Starting transmit thread" not in res:
            raise Exception("Could not open transmit and receive threads")

        # log.info(f"Connected to Seco on {hostname}")

    def get_nodes(self):
        response = self.exec_command("!nodes")

        match = re.search(r"-+\r\n(.*)OW3", response, re.DOTALL)
        if not match:
            raise ValueError("Not possible to find nodes")

        node_matchs = re.finditer(r"(.{20})\s(.{12})\s\[(.{4})\]!\s\s\[(.{23})\]!\s\s(.+)$", match.group(1),
                                  re.MULTILINE)
        if not node_matchs:
            raise ValueError("Not possible to match nodes")

        nodes = []
        for node_match in node_matchs:
            node = {
                "label": node_match.group(1).strip(),
                "type": node_match.group(2).strip(),
                "short_address": node_match.group(3),
                "extended": node_match.group(4),
                "hops": int(node_match.group(5))
            }
            nodes.append(node)

        return nodes

    def set_node(self, address, check_presence=True):
        address = address.upper()
        if check_presence:
            nodes = self.get_nodes()
            found = False
            for node in nodes:
                if address.upper() == node["extended"].upper():
                    found = True
                    break
            if not found:
                raise Exception(f"Node {address} not found in {nodes}")

        response = self.exec_command(f"!a{address}")
        return response

    def execute_cli(self, command, address=None):
        if address is not None:
            self.set_node(address)

        response = self.exec_command(command)
        response += self.read_until()

        response_match = re.search(r"\[([\w:]+)\]!\s\(\s\)\s+(.+)\s+\[CLI\]>", response, re.DOTALL)
        if not response_match:
            raise Exception(f"Not match on response {response} for command {command}")
        node = response_match.group(1)
        cli_response = response_match.group(2).strip()
        return cli_response


if __name__ == "__main__":
    print(f"Starting connection")

    seco_ip = "192.168.1.74"
    TEST_SSH = False
    if TEST_SSH:
        seco = SecoInterfaceSSH(seco_ip)
    else:
        seco = SecoInterfaceTelnet(seco_ip)

    # nodes = seco.get_nodes()
    # print(f"Nodes: {nodes}")

    node_address = "00:13:a2:00:41:bc:00:07"

    seco.set_node(node_address)
    res = seco.execute_command("version")
    print(f"version: {res}")

    print("end")
