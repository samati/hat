from serial import Serial
import logging
import time
import re
from hat.comms.load_lwm2m_xml import load_xml
import websockets
import asyncio
from xmodem import XMODEM

from hat.utils.utils import retry

log = logging.getLogger("G4 interface")
handler = logging.StreamHandler()
handler.setLevel("DEBUG")
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
handler.setFormatter(formatter)

# add ch to logger
log.addHandler(handler)

log.setLevel("DEBUG")
SLEEP_TIME = 1

DALI_BUS_STATE = {
    0: "disabled",
    1: "enable-assigning",
    2: "enabled-ready"
}

RFID_CHECK_STATUS = {
    0: "OK",
    1: "Operation timed out",
    2: "Unique identifier changed",
    3: "Index out of bounds",
    4: "Read block value size larger than expected",
    5: "Write block value size larger than tag block size",
    6: "Out of memory",
    7: "Internal I/O error",
    8: "Busy",
    9: "Initialization error"
}

# function to implement a retry mechanism


DEFAULT_XML_FOLDER = r"../hat/comms/load_lwm2m_xml/g4-lwm2m-objects"


class SerialInterface:
    def __init__(self, com_port, baudrate=115200, timeout=10):
        self._intf = Serial(port=com_port, baudrate=baudrate, timeout=timeout)

    def read_all(self):
        return self._intf.read_all().decode("ascii")

    def read_until(self, *args, **kwargs):
        return self._intf.read_until(*args, **kwargs).decode("ascii")

    def flushOutput(self):
        self._intf.flushOutput()

    def flush(self):
        self._intf.flush()

    def write(self, *args, **kwargs):
        return self._intf.write(*args)

    def xmodem_read(self, size, timeout=1): #\x06: ACK, #\x15: NACK
        read_val = self._intf.read(size)
        if read_val.decode("ascii") == 'C':
            # ignore the 'C's printed by the device during the xmodem mode
            return None
        else:
            return read_val

    def xmodem_write(self, data, timeout=1): #\x18: CANCEL
        return self._intf.write(data) or None


class WebsocketInterface:
    def __init__(self, address, hook="console", timeout=10):
        self._address = address
        self._timeout = timeout
        self._hook = hook
        self.connect()

    def connect(self):
        uri = f"ws://{self._address}/{self._hook}"
        self._intf = asyncio.get_event_loop().run_until_complete(websockets.connect(uri))

    def flush(self):
        # TODO: check this on the future
        pass

    def write(self, data):
        asyncio.get_event_loop().run_until_complete(self._intf.send(data))

    async def _async_read(self):
        data = await asyncio.wait_for(self._intf.recv(), self._timeout)

        return data

    def read(self):
        return asyncio.get_event_loop().run_until_complete(self._async_read())

    def flushOutput(self):
        # TODO: check this on the future
        pass

    async def _read_until(self, terminator):
        start_time = time.time()
        run_time = 0
        data = ""

        prompt_found = False

        while run_time < self._timeout:
            run_time = time.time() - start_time
            try:
                data += await asyncio.wait_for(self._intf.recv(), self._timeout - run_time)
            except asyncio.TimeoutError as e:
                log.debug(f"Timeout error, can be normal...")
                pass

            if terminator.decode("ascii") in data:
                prompt_found = True
                break
            else:
                time.sleep(0.1)

        if not prompt_found:
            log.warning(f"Prompt not found, data. {data}")
            # self.connect()

        return data

    def read_until(self, terminator):
        return asyncio.get_event_loop().run_until_complete(self._read_until(terminator))


class G4:
    def __init__(self, address, interface_type="SERIAL", baudrate=115200, timeout=2, folder=DEFAULT_XML_FOLDER):
        log.info("Connecting to G4 module")
        self._timeout = timeout
        self._mode = "normal"
        self._prompt = ">"
        self.objs = load_xml.Lwm2mLibrary(folder)

        if interface_type == "SERIAL":
            self._interface = SerialInterface(address, baudrate, timeout)
        elif interface_type == "WEBSOCKET":
            self._interface = WebsocketInterface(address, timeout=timeout)
        else:
            raise ValueError(f"Interface type {interface_type} note recognized")
        #
        # self.last_update = 0
        # self.current_microseconds = 0

    def flush_input(self):
        self._interface.flush()

    def wait_reset(self, timeout=120):
        reset_complete = False
        start_time = time.time()

        print(f"Waiting reset to finish... (timeout = {timeout})", end="")

        while time.time() - start_time < timeout:
            try:
                res = self._read_all()
                if "On full baltic sea" in res:
                    print('wait_reset: Found "On full baltic sea" - Device in Normal Mode')
                    reset_complete = True
                    break
                elif "______________" in res:
                    print('wait_reset: Found "______________" - Device in Factory Mode')
                    reset_complete = True
                    break
            except Exception:
                print(" . ", end="")
            time.sleep(3)

        if reset_complete:
            print(f"Reset finished OK!- Time to reset:{time.time() - start_time}\n")
            self.flush_input()
        else:
            print("Reset finished NOK! - wait_reset() exited due to Timeout\n")
            self.flush_input()

    def flex_get_meter_data(self):
        res = self.execute_command("meter-get-data")
        match = re.search(r"rms voltage\(mV\)\s+(\d+)\s+rms current\(mA\)\s+(\d+)\s+active power\(mW\)\s+(\d+)",
                          res, re.MULTILINE)
        res_dict = {}
        if match:
            res_dict["voltage"] = float(match.group(1)) / 1000.0
            res_dict["current"] = float(match.group(2))
            res_dict["power"] = float(match.group(3)) / 1000.0
            return res_dict
        raise Exception(f"Could not get meter data: {res}")

    def flex_set_relay(self, status):
        res = self.execute_command(f"zcr-relay {'on' if status else 'off'}")
        return res

    def set_flex_app_prompt(self):
        self.set_prompt("$ \r\n$ ")

    def set_prompt(self, prompt):
        self._prompt = prompt

    def flex_set_1_10v_start(self):
        self.execute_command("1_10v_start")

    def flex_set_1_10v_stop(self):
        self.execute_command("1_10v_stop")

    def flex_set_1_10v_voltage(self, voltage_mv):
        log.info(f"Setting 1_10v voltage: {voltage_mv}mV")
        self.execute_command(f"1_10v_set {int(voltage_mv)}")

    def flex_dali_start(self):
        self.execute_command("dali_start")

    def flex_dali_stop(self):
        self.execute_command("dali_stop")

    def flex_get_1_10v_voltage(self):
        aux = self.execute_command("1_10v_get")
        search = re.search(r"1-10V voltage:\s(\d+)\smV", aux)

        if search:
            voltage_mv = int(search.group(1))
            return voltage_mv
        else:
            raise Exception(f"Could not find voltage {aux}")

    def _read_until(self, prompt=None):
        if prompt is None:
            prompt = self._prompt
        data = self._interface.read_until(prompt.encode("ascii"))
        log.debug(f"Receiving: {data}")
        return data

    def _read_all(self):
        # Internal funtion, which reads all available data in the input
        data = self._interface.read_all()
        log.debug(f"Receiving: {data}")
        return data

    def write(self, command, flush=True):
        log.debug(f"Sending: {command}")
        if not command[-1] == "\n":
            command += "\n"

        command = command.encode("ascii")
        self._interface.write(command)

        if flush:
            self._interface.flushOutput()

    def read_all(self, prompt=None):
        if prompt is None:
            prompt = self._prompt
        data = self._read_until(prompt)
        return data

    def read_all_greedy(self):
        data = self._read_all()
        return data

    def execute_command(self, command, sleep_time=SLEEP_TIME, check_errors=False):
        self._interface.flush()
        # self.read_all() #- Works but is too slow due to timeout in reading
        self.write(command)
        time.sleep(sleep_time)
        data = self.read_all()
        self._interface.flush()

        data = data.replace(command, "")
        data = data.replace(self._prompt, "")

        return data

    def execute_command_slow(self, command, sleep_time=SLEEP_TIME, check_errors=False):
        # self._interface.flush()
        self.read_all()
        self._interface.flush()
        self.write(command)
        time.sleep(sleep_time)
        data = self.read_all()
        self._interface.flush()

        data = data.replace(command, "")
        data = data.replace(self._prompt, "")

        return data

    @retry(Exception, 4, 2, 1)
    def set_global_loglevel(self, level):
        log.info(f"Setting global log level to {level}")
        cmd = f"loglevel {level}"
        return self.execute_command(cmd)

    def set_module_loglevel(self, module, level):
        log.info(f"Setting module {module} log level to {level}")
        cmd = f"loglevel {module} {level}"
        return self.execute_command(cmd)

    @retry(Exception, 4, 2, 1)
    def lwm2m_read(self, object_id, resource_id, object_inst=0, check_read=True):
        resource = self.objs[object_id][resource_id]

        # check if it is possible to read
        if not resource.is_readable():
            raise Exception("Resource don't allow to read")

        log.info(f"Reading {resource}")
        cmd = f"lwm2m read {resource.object_id} {object_inst} {resource.resource_id}"
        resp = self.execute_command(cmd)
        match = re.search(
            r"Read \w+\s*\w* (?P<obj_id>\d+)/(?P<obj_inst>\d+)/(?P<resource>\d+): '?(?P<data>-?\w+\.?\w*)'?",
            resp,
            re.MULTILINE)
        if match:
            data = match.group("data")
            # Check if read is correct
            if check_read and (resource.object_id != int(match.group("obj_id")) or
                               object_inst != int(match.group("obj_inst")) or
                               resource.resource_id != int(match.group("resource"))):
                raise Exception("Invalid read")

            if resource.type == "Boolean":
                data = data == "true"
            elif resource.type == "Integer" or resource.type == "Unsigned Integer":
                data = int(data)
            elif resource.type == "String":
                pass
            elif resource.type == "Float":
                data = float(data)
            else:
                log.warning(f"Type {resource.type} is not recognized!")
            return data
        else:
            raise Exception(f"Error reading, read: {resp}")

    @retry(Exception, 4, 2, 1)
    def lwm2m_write(self, object_id, resource_id, data, object_inst=0, check_write=True):
        resource = self.objs[object_id][resource_id]
        log.info(f"Writing {resource} with {data}")

        # check if it is possible to write
        if not resource.is_writable():
            raise Exception("Resource don't allow to write")

        if resource.type == "Boolean":
            data = "1" if data else "0"
        elif resource.type == "Unsigned Integer":
            data = int(data)
        elif resource.type == "Integer" or resource.type == "String" or resource.type == "Float":
            pass

        else:
            log.warning(f"Type {resource.type} is not recognized!")

        cmd = f"lwm2m write {resource.object_id} {object_inst} {resource.resource_id} {data}"
        resp = self.execute_command(cmd)

        # TODO: add other checks
        if check_write and not "Write OK" in resp:
            raise Exception("Invalid Write")
        return resp

    @retry(Exception, 4, 2, 1)
    def lwm2m_write_slow(self, object_id, resource_id, data, object_inst=0, check_write=True):
        resource = self.objs[object_id][resource_id]
        log.info(f"Writing {resource} with {data}")

        # check if it is possible to write
        if not resource.is_writable():
            raise Exception("Resource don't allow to write")

        if resource.type == "Boolean":
            data = "1" if data else "0"
        elif resource.type == "Unsigned Integer":
            data = int(data)
        elif resource.type == "Integer" or resource.type == "String" or resource.type == "Float":
            pass

        else:
            log.warning(f"Type {resource.type} is not recognized!")

        cmd = f"lwm2m write {resource.object_id} {object_inst} {resource.resource_id} {data}"
        resp = self.execute_command_slow(cmd)

        # TODO: add other checks
        if check_write and not "Write OK" in resp:
            raise Exception("Invalid Write")
        return resp


    # Called by xmodem.send at the end of each transferred packet
    def xmodem_callback(self, total_packets, success_count, error_count):
        print(f"Total Packets: {total_packets}")
        print(f"Success Count: {success_count}")
        print(f"  Error Count: {error_count}")
        return


    @retry(Exception, 4, 2, 1)
    def upload_xmodem_configurations(self, filename):
        self._interface.flush()
        self.execute_command_slow("xmodem recv misc", sleep_time=1)
        #time.sleep(1)
        self._interface.flushOutput()
        #self._interface.flush()
        self.read_all_greedy()

        modem = XMODEM(self._interface.xmodem_read,self._interface.xmodem_write)
        try:
            stream = open (filename, 'rb')
        except:
            raise Exception("Failed to open file to transfer")

        if modem.send(stream, callback=self.xmodem_callback):
            return True
        else:
            raise Exception("Xmodem transfer failed")

    @retry(Exception, 4, 2, 1)
    def apply_factory_config(self, sleep_time = 7):
        data = self.execute_command_slow("factory apply", sleep_time=sleep_time)
        if "SUCCESSFUL" in data:
            return True
        else:
            raise Exception("Failed applying factory configs")
            return False

    @retry(Exception, 4, 2, 1)
    def lwm2m_exec(self, object_id, resource_id, data="", object_inst=0, check_exec=False):
        resource = self.objs[object_id][resource_id]
        log.info(f"Execute {resource} with {data}")

        # check if it is possible to write
        if not resource.is_executable():
            raise Exception("Resource don't allow to executed")

        if resource.type == "Boolean":
            data = "1" if data else "0"
        elif resource.type == "Integer" or resource.type == "" or resource.type is None:
            pass
        else:
            log.warning(f"Type {resource.type} is not recognized!")

        # FIXME: dual enter used for making appear the shell
        cmd = f"lwm2m exec {resource.object_id} {object_inst} {resource.resource_id} {data}"
        resp = self.execute_command(cmd)

        # TODO: correct check for exec
        if check_exec and not "Write OK" in resp:
            raise Exception("Invalid Write")
        return resp

    def lwm2m_get_dali_enable(self):
        log.debug("Get DALI bus enable")
        data = self.lwm2m_read("DALI bus", "Enable")
        log.info(f"DALI bus is {'Enabled' if data else 'Disabled'}")
        return data

    def lwm2m_set_dali_enable(self, enable):
        log.info(f"Set DALI bus {enable}")
        data = self.lwm2m_write("DALI bus", "Enable", enable)
        return data

    def lwm2m_get_dali_bus_state(self):
        log.debug("Get DALI bus state")
        data = self.lwm2m_read("DALI bus", "Bus state")
        if data in DALI_BUS_STATE.keys():
            data = DALI_BUS_STATE[data]
        else:
            raise Exception("Not recognized bus state")
        return data

    def lwm2m_get_dali_devices(self):
        log.debug("Get DALI bus devices")
        data = self.lwm2m_read("DALI bus", "No of devices")
        return data

    def lwm2m_get_1_10v_enable(self):
        log.debug("Get 1-10V bus enable")
        data = self.lwm2m_read("1_10V", "Enable")
        log.info(f"1-10V bus is {'Enabled' if data else 'Disabled'}")
        return data

    def lwm2m_set_1_10v_enable(self, enable):
        log.info(f"Set 1-10V bus {enable}")
        data = self.lwm2m_write("1_10V", "Enable", enable)
        return data

    def lwm2m_get_1_10v_value(self):
        log.debug("Get 1-10V value")
        data = self.lwm2m_read("1_10V", "value")
        log.info(f"1-10V value is {data}")
        return data

    def lwm2m_set_1_10v_value(self, value):
        log.info(f"Set 1-10V value {value}")
        data = self.lwm2m_write("1_10V", "value", value)
        return data

    def lwm2m_get_1_10v_measured(self):
        log.debug("Get 1-10V measured")
        data = self.lwm2m_read("1_10V", "measurement")
        log.info(f"1-10V measured is {data}%")
        return data

    def lwm2m_read_object(self, object_id, object_inst=0, use_id=False):
        obj = self.objs[object_id]
        log.debug(f"Reading object {obj}")
        data = {}
        for resource_id, resource in obj.resources.items():
            if resource.is_readable():
                data[resource_id if use_id else resource.name] = self.lwm2m_read(obj.object_id, resource_id,
                                                                                 object_inst)

        return data

    def lwm2m_get_RFID_UID(self):
        log.debug("Get RFID UID")
        data = self.lwm2m_read("RFID", "Unique Identifier")
        return data

    def lwm2m_check_RFID(self):
        log.debug("Execute check on RFID")
        data = self.lwm2m_exec("RFID", "Check")

    def lwm2m_get_RFID_check_status(self):
        log.debug("Get RFID check status")
        data = self.lwm2m_read("RFID", "Check Status")
        if data in RFID_CHECK_STATUS.keys():
            data = RFID_CHECK_STATUS[data]
        log.info(f"RFID check status is {data}")
        return data

    def lwm2m_set_photocell_sample_interval(self, interval):
        log.debug(f"Set photocell sample interval to {interval}")
        data = self.lwm2m_write("Photocell", "Sample interval", interval)
        return data

    def lwm2m_set_relay_switch_type(self, state: bool):
        log.debug(f"Set relay type {state}")
        data = self.lwm2m_write("Relay", "Planned Switch On/Off", state)
        return data

    def lwm2m_get_relay_switch_type(self):
        state = self.lwm2m_read("Relay", "Planned Switch On/Off")
        log.debug(f"Get relay type {state}")
        return state

    def lwm2m_get_microseconds(self):
        microseconds = self.lwm2m_read("Micro-seconds Timer", "Time micro")
        # self.last_update = time.time()
        # self.current_microseconds = microseconds
        return microseconds

    def lwm2m_set_relay_planned_switch_time(self, microseconds: int):
        log.debug(f"Set relay planned switch time {microseconds}")
        self.lwm2m_write("Relay", "Planned Switch Time", microseconds)

    def set_relay_state(self, state, delay=1):
        log.debug(f"Ser relay with state {state} with delay {delay}")
        # first set the switch type
        self.lwm2m_set_relay_switch_type(state)
        microseconds = self.lwm2m_get_microseconds()
        microseconds += delay * 1e6
        self.lwm2m_set_relay_planned_switch_time(microseconds)

    def lwm2m_get_fcfg_persisted(self):
        persisted = self.lwm2m_read("G4 Factory Config", "Config persisted")
        log.debug(f"Factory configuration persisted {persisted}")
        return persisted

    def lwm2m_set_fcfg_persisted(self):
        self.lwm2m_write("G4 Factory Config", "Config persisted", True)
        log.debug(f"Factory configuration persisted")
        return

    def lwm2m_get_fcfg_relay_on_delay(self):
        delay = self.lwm2m_read("G4 Factory Config", "Relay ON Delay")
        log.debug(f"Relay on delay {delay}us")
        return delay

    def lwm2m_get_fcfg_relay_off_delay(self):
        delay = self.lwm2m_read("G4 Factory Config", "Relay OFF Delay")
        log.debug(f"Relay off delay {delay}us")
        return delay

    def lwm2m_set_fcfg_relay_off_delay(self, delay):
        log.debug(f"Set Relay off delay {delay}us")
        self.lwm2m_write("G4 Factory Config", "Relay OFF Delay", delay)

    def lwm2m_set_fcfg_relay_on_delay(self, delay):
        log.debug(f"Set Relay on delay {delay}us")
        self.lwm2m_write("G4 Factory Config", "Relay ON Delay", delay)

    def lwm2m_fcfg_save(self):
        log.info("Save factory configuration")
        self.lwm2m_exec("G4 Factory Config", "Confirm config")

    def fcfg_wipe(self):
        log.warning("Deleting all the factory configurations")
        self.write("factory-wipe")

    def lwm2m_get_supply_voltage(self):
        voltage = self.lwm2m_read("Electrical monitor", "Supply voltage")
        log.debug(f"Supply voltage: {voltage}")
        return voltage

    def lwm2m_get_supply_current(self):
        current = self.lwm2m_read("Electrical monitor", "Supply current")
        log.debug(f"Supply current: {current}")
        return current

    def lwm2m_get_active_power(self):
        active_power = self.lwm2m_read("Electrical monitor", "Active power")
        log.debug(f"Active power: {active_power}")
        return active_power

    def lwm2m_get_active_energy(self):
        active_energy = self.lwm2m_read("Electrical monitor", "Cumulated active energy")
        log.debug(f"Active energy: {active_energy}")
        return active_energy

    def lwm2m_get_frequency(self):
        frequency = self.lwm2m_read("Electrical monitor", "Frequency")
        log.debug(f"Frequency: {frequency}")
        return frequency

    def lwm2m_get_power_factor(self):
        power_factor = self.lwm2m_read("Electrical monitor", "Power factor")
        log.debug(f"Power factor: {power_factor}")
        return power_factor

    # calibration writes need to be slower than the normal write method
    def lwm2m_set_eui(self, EUI):
        self.lwm2m_write_slow("G4 Factory Config", "DEV_EUI", EUI)

    def lwm2m_get_eui(self):
        return self.lwm2m_read("G4 Factory Config", "DEV_EUI")

    def lwm2m_set_voltage_factor(self, factor):
        self.lwm2m_write_slow("G4 Factory Config", "Voltage STPM correction", factor)

    def lwm2m_get_voltage_factor(self):
        return self.lwm2m_read("G4 Factory Config", "Voltage STPM correction")

    def lwm2m_set_current_factor(self, factor):
        self.lwm2m_write_slow("G4 Factory Config", "Current STPM correction", factor)

    def lwm2m_get_current_factor(self):
        return self.lwm2m_read("G4 Factory Config", "Current STPM correction")

    def lwm2m_set_relay_off(self, time_off):
        self.lwm2m_write_slow("G4 Factory Config", "Relay OFF Delay", time_off)

    def lwm2m_get_relay_off(self):
        return self.lwm2m_read("G4 Factory Config", "Relay OFF Delay")

    def lwm2m_set_relay_on(self, time_on):
        self.lwm2m_write_slow("G4 Factory Config", "Relay ON Delay", time_on)

    def lwm2m_get_relay_on(self):
        return self.lwm2m_read("G4 Factory Config", "Relay ON Delay")

    def lwm2m_get_hw_version(self):
        return self.lwm2m_read("G4 Factory Config", "HW Version")

    def lwm2m_set_hw_version(self, hw_version):
        self.lwm2m_write_slow("G4 Factory Config", "HW Version", hw_version)

    @staticmethod
    def check_cert_type(cert_type):
        if cert_type not in ['Client certifcate', 'Root certifcate', "subCA certificate", "Client private key"]:
            raise ValueError(f"Cert type invalid. {cert_type}")

    def lwm2m_read_cert(self, cert_type):
        G4.check_cert_type(cert_type)
        return self.lwm2m_read("G4 Factory Config", cert_type)

    def lwm2m_write_cert_part(self, cert_type, certificate_part):
        G4.check_cert_type(cert_type)
        self.lwm2m_write_slow("G4 Factory Config", cert_type,
                              certificate_part)

    @retry(Exception, 4, 2, 1)
    def lwm2m_write_cert(self, cert_type, certificate, xor, sleeptime=1):
        G4.check_cert_type(cert_type)

        # clear certificate
        self.lwm2m_write_cert_part(cert_type, '""')
        length = len(certificate)

        idx = 0
        while True:
            part = certificate[idx:idx + 64]
            if len(part) > 0:
                self.lwm2m_write_cert_part(cert_type, part)
                time.sleep(sleeptime)
            else:
                break
            idx += 64

        time.sleep(sleeptime)

        # fatory
        xor_certificate = int(self.lwm2m_read_cert(cert_type))

        if xor != xor_certificate:
            raise Exception(f"Xor expected {xor} different from {xor_certificate}")

        return

    def get_mesh_fw_version(self):
        return self.execute_command_slow("mesh efr-ver")

    def get_cal_data(self):
        data = {}
        data["EUI"] = self.lwm2m_get_eui()
        data["volt_factor"] = self.lwm2m_get_voltage_factor()
        data["current_factor"] = self.lwm2m_get_current_factor()
        data["relay_on"] = self.lwm2m_get_relay_on()
        data["relay_off"] = self.lwm2m_get_relay_off()
        try:
            data["hw_version"] = self.lwm2m_get_hw_version()
        except Exception as e:
            print(f"Could not read HW version")

        return data

    def set_lamp_level_cmd(self, instance, level):
        self.lwm2m_write("Outdoor lamp controller", "Command", level, instance)

    def get_lamp_level_cmd(self, instance):
        return self.lwm2m_read("Outdoor lamp controller", "Command", instance)

    def get_lamp_dim_level(self, instance):
        return self.lwm2m_read("Outdoor lamp controller", "Dimming level", instance)

    def set_lamp_min_level(self, instance, level):
        self.lwm2m_write("Outdoor lamp controller", "Minimum dimming level", level, instance)

    def get_lamp_min_level(self, instance):
        return self.lwm2m_read("Outdoor lamp controller", "Minimum dimming level", instance)

    def set_lamp_min_voltage_level(self, instance, level):
        self.lwm2m_write("Outdoor lamp controller", "Voltage at min dim level", level, instance)

    def get_lamp_min_voltage_level(self, instance):
        return self.lwm2m_read("Outdoor lamp controller", "Voltage at min dim level", instance)

    def set_lamp_max_voltage_level(self, instance, level):
        self.lwm2m_write("Outdoor lamp controller", "Voltage at max dim level", level, instance)

    def get_lamp_max_voltage_level(self, instance):
        return self.lwm2m_read("Outdoor lamp controller", "Voltage at max dim level", instance)

    def set_bootstrap_server(self, instance, uri):
        self.lwm2m_write("LWM2M Security", "LWM2M  Server URI", uri)

    def get_bootstrap_server(self, instance):
        return self.lwm2m_read("LWM2M Security", "LWM2M  Server URI", instance)

    def sys_info(self):
        res_dict = {}
        res = self.execute_command("sys info")

        return res

    def reset(self, wait_end=False, timeout=60):
        res = self.execute_command("reset")
        if wait_end:
            self.wait_reset(timeout)
        return res

    def fsclear(self, wait_end=False, timeout=60):
        res = self.execute_command("fsclear")
        if wait_end:
            self.wait_reset(timeout)
        return res

    def rules_lock(self, seconds):
        log.info(f"Setting rules lock for {seconds}s")
        self.execute_command(f"rules lock_for {seconds}")


if __name__ == "__main__":
    # variable setting
    com_port = "COM23"
    timeout = 15

    test_relay = False
    test_dali = False
    test_1_10v = True
    test_rfid = False
    ip = "192.168.1.83"
    interface_type = "WEBSOCKET"

    print(f"Connecting to port {com_port}")
    if interface_type == "SERIAL":
        g4 = G4(com_port, timeout=timeout)
    elif interface_type == "WEBSOCKET":
        g4 = G4(ip, interface_type=interface_type)
    else:
        raise ValueError(f"Invalid interface type: {interface_type}")

    print(f"Help: {g4.execute_command('help')}")

    g4.set_global_loglevel(0)
    g4.set_module_loglevel("CONN_M", 4)
    g4.set_module_loglevel("MDM", 4)
    g4.set_module_loglevel("NRG_S", 2)
    g4.set_module_loglevel("WAKAAMA_MAIN", 4)
    g4.set_module_loglevel("WAKAAMA_UDP", 4)
    g4.set_module_loglevel("NTP", 4)
    g4.set_module_loglevel("E2EU", 4)
    g4.set_module_loglevel("SMS_S", 4)

    # read a complete object
    # print(g4.lwm2m_read_object("DALI device"))

    # disable 1-10V and Dali before starting
    # print(g4.lwm2m_set_dali_enable(False))
    # print(g4.lwm2m_set_1_10v_enable(False))
    time.sleep(1)

    if test_dali:
        def test_enable():
            print(g4.lwm2m_set_dali_enable(False))
            time.sleep(1)
            print(g4.lwm2m_get_dali_enable())
            time.sleep(1)
            print(g4.lwm2m_set_dali_enable(True))
            print(g4.lwm2m_get_dali_enable())

            for i in range(20):
                time.sleep(2)
                data = g4.lwm2m_get_dali_bus_state()
                if data == "enabled-ready":
                    log.info("Assigment ready")
                    break

            data = g4.lwm2m_get_dali_devices()
            print(data)
            if data != 1:
                raise Exception(f"Should have found 1 device and not {data}")


        test_enable()

        print(g4.lwm2m_read("DALI device", "Short address", 0))
        print(g4.lwm2m_read("DALI device", "Enable", 0))
        print(g4.lwm2m_read("DALI device", "Type", 0))
        print(g4.lwm2m_read("DALI device", "Is working", 0))
        print(g4.lwm2m_read("DALI device", "Operating", 0))
        print(g4.lwm2m_read("DALI device", "In reset", 0))
        print(g4.lwm2m_read("DALI device", "Level", 0))
        print(g4.lwm2m_read("DALI device", "On/Off", 0))
        print(g4.lwm2m_read("DALI device", "Power on level", 0))
        print(g4.lwm2m_read("DALI device", "Power on failure", 0))
        print(g4.lwm2m_read("DALI device", "Max level", 0))
        print(g4.lwm2m_read("DALI device", "Fade time", 0))
        print(g4.lwm2m_read("DALI device", "Bank0 GTIN", 0))

    if test_1_10v:
        log.info("Testing 1-10V")
        print(g4.lwm2m_set_1_10v_enable(False))
        time.sleep(1)
        print(g4.lwm2m_get_1_10v_enable())
        print(g4.lwm2m_set_1_10v_enable(True))
        time.sleep(1)
        print(g4.lwm2m_get_1_10v_enable())
        print(g4.lwm2m_set_1_10v_value(50))
        time.sleep(1)
        print(g4.lwm2m_get_1_10v_measured())

    if test_rfid:
        import pandas as pd

        n_repetitions = 100
        max_timeout = 40
        filename = "data/test.xls"

        data = pd.DataFrame()

        # test with different sample intervals
        # g4.lwm2m_set_photocell_sample_interval(0.1)

        log.info("Testing RFID")
        for i in range(n_repetitions):
            log.info(f"Test #{i + 1}")
            start_time = time.time()
            print(g4.lwm2m_check_RFID())
            time.sleep(1)
            while time.time() - start_time < max_timeout:
                status = g4.lwm2m_get_RFID_check_status()
                print(f"Status is {status}")
                if status == "OK":
                    break

            uid = g4.lwm2m_get_RFID_UID()
            data.loc[i, "Elapsed Time"] = time.time() - start_time
            data.loc[i, "UID"] = uid
            data.loc[i, "status"] = status

        data.to_excel(filename)

    if test_relay:
        g4.lwm2m_set_relay_switch_type(True)
        state = g4.lwm2m_get_relay_switch_type()
        log.info(f"Should be True: {state}")

        g4.lwm2m_set_relay_switch_type(False)
        state = g4.lwm2m_get_relay_switch_type()
        log.info(f"Should be False: {state}")

        current_micro = g4.lwm2m_get_microseconds()
        log.info(f"Time: {current_micro}")

        log.info("Setting relay on")

        g4.set_relay_state(True)

        time.sleep(1)

        log.info("Serring relay off")
        g4.set_relay_state(False)

    print("end!")
