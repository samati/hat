import xml.etree.ElementTree as ET
import os
import re


class Lwm2mResource:
    object_id: int
    resource_id: int
    name: str
    operations: str
    type: str
    units: str
    description: str
    others: dict

    def __init__(self, object_id, resource_id, name, operations, resource_type, units, description, others):
        self.object_id = object_id
        self.resource_id = resource_id
        self.name = name
        self.operations = operations
        self.type = resource_type
        self.units = units
        self.description = description
        self.others = others

    def __str__(self):
        resp = f"Resource id: {self.resource_id}, name: {self.name}, operations: {self.operations}, type: {self.type}, description: {self.description}"

        return resp

    def is_readable(self):
        return "R" in self.operations

    def is_writable(self):
        return "W" in self.operations

    def is_executable(self):
        return "E" in self.operations


class Lwm2mObject:
    object_id: int
    name: str
    description1: str
    object_urn: str
    lwm2m_version: str
    multi_instance: bool
    mandatory: bool

    def __init__(self, object_id, name, description1, object_urn, lwm2m_version, multi_instance,
                 mandatory):
        self.object_id = object_id
        self.name = name
        self.description1 = description1
        self.object_urn = object_urn
        self.lwm2m_version = lwm2m_version
        self.multi_instance = multi_instance
        self.mandatory = mandatory

        self.resources = {}

    def __str__(self):
        resp = f"Obj id: {self.object_id}, name: {self.name}\r\nResources:"
        for resource_id, resource in self.resources.items():
            resp += f"\r\n\tid: {resource_id}, name: {resource.name}"

        return resp

    def __getitem__(self, item):
        if isinstance(item, int):
            if item in self.resources:
                return self.resources[item]
            else:
                raise ValueError(f"Item {item} not found!")
        elif isinstance(item, str):
            for _, resource in self.resources.items():
                if resource.name == item:
                    return resource
            raise ValueError(f"Item {item} not found!")
        else:
            raise IndexError(f"Don't know how to handle item type")


class Lwm2mLibrary:
    def __init__(self, folder):
        self.objects = {}
        self.map_name_to_id = {}
        print(f"Parsing objects in folder {folder}")

        if not os.path.exists(folder):
            print(f"Folder: {os.path.abspath(folder)} does not exists")

        for root_dir, dirs, files in os.walk(folder):
            for filename in files:
                # print(f"Filename {filename}")
                if os.path.splitext(filename)[-1] != ".xml":
                    # print(f"Not a XML filename, continuing")
                    continue

                # file is a xml file
                # parse it
                tree = ET.parse(os.path.join(root_dir, filename))
                root = tree.getroot()
                # for child in root:
                #     print(f"{child.tag}, f{child.attrib}")

                child = root[0]
                if child.tag != "Object":
                    raise Exception("Object received don't have right tag")

                object_map = {}
                for x in child:
                    if x.tag == "Resources":  # resources have several child
                        # when getting to Resources, all others parameters should be available
                        object_id = int(object_map["ObjectID"])
                        lwm2m_obj = Lwm2mObject(object_id,
                                                object_map["Name"],
                                                object_map["Description1"],
                                                object_map["ObjectURN"],
                                                object_map["LWM2MVersion"] if "LWM2MVersion" in object_map else "NA",
                                                True if object_map["MultipleInstances"] == "Multiple" else False,
                                                True if object_map["Mandatory"] == "Mandatory" else False)

                        for item in x:  # iterate over items
                            others = {}
                            id = int(item.attrib["ID"])
                            for item_obj in item:
                                if item_obj.tag == "Name":
                                    name = item_obj.text
                                elif item_obj.tag == "Operations":
                                    operations = item_obj.text
                                elif item_obj.tag == "Type":
                                    obj_type = item_obj.text
                                elif item_obj.tag == "Units":
                                    units = item_obj.text
                                elif item_obj.tag == "Description":
                                    description = item_obj.text
                                else:
                                    others[item_obj.tag] = item_obj.text
                            lwm2m_obj.resources[id] = Lwm2mResource(object_id, id, name, operations, obj_type, units,
                                                                    description, others)

                    else:
                        object_map[x.tag] = x.text

                # add object to list
                if object_id in self.objects:
                    raise Exception(f"Object {object_id} already present on the list")

                self.objects[object_id] = lwm2m_obj
                self.map_name_to_id[lwm2m_obj.name] = object_id

        if not len(self.objects):
            print("No objects created, check file location")

        print("Objects created!")

    def __getitem__(self, item):
        # print(f"Getting object {item}")
        if isinstance(item, int):
            if item in self.objects:
                return self.objects[item]
            else:
                raise IndexError(f"Item {item} not found")
        elif isinstance(item, str):
            if item in self.map_name_to_id:
                return self.objects[self.map_name_to_id[item]]
            else:
                raise IndexError(f"Item {item} not found")
        else:
            raise IndexError(f"Don't know how to handle item type")


if __name__ == "__main__":
    folder = "g4-lwm2m-objects"
    filename = "g4-lwm2m-objects/32212_1_10V.xml"
    print(f"Reading xml file {filename}")

    obj = Lwm2mLibrary(folder)
    print(obj[32211])
    print(obj["DALI bus"])

    print(obj["DALI bus"]["Enable"])
    print(obj["DALI bus"][26990])
    print("end!")
