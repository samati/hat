<?xml version="1.0" encoding="UTF-8"?>

<!-- BSD-3 Clause License

Copyright 2019 Open Mobile Alliance. 

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived
from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
-->

<LWM2M xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://openmobilealliance.org/tech/profiles/LWM2M.xsd">

	<Object ObjectType="MODefinition">
		<Name>Digital Input</Name>
		<Description1>Generic digital input for non-specific sensors</Description1>
		<ObjectID>3200</ObjectID>
		<ObjectURN>urn:oma:lwm2m:ext:3200</ObjectURN>
		<LWM2MVersion>1.0</LWM2MVersion>
                <ObjectVersion>1.0</ObjectVersion>
		<MultipleInstances>Multiple</MultipleInstances>
		<Mandatory>Optional</Mandatory>
		<Resources>
			<Item ID="5500">
				<Name>Digital Input State</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Mandatory</Mandatory>
				<Type>Boolean</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>The current state of a digital input.</Description>
			</Item>
			<Item ID="5501">
				<Name>Digital Input Counter</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>The cumulative value of active state detected.</Description>
			</Item>
			<Item ID="5502">
				<Name>Digital Input Polarity</Name>
				<Operations>RW</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Boolean</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>The polarity of the digital input as a Boolean (0 = Normal, 1= Reversed)</Description>
			</Item>
			<Item ID="5503">
				<Name>Digital Input Debounce</Name>
				<Operations>RW</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>ms</Units>
				<Description>The debounce period in ms. .</Description>
			</Item>
			<Item ID="5504">
				<Name>Digital Input Edge Selection</Name>
				<Operations>RW</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration>1-3</RangeEnumeration>
				<Units></Units>
				<Description>The edge selection as an integer (1 = Falling edge, 2 = Rising edge, 3 = Both Rising and Falling edge).</Description>
			</Item>
			<Item ID="5505">
				<Name>Digital Input Counter Reset</Name>
				<Operations>E</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type></Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Reset the Counter value.</Description>
			</Item>
			<Item ID="5750">
				<Name>Application Type</Name>
				<Operations>RW</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>String</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>The application type of the sensor or actuator as a string, for instance, "Air Pressure"</Description>
			</Item>
			<Item ID="5751">
				<Name>Sensor Type</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>String</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>The type of the sensor (for instance PIR type)</Description>
			</Item>
			<Item ID="0">
				<Name>Digital Input Failure check period</Name>
				<Operations>RW</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>s</Units>
				<Description><![CDATA[G4: Duration after which the Digital Input failure is set to "1" when there is no change to the Digital Input. Set it to zero will disable the failure check]]></Description>
			</Item>
			<Item ID="1">
				<Name>Digital Input Failure</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Boolean</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description><![CDATA[G4: If the sensor is not showing any edges for "Digital Input Failure check period" we can conclude that it has a failure]]></Description>
			</Item>
			<Item ID="2">
				<Name>Digital Input Level Selection</Name>
				<Operations>RW</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration>1-2</RangeEnumeration>
				<Units></Units>
				<Description><![CDATA[G4: The active level selection as an integer (1 = active low (0) , 2 = active high (1))]]></Description>
			</Item>
			<Item ID="3">
				<Name>Digital Input  Selection</Name>
				<Operations>RW</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration>1-3</RangeEnumeration>
				<Units></Units>
				<Description><![CDATA[G4: The trigger selection as an integer (1 = edge trigger (5504), 2 =level trigger (2),  3 = both)]]></Description>
			</Item>
			<Item ID="5850">
				<Name>On/Off</Name>
				<Operations>RW</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Boolean</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>ms</Units>
				<Description>On/off control. Boolean value where True is On and False is Off</Description>
			</Item>
		</Resources>
		<Description2><![CDATA[]]></Description2>
	</Object>
</LWM2M>
