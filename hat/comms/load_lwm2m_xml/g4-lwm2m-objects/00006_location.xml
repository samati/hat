<?xml version="1.0" encoding="UTF-8"?>

<!--
FILE INFORMATION

OMA Permanent Document
   File: OMA-SUP-XML_LWM2M_Location-V1_0_2-20190617-A
   Type: xml
   Date: 2019-Jun-17

Public Reachable Information
   Path: http://www.openmobilealliance.org/tech/profiles
   Name: LWM2M_Location-v1_0_2.xml

NORMATIVE INFORMATION

  Information about this file can be found in the latest revision of

    OMA-TS-LightweightM2M-V1_0_2
    OMA-TS-LightweightM2M_Core-V1_1_1

  This is available at http://www.openmobilealliance.org/

  Send comments to https://github.com/OpenMobileAlliance/OMA_LwM2M_for_Developers/issues

LEGAL DISCLAIMER

  Copyright 2019 Open Mobile Alliance.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

  1. Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
  3. Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived
  from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.

  The above license is used as a license under copyright only.  Please
  reference the OMA IPR Policy for patent licensing terms:
  https://www.omaspecworks.org/about/intellectual-property-rights/

-->

<LWM2M xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://www.openmobilealliance.org/tech/profiles/LWM2M.xsd">
  <Object ObjectType="MODefinition">
		<Name>Location</Name>
		<Description1><![CDATA[This LwM2M Object provides a range of location telemetry related information which can be queried by the LwM2M Server.]]></Description1>
		<ObjectID>6</ObjectID>
		<ObjectURN>urn:oma:lwm2m:oma:6</ObjectURN>
	  	<LWM2MVersion>1.0</LWM2MVersion>
	  	<ObjectVersion>1.0</ObjectVersion>
		<MultipleInstances>Single</MultipleInstances>
		<Mandatory>Optional</Mandatory>
		<Resources>
			<Item ID="0">
				<Name>Latitude</Name>
        			<Operations>RW</Operations>
        			<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Mandatory</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration>-90..90</RangeEnumeration>
				<Units>lat</Units>
				<Description><![CDATA[The decimal notation of latitude, e.g. -43.5723 [World Geodetic System 1984]. Values outside the range indicate that the latitude is not available.]]></Description>
			</Item>
			<Item ID="1">
				<Name>Longitude</Name>
        			<Operations>RW</Operations>
        			<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Mandatory</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration>-180..180</RangeEnumeration>
				<Units>lon</Units>
				<Description><![CDATA[The decimal notation of longitude, e.g. 153.21760 [World Geodetic System 1984]. Values outside the range indicate that the longitude is not available.]]></Description>
			</Item>
			<Item ID="2">
				<Name>Altitude</Name>
        			<Operations>RW</Operations>
        			<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>m</Units>
				<Description><![CDATA[The decimal notation of altitude in meters above mean sea level. High negative values indicate that the altitude is not available.]]></Description>
			</Item>
			<Item ID="3">
				<Name>Radius</Name>
        			<Operations>RW</Operations>
        			<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>m</Units>
				<Description><![CDATA[The value in this resource indicates the radius of a circular area in meters. The circular area is used to describe uncertainty about a point for coordinates in a two-dimensional coordinate reference systems (CRS). The center point of a circular area is specified by using the Latitude and the Longitude Resources. Negatives values indicate that the radius is not available.]]></Description>
			</Item>
			<Item ID="5">
				<Name>Timestamp</Name>
        			<Operations>R</Operations>
        			<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Mandatory</Mandatory>
				<Type>Time</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>s</Units>
				<Description><![CDATA[The timestamp of when the location measurement was performed. Old dates indicate that the timestamp is not available.]]></Description>
			</Item>
			<Item ID="31313">
				<Name>GPS Sync On/Off</Name>
        			<Operations>RW</Operations>
        			<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Boolean</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description><![CDATA[This resource represents an on/off actuator that controls the GPS synchronization, which can be controlled, the setting of which is a Boolean value where True is On and False is Off.]]></Description>
			</Item>
			<Item ID="31314">
				<Name>Location Acquisition Timeout</Name>
        			<Operations>RW</Operations>
        			<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>s</Units>
				<Description><![CDATA[The timeout for GPS based location acquisition.]]></Description>
			</Item>
			<Item ID="31315">
				<Name>Location Acquisition Fail</Name>
        			<Operations>R</Operations>
        			<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Boolean</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description><![CDATA[Flag indicating the occurrence of the GPS based location acquisition timeout.]]></Description>
			</Item>
			<Item ID="31316">
				<Name>Location Accuracy</Name>
        			<Operations>RW</Operations>
        			<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>m</Units>
				<Description><![CDATA[The value in this resource indicates the minimum horizontal accuracy for GPS based location acquisition.]]></Description>
			</Item>
			</Resources>
		<Description2></Description2>
	</Object>
</LWM2M>
