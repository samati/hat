#  vim:fileencoding=utf-8
#  
#  Owlet_CAL.py
#  src
#  
#  Created by Raoul van Bergen on 2013-01-10.
#  Copyright 2013 Owlet GmbH. All rights reserved.
# 
MY_NAME = "Owlet3 CommandLine Interface"
MY_VERSION = "4.000.00.001Alpha"

"""
Owlet ComandLine Interface.

This tool sends data request as braodcasts and will read EBD01 tag and display Mac Address, returned energy measurements
so Voltage, Current and Wattage

Open Items:
Mantis 2188: timezone UTC of GPX01
app17 tag support
Create main Owlet_TAG and zipped _Owlet_TAG.py so code is at least zipped and smaller and not that easy to read

"""

# constants

# timeout for responses of the ZB node (coc/luco)
RESPONSE_TIMEOUT = 10
MAX_TRIES = 2
# interval between retries of each step
RETRY_INTERVAL = 4
# ZB timeouts
TX_TIMEOUT = 4.0

ND_CYCLE_DELAY = 20.0

REDUNDANCY_INTERVAL = 3  # mins
ZIGBEE_SERIAL_EP = 0xe8
ZIGBEE_ALT_SERIAL_EP = 0xda
ZIGBEE_NETRESET_EP = 0xe6
ZIGBEE_TAG_EP = 0xB2
ZIGBEE_CLI_EP = 0xB3
ZIGBEE_BTL_EP = 0xB4

# imports

import binascii
import code
import math
import os
import re
import socket
import struct
import sys
import threading
import time
import traceback

# add digi xbee
from digi.xbee.devices import ZigBeeDevice, RemoteZigBeeDevice, ZigBeeNetwork
from digi.xbee.exception import TimeoutException
from digi.xbee.models.address import XBee64BitAddress
from digi.xbee.models.options import DiscoveryOptions

# from select import select

# private imports

OWLET_APPLICATION = os.path.join('WEB/python', "owlet.zip")
OWLET_SUPPORT_PYTHON = os.path.join('WEB/python', 'python.zip')
sys.path.insert(0, OWLET_SUPPORT_PYTHON)
sys.path.insert(0, OWLET_APPLICATION)
sys.path.insert(0, os.path.join(OWLET_APPLICATION, "src"))
sys.path.insert(0, os.path.join(OWLET_APPLICATION, "lib"))

try:
    #    import digicli
    import zigbee
except:
    print("Could not import zigbee")

# exception classes

# interface functions

# classes

# globals
num_retries = MAX_TRIES
StopRunning = False
StealthMode = False
RangeTest = 0
SdbTest = 0
displayRaw = False
sendmsg = ""
climsg = ""
tagmsg = ""
Tx_count = 0
Rx_count = 0
node_list = []
bb_overrule = False
cur_ver = 0xFFFFFFFF
prompt = ""
last_message_time = 0.0
cliautomode = 0
clicmdmode = 0
nextclitime = 0.0
# enums
E_HEX = 1
E_UDEC = 2
E_SDEC = 3
E_TIMESTAMP = 4
E_STRING = 5
E_VERSION = 6
E_ACTIME = 7
E_S2B = 8
E_HEXSTRING = 9
E_UTIMESTAMP = 10
E_UTCTIMESTAMP = 11
E_5VOLT = 12
E_PF = 13
E_TEMP = 14
E_VOLT = 15

# tag table contains list of tuple
info_tag_table = [ \
    (0x10, "SYS02", 14, [("FFM", 1, E_HEX), ("FRH", 4, E_UDEC), ("TMP", 2, E_UDEC), \
                         ("FTS", 4, E_UTIMESTAMP), ("FRE", 4, E_UTIMESTAMP)]), \
    (0x11, "SYS03", 19, [("FEC", 8, E_UDEC), ("FET", 4, E_UTIMESTAMP), ("FCP", 2, E_UDEC), \
                         ("FCV", 2, E_UDEC), ("FCC", 2, E_UDEC), ("FPF", 1, E_PF)]), \
    (0x12, "SYS04", 47,
     [("SFW", 4, E_VERSION), ("SNO", 12, E_STRING), ("PTY", 22, E_STRING), ("EUI", 8, E_HEX), ("BVR", 1, E_HEX)]), \
    (0x13, "SYS05", 1, [("FPE", 1, E_HEX)]), \
    (0x14, "PHC01", 5, [("FPS", 1, E_HEX), ("FL1", 2, E_UDEC), ("FL2", 2, E_UDEC)]), \
    (0x19, "COM01", 1, [("FCT", 1, E_HEX)]), \
    (0x20, "POS01", 27, [("FDL", 1, E_UDEC), ("FRH", 2, E_UDEC), ("FON", 6, E_TIMESTAMP), \
                         ("FOF", 6, E_TIMESTAMP), ("FEM", 1, E_HEX), ("FEC", 4, E_UDEC), ("FCP", 2, E_UDEC), \
                         ("FCV", 2, E_UDEC), ("FCC", 2, E_UDEC), ("FPF", 1, E_PF)]), \
    (0x72, "PHX01", 4, [("PLL", 1, E_UDEC), ("PLH", 1, E_UDEC), ("PLS", 1, E_UDEC), ("PLT", 1, E_UDEC)]), \
    (0x73, "MPX01", 7,
     [("XPE", 1, E_UDEC), ("EEE", 1, E_UDEC), ("CED", 1, E_UDEC), ("CDD", 1, E_UDEC), ("OTC", 1, E_UDEC),
      ("TMX", 2, E_TEMP)]), \
    (0x74, "ENX01", 10,
     [("AFS", 1, E_UDEC), ("AFZ", 1, E_UDEC), ("ACC", 3, E_HEX), ("AHZ", 1, E_UDEC), ("AVL", 1, E_UDEC),
      ("ADP", 3, E_UDEC)]), \
    (0x75, "SYX01", 9,
     [("AFV", 1, E_5VOLT), ("P0E", 2, E_UDEC), ("P1E", 2, E_UDEC), ("P2E", 2, E_UDEC), ("P3E", 2, E_UDEC)]), \
    (0x78, "I2X01", 2, [("FBE", 2, E_HEX)]), \
    (0x80, "DLS01", 2, [("FBE", 1, E_HEX), ("FDC", 1, E_UDEC)]), \
    (0x81, "dld01", 16, [("FDL", 1, E_UDEC), ("FRH", 2, E_UDEC), ("FON", 4, E_UTIMESTAMP), \
                         ("FOF", 4, E_UTIMESTAMP), ("FGS", 1, E_HEX), ("FLP", 1, E_UDEC), ("FDT", 1, E_UDEC), \
                         ("FHF-FFS", 1, E_UDEC), ("FHS-FTM", 1, E_UDEC)]), \
    (0x82, "dld02", 16, [("FDL", 1, E_UDEC), ("FRH", 2, E_UDEC), ("FON", 4, E_UTIMESTAMP), \
                         ("FOF", 4, E_UTIMESTAMP), ("FGS", 1, E_HEX), ("FLP", 1, E_UDEC), ("FDT", 1, E_UDEC), \
                         ("FHF-FFS", 1, E_UDEC), ("FHS-FTM", 1, E_UDEC)]), \
    (0x83, "dld03", 16, [("FDL", 1, E_UDEC), ("FRH", 2, E_UDEC), ("FON", 4, E_UTIMESTAMP), \
                         ("FOF", 4, E_UTIMESTAMP), ("FGS", 1, E_HEX), ("FLP", 1, E_UDEC), ("FDT", 1, E_UDEC), \
                         ("FHF-FFS", 1, E_UDEC), ("FHS-FTM", 1, E_UDEC)]), \
    (0x84, "dld04", 16, [("FDL", 1, E_UDEC), ("FRH", 2, E_UDEC), ("FON", 4, E_UTIMESTAMP), \
                         ("FOF", 4, E_UTIMESTAMP), ("FGS", 1, E_HEX), ("FLP", 1, E_UDEC), ("FDT", 1, E_UDEC), \
                         ("FHF-FFS", 1, E_UDEC), ("FHS-FTM", 1, E_UDEC)]), \
    (0x85, "dld05", 16, [("FDL", 1, E_UDEC), ("FRH", 2, E_UDEC), ("FON", 4, E_UTIMESTAMP), \
                         ("FOF", 4, E_UTIMESTAMP), ("FGS", 1, E_HEX), ("FLP", 1, E_UDEC), ("FDT", 1, E_UDEC), \
                         ("FHF-FFS", 1, E_UDEC), ("FHS-FTM", 1, E_UDEC)]), \
    (0x86, "dld06", 16, [("FDL", 1, E_UDEC), ("FRH", 2, E_UDEC), ("FON", 4, E_UTIMESTAMP), \
                         ("FOF", 4, E_UTIMESTAMP), ("FGS", 1, E_HEX), ("FLP", 1, E_UDEC), ("FDT", 1, E_UDEC), \
                         ("FHF-FFS", 1, E_UDEC), ("FHS-FTM", 1, E_UDEC)]), \
    (0x87, "dld07", 16, [("FDL", 1, E_UDEC), ("FRH", 2, E_UDEC), ("FON", 4, E_UTIMESTAMP), \
                         ("FOF", 4, E_UTIMESTAMP), ("FGS", 1, E_HEX), ("FLP", 1, E_UDEC), ("FDT", 1, E_UDEC), \
                         ("FHF-FFS", 1, E_UDEC), ("FHS-FTM", 1, E_UDEC)]), \
    (0x88, "dld08", 16, [("FDL", 1, E_UDEC), ("FRH", 2, E_UDEC), ("FON", 4, E_UTIMESTAMP), \
                         ("FOF", 4, E_UTIMESTAMP), ("FGS", 1, E_HEX), ("FLP", 1, E_UDEC), ("FDT", 1, E_UDEC), \
                         ("FHF-FFS", 1, E_UDEC), ("FHS-FTM", 1, E_UDEC)]), \
    (0x89, "dld09", 16, [("FDL", 1, E_UDEC), ("FRH", 2, E_UDEC), ("FON", 4, E_UTIMESTAMP), \
                         ("FOF", 4, E_UTIMESTAMP), ("FGS", 1, E_HEX), ("FLP", 1, E_UDEC), ("FDT", 1, E_UDEC), \
                         ("FHF-FFS", 1, E_UDEC), ("FHS-FTM", 1, E_UDEC)]), \
    (0x8a, "dld10", 16, [("FDL", 1, E_UDEC), ("FRH", 2, E_UDEC), ("FON", 4, E_UTIMESTAMP), \
                         ("FOF", 4, E_UTIMESTAMP), ("FGS", 1, E_HEX), ("FLP", 1, E_UDEC), ("FDT", 1, E_UDEC), \
                         ("FHF-FFS", 1, E_UDEC), ("FHS-FTM", 1, E_UDEC)]), \
    (0x8b, "dld11", 16, [("FDL", 1, E_UDEC), ("FRH", 2, E_UDEC), ("FON", 4, E_UTIMESTAMP), \
                         ("FOF", 4, E_UTIMESTAMP), ("FGS", 1, E_HEX), ("FLP", 1, E_UDEC), ("FDT", 1, E_UDEC), \
                         ("FHF-FFS", 1, E_UDEC), ("FHS-FTM", 1, E_UDEC)]), \
    (0x8c, "dld12", 16, [("FDL", 1, E_UDEC), ("FRH", 2, E_UDEC), ("FON", 4, E_UTIMESTAMP), \
                         ("FOF", 4, E_UTIMESTAMP), ("FGS", 1, E_HEX), ("FLP", 1, E_UDEC), ("FDT", 1, E_UDEC), \
                         ("FHF-FFS", 1, E_UDEC), ("FHS-FTM", 1, E_UDEC)]), \
    (0x8d, "dld13", 16, [("FDL", 1, E_UDEC), ("FRH", 2, E_UDEC), ("FON", 4, E_UTIMESTAMP), \
                         ("FOF", 4, E_UTIMESTAMP), ("FGS", 1, E_HEX), ("FLP", 1, E_UDEC), ("FDT", 1, E_UDEC), \
                         ("FHF-FFS", 1, E_UDEC), ("FHS-FTM", 1, E_UDEC)]), \
    (0x8e, "dld14", 16, [("FDL", 1, E_UDEC), ("FRH", 2, E_UDEC), ("FON", 4, E_UTIMESTAMP), \
                         ("FOF", 4, E_UTIMESTAMP), ("FGS", 1, E_HEX), ("FLP", 1, E_UDEC), ("FDT", 1, E_UDEC), \
                         ("FHF-FFS", 1, E_UDEC), ("FHS-FTM", 1, E_UDEC)]), \
    (0x8f, "dld15", 16, [("FDL", 1, E_UDEC), ("FRH", 2, E_UDEC), ("FON", 4, E_UTIMESTAMP), \
                         ("FOF", 4, E_UTIMESTAMP), ("FGS", 1, E_HEX), ("FLP", 1, E_UDEC), ("FDT", 1, E_UDEC), \
                         ("FHF-FFS", 1, E_UDEC), ("FHS-FTM", 1, E_UDEC)]), \
    (0xa1, "dlx01", 255, [("FGI", 6, E_HEX), ("FFW", 2, E_HEX), ("FSN", 4, E_UDEC), ("FXE", 1, E_HEX)]), \
    (0xa2, "dlx02", 255, [("FGI", 6, E_HEX), ("FFW", 2, E_HEX), ("FSN", 4, E_UDEC), ("FXE", 1, E_HEX)]), \
    (0xa3, "dlx03", 255, [("FGI", 6, E_HEX), ("FFW", 2, E_HEX), ("FSN", 4, E_UDEC), ("FXE", 1, E_HEX)]), \
    (0xa4, "dlx04", 255, [("FGI", 6, E_HEX), ("FFW", 2, E_HEX), ("FSN", 4, E_UDEC), ("FXE", 1, E_HEX)]), \
    (0xa5, "dlx05", 255, [("FGI", 6, E_HEX), ("FFW", 2, E_HEX), ("FSN", 4, E_UDEC), ("FXE", 1, E_HEX)]), \
    (0xa6, "dlx06", 255, [("FGI", 6, E_HEX), ("FFW", 2, E_HEX), ("FSN", 4, E_UDEC), ("FXE", 1, E_HEX)]), \
    (0xa7, "dlx07", 255, [("FGI", 6, E_HEX), ("FFW", 2, E_HEX), ("FSN", 4, E_UDEC), ("FXE", 1, E_HEX)]), \
    (0xa8, "dlx08", 255, [("FGI", 6, E_HEX), ("FFW", 2, E_HEX), ("FSN", 4, E_UDEC), ("FXE", 1, E_HEX)]), \
    (0xa9, "dlx09", 255, [("FGI", 6, E_HEX), ("FFW", 2, E_HEX), ("FSN", 4, E_UDEC), ("FXE", 1, E_HEX)]), \
    (0xaa, "dlx10", 255, [("FGI", 6, E_HEX), ("FFW", 2, E_HEX), ("FSN", 4, E_UDEC), ("FXE", 1, E_HEX)]), \
    (0xab, "dlx11", 255, [("FGI", 6, E_HEX), ("FFW", 2, E_HEX), ("FSN", 4, E_UDEC), ("FXE", 1, E_HEX)]), \
    (0xac, "dlx12", 255, [("FGI", 6, E_HEX), ("FFW", 2, E_HEX), ("FSN", 4, E_UDEC), ("FXE", 1, E_HEX)]), \
    (0xad, "dlx13", 255, [("FGI", 6, E_HEX), ("FFW", 2, E_HEX), ("FSN", 4, E_UDEC), ("FXE", 1, E_HEX)]), \
    (0xae, "dlx14", 255, [("FGI", 6, E_HEX), ("FFW", 2, E_HEX), ("FSN", 4, E_UDEC), ("FXE", 1, E_HEX)]), \
    (0xaf, "dlx15", 255, [("FGI", 6, E_HEX), ("FFW", 2, E_HEX), ("FSN", 4, E_UDEC), ("FXE", 1, E_HEX)]), \
    (0xff, "all", 0, []) \
    ]

config_tag_table = [ \
    (0x10, "SYS02", 8, [("TML", 1, E_UDEC)]), \
    (0x11, "SYS03", 4, []), \
    (0x12, "SYS04", 0, []), \
    (0x13, "SYS05", 4, []), \
    (0x14, "PHC01", 7, [("PPI", 1, E_HEX), ("PPX", 1, E_HEX), ("PED", 1, E_UDEC), ("PDD", 1, E_UDEC), \
                        ("POM", 1, E_HEX), ("PPM", 1, E_HEX), ("PAM", 1, E_HEX)]), \
    (0x19, "COM01", 4, [("0LH", 2, E_UDEC), ("0HH", 2, E_UDEC)]), \
    (0x20, "POS01", 7, [("PGR", 1, E_UDEC), ("PST", 1, E_UDEC), ("PPF", 1, E_UDEC), \
                        ("2LH", 2, E_UDEC), ("2HH", 2, E_UDEC)]), \
    (0x72, "PHX01", 255, []), \
    (0x73, "MPX01", 255, [("SNO", 12, E_STRING), ("MTY", 22, E_STRING), ("DED", 1, E_UDEC), ("DDD", 1, E_UDEC)]), \
    (0x74, "ENX01", 16, [("SAG", 1, E_UDEC), \
                         ("APG", 3, E_HEX), ("APO", 3, E_HEX), ("AIG", 3, E_HEX), ("AIO", 3, E_HEX),
                         ("AVG", 3, E_HEX)]), \
    (0x75, "SYX01", 255, [("SDB", 1, E_HEX), ("1VA", 1, E_UDEC), ("1VB", 1, E_UDEC), ("1VC", 1, E_SDEC)]), \
    (0x76, "ANX01", 255, [("MXT", 1, E_UDEC), ("MIT", 1, E_UDEC)]), \
    (0x78, "I2X01", 0, []), \
    (0x80, "DLS01", 1, [("PRP", 1, E_UDEC)]), \
    (0x81, "dld01", 8, [("PGR", 1, E_UDEC), ("PDI", 1, E_UDEC), ("PFD", 1, E_UDEC), \
                        ("PWO", 1, E_UDEC), ("PMF", 1, E_UDEC), ("PRH", 2, E_UDEC), ("PLD", 1, E_UDEC)]), \
    (0x82, "dld02", 8, [("PGR", 1, E_UDEC), ("PDI", 1, E_UDEC), ("PFD", 1, E_UDEC), \
                        ("PWO", 1, E_UDEC), ("PMF", 1, E_UDEC), ("PRH", 2, E_UDEC), ("PLD", 1, E_UDEC)]), \
    (0x83, "dld03", 8, [("PGR", 1, E_UDEC), ("PDI", 1, E_UDEC), ("PFD", 1, E_UDEC), \
                        ("PWO", 1, E_UDEC), ("PMF", 1, E_UDEC), ("PRH", 2, E_UDEC), ("PLD", 1, E_UDEC)]), \
    (0x84, "dld04", 8, [("PGR", 1, E_UDEC), ("PDI", 1, E_UDEC), ("PFD", 1, E_UDEC), \
                        ("PWO", 1, E_UDEC), ("PMF", 1, E_UDEC), ("PRH", 2, E_UDEC), ("PLD", 1, E_UDEC)]), \
    (0x85, "dld05", 8, [("PGR", 1, E_UDEC), ("PDI", 1, E_UDEC), ("PFD", 1, E_UDEC), \
                        ("PWO", 1, E_UDEC), ("PMF", 1, E_UDEC), ("PRH", 2, E_UDEC), ("PLD", 1, E_UDEC)]), \
    (0x86, "dld06", 8, [("PGR", 1, E_UDEC), ("PDI", 1, E_UDEC), ("PFD", 1, E_UDEC), \
                        ("PWO", 1, E_UDEC), ("PMF", 1, E_UDEC), ("PRH", 2, E_UDEC), ("PLD", 1, E_UDEC)]), \
    (0x87, "dld07", 8, [("PGR", 1, E_UDEC), ("PDI", 1, E_UDEC), ("PFD", 1, E_UDEC), \
                        ("PWO", 1, E_UDEC), ("PMF", 1, E_UDEC), ("PRH", 2, E_UDEC), ("PLD", 1, E_UDEC)]), \
    (0x88, "dld08", 8, [("PGR", 1, E_UDEC), ("PDI", 1, E_UDEC), ("PFD", 1, E_UDEC), \
                        ("PWO", 1, E_UDEC), ("PMF", 1, E_UDEC), ("PRH", 2, E_UDEC), ("PLD", 1, E_UDEC)]), \
    (0x89, "dld09", 8, [("PGR", 1, E_UDEC), ("PDI", 1, E_UDEC), ("PFD", 1, E_UDEC), \
                        ("PWO", 1, E_UDEC), ("PMF", 1, E_UDEC), ("PRH", 2, E_UDEC), ("PLD", 1, E_UDEC)]), \
    (0x8a, "dld10", 8, [("PGR", 1, E_UDEC), ("PDI", 1, E_UDEC), ("PFD", 1, E_UDEC), \
                        ("PWO", 1, E_UDEC), ("PMF", 1, E_UDEC), ("PRH", 2, E_UDEC), ("PLD", 1, E_UDEC)]), \
    (0x8b, "dld11", 8, [("PGR", 1, E_UDEC), ("PDI", 1, E_UDEC), ("PFD", 1, E_UDEC), \
                        ("PWO", 1, E_UDEC), ("PMF", 1, E_UDEC), ("PRH", 2, E_UDEC), ("PLD", 1, E_UDEC)]), \
    (0x8c, "dld12", 8, [("PGR", 1, E_UDEC), ("PDI", 1, E_UDEC), ("PFD", 1, E_UDEC), \
                        ("PWO", 1, E_UDEC), ("PMF", 1, E_UDEC), ("PRH", 2, E_UDEC), ("PLD", 1, E_UDEC)]), \
    (0x8d, "dld13", 8, [("PGR", 1, E_UDEC), ("PDI", 1, E_UDEC), ("PFD", 1, E_UDEC), \
                        ("PWO", 1, E_UDEC), ("PMF", 1, E_UDEC), ("PRH", 2, E_UDEC), ("PLD", 1, E_UDEC)]), \
    (0x8e, "dld14", 8, [("PGR", 1, E_UDEC), ("PDI", 1, E_UDEC), ("PFD", 1, E_UDEC), \
                        ("PWO", 1, E_UDEC), ("PMF", 1, E_UDEC), ("PRH", 2, E_UDEC), ("PLD", 1, E_UDEC)]), \
    (0x8f, "dld15", 8, [("PGR", 1, E_UDEC), ("PDI", 1, E_UDEC), ("PFD", 1, E_UDEC), \
                        ("PWO", 1, E_UDEC), ("PMF", 1, E_UDEC), ("PRH", 2, E_UDEC), ("PLD", 1, E_UDEC)]), \
    (0xa1, "dlx01", 0, []), \
    (0xa2, "dlx02", 0, []), \
    (0xa3, "dlx03", 0, []), \
    (0xa4, "dlx04", 0, []), \
    (0xa5, "dlx05", 0, []), \
    (0xa6, "dlx06", 0, []), \
    (0xa7, "dlx07", 0, []), \
    (0xa8, "dlx08", 0, []), \
    (0xa9, "dlx09", 0, []), \
    (0xaa, "dlx10", 0, []), \
    (0xab, "dlx11", 0, []), \
    (0xac, "dlx12", 0, []), \
    (0xad, "dlx13", 0, []), \
    (0xae, "dlx14", 0, []), \
    (0xaf, "dlx15", 0, []), \
    (0xff, "all", 0, []), \
    ]


# class ZBSender(object, ):
#     """Sends a chunk to a recipient and waits for an acknowledgement"""
#
#     def __init__(self, recipient):
#         super(ZBSender, self).__init__()
#
#         recipient = recipient.strip("[]!")
#         recipient = "[" + recipient + "]!"
#
#         self.__timeout = 0.02
#         self.__xbversion = None
#
#         self.__recipients = []
#         self.__recipient = (recipient, ZIGBEE_SERIAL_EP, 0xc105, 0x11)
#         self.__recipients.append(self.__recipient)
#         self.__sd = self.__get_serial_endpoint_socket()
#
#         self.__netresettarget = (ZIGBEE_NETRESET_EP, 0xc105, 0x22)
#         self.__sdr = self.__get_netreset_endpoint_socket()
#
#         self.__pingtarget = (recipient, ZIGBEE_SERIAL_EP, 0xc105, 0x12, socket.XBS_OPT_TX_NOACK)
#
#         self.__clirecipients = []
#         self.__clirecipient = (recipient, ZIGBEE_CLI_EP, 0xc105, 0x11)
#         self.__clirecipients.append(self.__clirecipient)
#         self.__sdcli = self.__seco_get_cli_endpoint_socket()
#
#         self.__tagrecipients = []
#         self.__tagrecipient = (recipient, ZIGBEE_TAG_EP, 0xc105, 0x11)
#         self.__tagrecipients.append(self.__tagrecipient)
#         self.__sdtag = self.__seco_get_tag_endpoint_socket()
#
#     def IsBroadcast(self):
#         if self.__recipient[0] == "[00:00:00:00:00:00:ff:ff]!":
#             return True
#         else:
#             return False
#
#     def changeaddr(self, recipient):
#         if not recipient.endswith("!"):
#             recipient = "[" + recipient + "]!"
#         self.__recipient = (recipient, 0xe8, 0xc105, 0x11)
#         self.__recipients = []
#         self.__recipients.append(self.__recipient)
#         self.__clirecipients = []
#         self.__clirecipient = (recipient, ZIGBEE_CLI_EP, 0xc105, 0x11)
#         self.__clirecipients.append(self.__clirecipient)
#         self.__tagrecipients = []
#         self.__tagrecipient = (recipient, ZIGBEE_TAG_EP, 0xc105, 0x11)
#         self.__tagrecipients.append(self.__tagrecipient)
#         self.__pingtarget = (recipient, 0xe8, 0xc105, 0x12)
#
#     def addaddr(self, recipient):
#         if not recipient.endswith("!"):
#             recipient = "[" + recipient + "]!"
#         self.__recipients.append((recipient, ZIGBEE_SERIAL_EP, 0xc105, 0x11))
#         self.__clirecipients.append((recipient, ZIGBEE_CLI_EP, 0xc105, 0x11))
#         self.__tagrecipients.append((recipient, ZIGBEE_TAG_EP, 0xc105, 0x11))
#
#     def set_timeout(self, timeout):
#         try:
#             value = float(timeout)
#             self.__timeout = value
#         except:
#             print("value %s not understood" % timeout)
#
#     def parity(self, message):
#         """generates the parity-byte for the given message, as string"""
#         parity = 0
#         for b in message:
#             # this is a one char string, xor it's nurmeric value
#             parity = parity ^ ord(b)
#         return chr(parity)
#
#     def send(self, chunk):
#         """
#         sends the chunk in a message to the recipient
#         """
#         self.__chunk = chunk
#         for recipient in self.__recipients:
#             print("S ", repr(recipient[0]), ": ", binascii.hexlify(self.__chunk))
#             self.__sd.sendto(self.__chunk, 0, recipient)
#
#     def sendcli(self, chunk):
#         """
#         sends the chunk in a message to the recipient
#         """
#         self.__chunk = chunk
#         for recipient in self.__clirecipients:
#             print("S ", repr(recipient[0]), ": ", binascii.hexlify(self.__chunk))
#             self.__sdcli.sendto(self.__chunk, 0, recipient)
#
#     def sendtag(self, chunk):
#         """
#         sends the chunk in a message to the recipient
#         """
#         self.__chunk = chunk
#         for recipient in self.__tagrecipients:
#             print("S ", repr(recipient[0]), ": ", binascii.hexlify(self.__chunk))
#             self.__sdtag.sendto(self.__chunk, 0, recipient)
#
#     def send_netreset(self):
#         target = (self.__recipient[0], self.__netresettarget[0], self.__netresettarget[1], self.__netresettarget[2])
#         try:
#             if (self.__sdr.sendto(chr(0), 0, target) < 1):
#                 print(" %011.3f: not all characters sent" % (time.process_time()))
#                 return -1
#         except:
#             print(" Send not confirmed, but it is not expected to")
#
#         return 0
#
#     def XB_ping(self, num_bytes=16):
#         """
#         tries to send a ping to the node and reports if APS Tx ack was received or not
#         returns:
#         number of replies received
#         """
#         global Tx_count
#         ping_msg = ""
#         for i in range(num_bytes):
#             ping_msg += "".join(chr(i))
#
#         # logging.info("  %011.3f: P %s %s" % (time.process_time(), repr(self.__pingtarget), binascii.hexlify(ping_msg)))
#         # self.__sd.setsockopt(XBS_SOL_EP, XBS_SO_EP_SYNC_TX, 1)
#         try:
#             self.__sd.sendto(ping_msg, 0, self.__pingtarget)
#             # print "%011.3f: ping sent ok" % time.process_time()
#             Tx_count += 1
#         except:
#             # if XBS_SO_EP_SYNC_TX is set as option, the sendto will block
#             # and if the send fails we will get an exception meaning
#             # the send message was not confirmed to be delivered
#             print("%011.3f: ping message delivery failed" % time.process_time())
#             # still there is a remote change the ack never got back
#             # so we briefly try if a reply maybe got in
#
#         # self.__sd.setsockopt(XBS_SOL_EP, XBS_SO_EP_SYNC_TX, 0)
#
#     def recv(self):
#         """
#         receives the response of the recipient for validation and acknowledgement
#         """
#         self.__sd.settimeout(self.__timeout)
#         try:
#             data, src_addr = self.__sd.recvfrom(255)
#             report_tag(src_addr, data)
#         except:
#             pass
#
#     def recvcli(self):
#         """
#         receives the response of the recipient for validation and acknowledgement
#         """
#         global nextclitime
#         self.__sdcli.settimeout(self.__timeout)
#         try:
#             data, src_addr = self.__sdcli.recvfrom(255)
#             report_cli(src_addr, data)
#         except:
#             if nextclitime != 0.0 and nextclitime < time.process_time():
#                 # time out on CLI command
#                 nextclitime = 1.0
#             pass
#
#     def recvtag(self):
#         """
#         receives the response of the recipient for validation and acknowledgement
#         """
#         self.__sdtag.settimeout(self.__timeout)
#         try:
#             data, src_addr = self.__sdtag.recvfrom(255)
#             report_tag(src_addr, data)
#         except:
#             pass
#
#     def display_nodes(self, sorton="l", discover=False):
#         global node_list
#         try:
#             print("%011.3f: collecting nodes, please be patient" % (time.process_time()))
#             zigbee.get_node_list(refresh=False, discover_digi=discover)
#         except Exception as err:
#             print(" %011.3f: Error collecting nodes %s, waiting one minute" % (time.process_time(), str(err)))
#             time.sleep(60)
#         except:
#             print(" %011.3f: Unknown except" % (time.process_time()))
#             traceback.print_exc(file=sys.stdout)
#
#         try:
#             node_list = zigbee.get_node_list(refresh=False, discover_digi=False, discover_zigbee=False)
#         except socket.timeout as timeout:
#             print(" %011.3f: Get Node list failed: %s" % (time.process_time(), str(timeout)))
#             pass
#         except:
#             print(" %011.3f: Unknown except" % (time.process_time()))
#             traceback.print_exc(file=sys.stdout)
#         # sort the list
#         sorted_node_list = sorted(node_list, key=lambda node: node.label)
#         if sorton == "t":
#             sorted_node_list = sorted(sorted_node_list, key=lambda node: node.type)
#         elif sorton == "s":
#             sorted_node_list = sorted(sorted_node_list, key=lambda node: node.addr_short)
#         elif sorton == "e":
#             sorted_node_list = sorted(sorted_node_list, key=lambda node: node.addr_extended)
#         elif sorton == "h":
#             sorted_node_list = sorted(sorted_node_list, key=lambda node: len(node.source_route))
#         # Print the table:
#         print("%-20s %-12s %-8s %-24s %-14s" % ("Label", "Type", "Short", "Extended", "Number of Hops"))
#         print("%-20s %-12s %-8s %-24s %-14s" % ("-" * 20, "-" * 12, "-" * 8, "-" * 24, "-" * 14))
#         for node in sorted_node_list:
#             print("%-20s %-12s %-8s %-24s %3d" % (
#             node.label, node.type, node.addr_short, node.addr_extended, len(node.source_route)))
#
#     def reset_mcu(self):
#         num_retries = 2
#         RESTART_INTERVAL = 2.0
#         if self.__recipient[0] == "[00:00:00:00:00:00:ff:ff]!":
#             print("Sorry not supported in broadcast mode")
#             return
#         print("Trying to reset MCU on node %s" % (self.__recipient[0]))
#         # use new universla mode compatible also with PiCo
#         # reset which works on both Lu/CoCo and PiCo:
#         # set D2 high
#         # set D1 low (PiCo resets)
#         # set D1 high impedance
#         # set D2 low (Lu/CoCo reset)
#         # set D2 high impedance
#         for tries in range(num_retries):
#             try:
#                 if not zigbee.ddo_set_param(self.__recipient[0], 'D2', 5, TX_TIMEOUT):
#                     print("Setting D2 line to high failed")
#                 else:
#                     print("Set D2 line high line: success")
#                     break
#             except socket.timeout:
#                 pass
#             except Exception:
#                 pass
#             except Exception:
#                 pass
#             except:
#                 print("Unknown except")
#                 traceback.print_exc(file=sys.stdout)
#         else:
#             print("Setting D2 line to high failed")
#             return 1
#
#         for tries in range(num_retries):
#             try:
#                 if not zigbee.ddo_set_param(self.__recipient[0], 'D1', 4, TX_TIMEOUT):
#                     print("Setting D1 line to low failed")
#                 else:
#                     print("Set D1 line low line: success")
#                     break
#             except socket.timeout:
#                 pass
#             except Exception:
#                 pass
#             except Exception:
#                 pass
#             except:
#                 traceback.print_exc(file=sys.stdout)
#         else:
#             print("Setting D1 line to low failed")
#             return 1
#
#         for tries in range(num_retries):
#             try:
#                 if not zigbee.ddo_set_param(self.__recipient[0], 'D1', 0, TX_TIMEOUT):
#                     print("Setting D1 line back to default failed")
#                 else:
#                     print("Set D1 line to default: success")
#                     break
#             except socket.timeout:
#                 pass
#             except Exception:
#                 pass
#             except Exception:
#                 pass
#             except:
#                 traceback.print_exc(file=sys.stdout)
#         else:
#             print("Setting D1 line to default failed")
#             return 1
#
#         for tries in range(num_retries):
#             try:
#                 if not zigbee.ddo_set_param(self.__recipient[0], 'D2', 4, TX_TIMEOUT):
#                     print("Setting D2 reset failed")
#                 else:
#                     print("Set D2 line to low: success")
#                     break
#             except socket.timeout:
#                 pass
#             except Exception:
#                 pass
#             except Exception:
#                 pass
#             except:
#                 traceback.print_exc(file=sys.stdout)
#         else:
#             print("Set D2 line to low failed")
#             return 1
#
#         for tries in range(num_retries):
#             try:
#                 if not zigbee.ddo_set_param(self.__recipient[0], 'D2', 0, TX_TIMEOUT):
#                     print("Clearing reset failed")
#                 else:
#                     print("Clear reset line: success, pausing to allow MCU to come back")
#                     time.sleep(RESTART_INTERVAL)
#                     return 0
#             except socket.timeout:
#                 # logging.info("BD Reset failed: %s" % str(timeout))
#                 pass
#             except Exception:
#                 pass
#             except Exception:
#                 pass
#             except:
#                 traceback.print_exc(file=sys.stdout)
#
#         print("Clear reset line failed")
#         return
#
#     def __xb_get_radio_series(self, addr_extended=None):
#         self.__xbversion = self.__xb_get_radio_version(addr_extended)
#         hwversion = self.__xbversion[0]
#         if hwversion >= 0x2000:
#             self.__xbhw = "S2C"
#             return 3
#         elif hwversion >= 0x1E00:
#             self.__xbhw = "S2B"
#             return 2
#         elif hwversion >= 0x1900:
#             self.__xbhw = "S2"
#             return 2
#         elif hwversion > 0x0000:
#             self.__xbhw = "XBee"
#             return 1
#         self.__xbhw = "unknown"
#         return 0
#
#     def __get_hwversion(self, addr_extended):
#         if addr_extended == None:
#             retries = 99  # sometimes we have a non responsive coordinator.....
#         else:
#             #            logging.debug("Trying to read XBee VR from %s" % addr_extended)
#             print("Trying to read XBee HV from %s" % addr_extended)
#             retries = num_retries
#         hv = 0
#         for tries in range(retries):
#             try:
#                 hv = zigbee.ddo_get_param(addr_extended, 'HV', TX_TIMEOUT)
#                 return struct.unpack('H', hv)[0]
#             except socket.timeout:
#                 if addr_extended:
#                     #                    logging.debug("HV read timeout")
#                     print("HV read timeout")
#                 if tries < retries - 1:
#                     time.sleep(RETRY_INTERVAL)
#             except:
#                 if addr_extended == None:
#                     #                    logging.debug("coordinator hangs retrieving its Hardware Version %s, waiting....." % repr(hv))
#                     print("coordinator hangs retrieving its Hardware Version %s, waiting....." % repr(hv))
#                 else:
#                     #                    logging.debug("HV ddo get failed")
#                     print("HV ddo get failed")
#                 if tries < retries - 1:
#                     time.sleep(RETRY_INTERVAL)
#         return 0x0000
#
#     def __get_fwversion(self, addr_extended):
#         if addr_extended == None:
#             retries = 99  # sometimes we have a non responsive coordinator.....
#         else:
#             #            logging.debug("Trying to read XBee VR from %s" % addr_extended)
#             print("Trying to read XBee VR from %s" % addr_extended)
#             retries = num_retries
#         vr = 0
#         for tries in range(retries):
#             try:
#                 vr = zigbee.ddo_get_param(addr_extended, 'VR', TX_TIMEOUT)
#                 return struct.unpack('H', vr)[0]
#             except socket.timeout:
#                 if addr_extended:
#                     #                    logging.debug("VR read timeout")
#                     print("VR read timeout")
#                 if tries < retries - 1:
#                     time.sleep(RETRY_INTERVAL)
#             except:
#                 if addr_extended == None:
#                     #                    logging.debug("coordinator hangs retrieving its Firmware Version, waiting....." % repr(vr))
#                     print("coordinator hangs retrieving its Firmware Version %s, waiting....." % repr(vr))
#                 else:
#                     #                    logging.debug("VR ddo get failed")
#                     print("VR ddo get failed")
#                 if tries < retries - 1:
#                     time.sleep(RETRY_INTERVAL)
#         return 0x0000
#
#     def __xb_get_radio_version(self, addr_extended=None):
#         hwversion = self.__get_hwversion(addr_extended)
#         fwversion = self.__get_fwversion(addr_extended)
#         return (hwversion, fwversion)
#
#     def __get_netreset_endpoint_socket(self):
#         sdr = socket.socket(socket.AF_ZIGBEE, socket.SOCK_DGRAM, socket.ZBS_PROT_TRANSPORT)
#         try:
#             sdr.bind(('', 0xe6, 0x0, 0x0))
#         except Exception as err:
#             print("NETRESET: binding socket failed, using fallback")
#             sdr.bind(('', 0xc6, 0x0, 0x0))
#         # socket timeout only applies to receive
#         sdr.settimeout(3.0)
#         # set blocking sendto() option so we are sure we know if and if succesful sent
#         return sdr
#
#     def __get_serial_endpoint_socket(self):
#         global StealthMode
#         sd = socket.socket(socket.AF_ZIGBEE, socket.SOCK_DGRAM, socket.ZBS_PROT_TRANSPORT)
#         if self.__xb_get_radio_series() == 1:
#             sd.bind(('', 0x0, 0x0, 0x0))
#         else:
#             try:
#                 sd.bind(('', 0xe8, 0x0, 0x0))
#                 StealthMode = False
#             except Exception as err:
#                 print("ZIGBEE: binding socket failed. likely Owlet.py running, performing fallback to stealth mode")
#                 sd.bind(('', 0xcf, 0x0, 0x0))
#                 StealthMode = True
#         sd.settimeout(RESPONSE_TIMEOUT)
#         return sd
#
#     def __seco_get_cli_endpoint_socket(self):
#         sdcli = socket.socket(socket.AF_ZIGBEE, socket.SOCK_DGRAM, socket.ZBS_PROT_TRANSPORT)
#         try:
#             sdcli.bind(('', ZIGBEE_CLI_EP, 0x0, 0x0))
#         except Exception as err:
#             print("binding to CLI socket failed")
#         # socket timeout only applies to receive
#         sdcli.settimeout(RESPONSE_TIMEOUT)
#         return sdcli
#
#     def __seco_get_tag_endpoint_socket(self):
#         sdtag = socket.socket(socket.AF_ZIGBEE, socket.SOCK_DGRAM, socket.ZBS_PROT_TRANSPORT)
#         try:
#             sdtag.bind(('', ZIGBEE_TAG_EP, 0x0, 0x0))
#         except Exception as err:
#             print("binding to TAG socket failed")
#         # socket timeout only applies to receive
#         sdtag.settimeout(RESPONSE_TIMEOUT)
#         return sdtag


class ZBSender(object, ):
    """Sends a chunk to a recipient and waits for an acknowledgement"""

    def __init__(self, recipient):
        super(ZBSender, self).__init__()

        recipient = recipient.strip("[]!")
        recipient = "[" + recipient + "]!"

        self.__timeout = 0.02
        self.__xbversion = None

        self.__recipients = []
        self.__recipient = (recipient, ZIGBEE_SERIAL_EP, 0xc105, 0x11)
        self.__recipients.append(self.__recipient)
        self.__sd = self.__get_serial_endpoint_socket()

        self.__netresettarget = (ZIGBEE_NETRESET_EP, 0xc105, 0x22)
        # self.__sdr = self.__get_netreset_endpoint_socket()
        # TODO: correct
        self.__sdr = None

        # self.__pingtarget = (recipient, ZIGBEE_SERIAL_EP, 0xc105, 0x12, socket.XBS_OPT_TX_NOACK)
        # TODO: correct
        self.__pingtarget = None

        self.__clirecipients = []
        self.__clirecipient = (recipient, ZIGBEE_CLI_EP, 0xc105, 0x11)
        self.__clirecipients.append(self.__clirecipient)
        # self.__sdcli = self.__seco_get_cli_endpoint_socket()
        # TODO: correct
        self.__sdcli = self.__sd
        # self.__sdcli = None

        self.__tagrecipients = []
        self.__tagrecipient = (recipient, ZIGBEE_TAG_EP, 0xc105, 0x11)
        self.__tagrecipients.append(self.__tagrecipient)
        # self.__sdtag = self.__seco_get_tag_endpoint_socket()
        # TODO: correct
        self.__sdtag = None

    def IsBroadcast(self):
        if self.__recipient[0] == "[00:00:00:00:00:00:ff:ff]!":
            return True
        else:
            return False

    def changeaddr(self, recipient):
        if not recipient.endswith("!"):
            recipient = "[" + recipient + "]!"
        self.__recipient = (recipient, 0xe8, 0xc105, 0x11)
        self.__recipients = []
        self.__recipients.append(self.__recipient)
        self.__clirecipients = []
        self.__clirecipient = (recipient, ZIGBEE_CLI_EP, 0xc105, 0x11)
        self.__clirecipients.append(self.__clirecipient)
        self.__tagrecipients = []
        self.__tagrecipient = (recipient, ZIGBEE_TAG_EP, 0xc105, 0x11)
        self.__tagrecipients.append(self.__tagrecipient)
        self.__pingtarget = (recipient, 0xe8, 0xc105, 0x12)

    def addaddr(self, recipient):
        if not recipient.endswith("!"):
            recipient = "[" + recipient + "]!"
        self.__recipients.append((recipient, ZIGBEE_SERIAL_EP, 0xc105, 0x11))
        self.__clirecipients.append((recipient, ZIGBEE_CLI_EP, 0xc105, 0x11))
        self.__tagrecipients.append((recipient, ZIGBEE_TAG_EP, 0xc105, 0x11))

    def set_timeout(self, timeout):
        try:
            value = float(timeout)
            self.__timeout = value
        except:
            print("value %s not understood" % timeout)

    def parity(self, message):
        """generates the parity-byte for the given message, as string"""
        parity = 0
        for b in message:
            # this is a one char string, xor it's nurmeric value
            parity = parity ^ ord(b)
        return chr(parity)

    def send(self, chunk):
        """
        sends the chunk in a message to the recipient
        """
        self.__chunk = chunk
        for recipient in self.__recipients:
            print("S ", repr(recipient[0]), ": ", binascii.hexlify(self.__chunk))
            self.__sd.sendto(self.__chunk, 0, recipient)

    def sendcli(self, chunk):
        """
        sends the chunk in a message to the recipient
        """
        self.__chunk = chunk
        for recipient in self.__clirecipients:
            print(f"S %r: %s" % (repr(recipient[0]), binascii.hexlify(self.__chunk.encode("ascii"))))
            remote_xbee = RemoteZigBeeDevice(self.__sdcli,
                                             XBee64BitAddress.from_hex_string(recipient[0].replace(":", "")[1:-2]))
            # self.__sdcli.send_data_async(remote_xbee, self.__chunk)
            #TODO: check correct cli
            self.__sdcli.send_expl_data_async(remote_xbee, self.__chunk, 0xB3, 0xB3, 0x0011, 0xC105)

    def sendtag(self, chunk):
        """
        sends the chunk in a message to the recipient
        """
        self.__chunk = chunk
        for recipient in self.__tagrecipients:
            print("S ", repr(recipient[0]), ": ", binascii.hexlify(self.__chunk))
            self.__sdtag.sendto(self.__chunk, 0, recipient)

    def send_netreset(self):
        target = (self.__recipient[0], self.__netresettarget[0], self.__netresettarget[1], self.__netresettarget[2])
        try:
            if (self.__sdr.sendto(chr(0), 0, target) < 1):
                print(" %011.3f: not all characters sent" % (time.process_time()))
                return -1
        except:
            print(" Send not confirmed, but it is not expected to")

        return 0

    def XB_ping(self, num_bytes=16):
        """
        tries to send a ping to the node and reports if APS Tx ack was received or not
        returns:
        number of replies received
        """
        global Tx_count
        ping_msg = ""
        for i in range(num_bytes):
            ping_msg += "".join(chr(i))

        # logging.info("  %011.3f: P %s %s" % (time.process_time(), repr(self.__pingtarget), binascii.hexlify(ping_msg)))
        # self.__sd.setsockopt(XBS_SOL_EP, XBS_SO_EP_SYNC_TX, 1)
        try:
            self.__sd.sendto(ping_msg, 0, self.__pingtarget)
            # print "%011.3f: ping sent ok" % time.process_time()
            Tx_count += 1
        except:
            # if XBS_SO_EP_SYNC_TX is set as option, the sendto will block
            # and if the send fails we will get an exception meaning
            # the send message was not confirmed to be delivered
            print("%011.3f: ping message delivery failed" % time.process_time())
            # still there is a remote change the ack never got back
            # so we briefly try if a reply maybe got in

        # self.__sd.setsockopt(XBS_SOL_EP, XBS_SO_EP_SYNC_TX, 0)

    def recv_explicit(self, expl_xbee_message):
        # Define callback.
        src_addr = expl_xbee_message.remote_device.get_64bit_addr()
        src_addr_hex = ":".join(["%02x" % x for x in src_addr.address])
        source_endpoint = expl_xbee_message.source_endpoint
        dest_endpoint = expl_xbee_message.dest_endpoint
        cluster = expl_xbee_message.cluster_id
        profile = expl_xbee_message.profile_id
        try:
            data = expl_xbee_message.data
        except Exception as e:
            print(f"Error in explicit data: {e}")

        try:
            print("%s" % data.decode("utf-8"), end="")
        except Exception as e:
            print(f"Error printing")

        # add report data
        report_tag(expl_xbee_message, data)
        # print("Received explicit data from %s: %s" % (address, data))

    def recv(self):
        """
        receives the response of the recipient for validation and acknowledgement
        """

        self.__sd.serial_port.set_read_timeout(self.__timeout)
        try:
            message = self.__sd.read_data(self.__timeout)
            src_addr = message.remote_device
            data = message
            # TODO: needs to be changed
            # data, src_addr = self.__sd.recvfrom(255)
            print(f"data: {message.data}")
            report_tag(src_addr, data)
        except TimeoutException:
            pass
        except Exception as e:
            print(f"Error in data read: {e}")

    def recvcli(self):
        """
        receives the response of the recipient for validation and acknowledgement
        """
        global nextclitime
        self.__sdcli.settimeout(self.__timeout)
        try:
            data, src_addr = self.__sdcli.recvfrom(255)
            report_cli(src_addr, data)
        except:
            if nextclitime != 0.0 and nextclitime < time.process_time():
                # time out on CLI command
                nextclitime = 1.0
            pass

    def recvtag(self):
        """
        receives the response of the recipient for validation and acknowledgement
        """
        self.__sdtag.settimeout(self.__timeout)
        try:
            data, src_addr = self.__sdtag.recvfrom(255)
            report_tag(src_addr, data)
        except:
            pass

    def display_nodes(self, sorton="l", discover=False):
        pass
        # global node_list
        # try:
        #     print("%011.3f: collecting nodes, please be patient" % (time.process_time()))
        #     zigbee.get_node_list(refresh=False, discover_digi=discover)
        # except Exception as err:
        #     print(" %011.3f: Error collecting nodes %s, waiting one minute" % (time.process_time(), str(err)))
        #     time.sleep(60)
        # except:
        #     print(" %011.3f: Unknown except" % (time.process_time()))
        #     traceback.print_exc(file=sys.stdout)
        #
        # try:
        #     node_list = zigbee.get_node_list(refresh=False, discover_digi=False, discover_zigbee=False)
        # except socket.timeout as timeout:
        #     print(" %011.3f: Get Node list failed: %s" % (time.process_time(), str(timeout)))
        #     pass
        # except:
        #     print(" %011.3f: Unknown except" % (time.process_time()))
        #     traceback.print_exc(file=sys.stdout)
        # # sort the list
        # sorted_node_list = sorted(node_list, key=lambda node: node.label)
        # if sorton == "t":
        #     sorted_node_list = sorted(sorted_node_list, key=lambda node: node.type)
        # elif sorton == "s":
        #     sorted_node_list = sorted(sorted_node_list, key=lambda node: node.addr_short)
        # elif sorton == "e":
        #     sorted_node_list = sorted(sorted_node_list, key=lambda node: node.addr_extended)
        # elif sorton == "h":
        #     sorted_node_list = sorted(sorted_node_list, key=lambda node: len(node.source_route))
        # # Print the table:
        # print("%-20s %-12s %-8s %-24s %-14s" % ("Label", "Type", "Short", "Extended", "Number of Hops"))
        # print("%-20s %-12s %-8s %-24s %-14s" % ("-" * 20, "-" * 12, "-" * 8, "-" * 24, "-" * 14))
        # for node in sorted_node_list:
        #     print("%-20s %-12s %-8s %-24s %3d" % (
        #         node.label, node.type, node.addr_short, node.addr_extended, len(node.source_route)))

    def reset_mcu(self):
        num_retries = 2
        RESTART_INTERVAL = 2.0
        if self.__recipient[0] == "[00:00:00:00:00:00:ff:ff]!":
            print("Sorry not supported in broadcast mode")
            return
        print("Trying to reset MCU on node %s" % (self.__recipient[0]))
        # use new universla mode compatible also with PiCo
        # reset which works on both Lu/CoCo and PiCo:
        # set D2 high
        # set D1 low (PiCo resets)
        # set D1 high impedance
        # set D2 low (Lu/CoCo reset)
        # set D2 high impedance
        for tries in range(num_retries):
            try:
                if not zigbee.ddo_set_param(self.__recipient[0], 'D2', 5, TX_TIMEOUT):
                    print("Setting D2 line to high failed")
                else:
                    print("Set D2 line high line: success")
                    break
            except socket.timeout:
                pass
            except Exception:
                pass
            except Exception:
                pass
            except:
                print("Unknown except")
                traceback.print_exc(file=sys.stdout)
        else:
            print("Setting D2 line to high failed")
            return 1

        for tries in range(num_retries):
            try:
                if not zigbee.ddo_set_param(self.__recipient[0], 'D1', 4, TX_TIMEOUT):
                    print("Setting D1 line to low failed")
                else:
                    print("Set D1 line low line: success")
                    break
            except socket.timeout:
                pass
            except Exception:
                pass
            except Exception:
                pass
            except:
                traceback.print_exc(file=sys.stdout)
        else:
            print("Setting D1 line to low failed")
            return 1

        for tries in range(num_retries):
            try:
                if not zigbee.ddo_set_param(self.__recipient[0], 'D1', 0, TX_TIMEOUT):
                    print("Setting D1 line back to default failed")
                else:
                    print("Set D1 line to default: success")
                    break
            except socket.timeout:
                pass
            except Exception:
                pass
            except Exception:
                pass
            except:
                traceback.print_exc(file=sys.stdout)
        else:
            print("Setting D1 line to default failed")
            return 1

        for tries in range(num_retries):
            try:
                if not zigbee.ddo_set_param(self.__recipient[0], 'D2', 4, TX_TIMEOUT):
                    print("Setting D2 reset failed")
                else:
                    print("Set D2 line to low: success")
                    break
            except socket.timeout:
                pass
            except Exception:
                pass
            except Exception:
                pass
            except:
                traceback.print_exc(file=sys.stdout)
        else:
            print("Set D2 line to low failed")
            return 1

        for tries in range(num_retries):
            try:
                if not zigbee.ddo_set_param(self.__recipient[0], 'D2', 0, TX_TIMEOUT):
                    print("Clearing reset failed")
                else:
                    print("Clear reset line: success, pausing to allow MCU to come back")
                    time.sleep(RESTART_INTERVAL)
                    return 0
            except socket.timeout:
                # logging.info("BD Reset failed: %s" % str(timeout))
                pass
            except Exception:
                pass
            except Exception:
                pass
            except:
                traceback.print_exc(file=sys.stdout)

        print("Clear reset line failed")
        return

    def __xb_get_radio_series(self, addr_extended=None):
        self.__xbversion = self.__xb_get_radio_version(addr_extended)
        hwversion = self.__xbversion[0]
        if hwversion >= 0x2000:
            self.__xbhw = "S2C"
            return 3
        elif hwversion >= 0x1E00:
            self.__xbhw = "S2B"
            return 2
        elif hwversion >= 0x1900:
            self.__xbhw = "S2"
            return 2
        elif hwversion > 0x0000:
            self.__xbhw = "XBee"
            return 1
        self.__xbhw = "unknown"
        return 0

    def __get_hwversion(self, addr_extended):
        if addr_extended == None:
            retries = 99  # sometimes we have a non responsive coordinator.....
        else:
            #            logging.debug("Trying to read XBee VR from %s" % addr_extended)
            print("Trying to read XBee HV from %s" % addr_extended)
            retries = num_retries
        hv = 0
        for tries in range(retries):
            try:
                hv = zigbee.ddo_get_param(addr_extended, 'HV', TX_TIMEOUT)
                return struct.unpack('H', hv)[0]
            except socket.timeout:
                if addr_extended:
                    #                    logging.debug("HV read timeout")
                    print("HV read timeout")
                if tries < retries - 1:
                    time.sleep(RETRY_INTERVAL)
            except:
                if addr_extended == None:
                    #                    logging.debug("coordinator hangs retrieving its Hardware Version %s, waiting....." % repr(hv))
                    print("coordinator hangs retrieving its Hardware Version %s, waiting....." % repr(hv))
                else:
                    #                    logging.debug("HV ddo get failed")
                    print("HV ddo get failed")
                if tries < retries - 1:
                    time.sleep(RETRY_INTERVAL)
        return 0x0000

    def __get_fwversion(self, addr_extended):
        if addr_extended == None:
            retries = 99  # sometimes we have a non responsive coordinator.....
        else:
            #            logging.debug("Trying to read XBee VR from %s" % addr_extended)
            print("Trying to read XBee VR from %s" % addr_extended)
            retries = num_retries
        vr = 0
        for tries in range(retries):
            try:
                vr = zigbee.ddo_get_param(addr_extended, 'VR', TX_TIMEOUT)
                return struct.unpack('H', vr)[0]
            except socket.timeout:
                if addr_extended:
                    #                    logging.debug("VR read timeout")
                    print("VR read timeout")
                if tries < retries - 1:
                    time.sleep(RETRY_INTERVAL)
            except:
                if addr_extended == None:
                    #                    logging.debug("coordinator hangs retrieving its Firmware Version, waiting....." % repr(vr))
                    print("coordinator hangs retrieving its Firmware Version %s, waiting....." % repr(vr))
                else:
                    #                    logging.debug("VR ddo get failed")
                    print("VR ddo get failed")
                if tries < retries - 1:
                    time.sleep(RETRY_INTERVAL)
        return 0x0000

    def __xb_get_radio_version(self, addr_extended=None):
        hwversion = self.__get_hwversion(addr_extended)
        fwversion = self.__get_fwversion(addr_extended)
        return (hwversion, fwversion)

    def __get_netreset_endpoint_socket(self):
        sdr = socket.socket(socket.AF_ZIGBEE, socket.SOCK_DGRAM, socket.ZBS_PROT_TRANSPORT)
        try:
            sdr.bind(('', 0xe6, 0x0, 0x0))
        except Exception as err:
            print("NETRESET: binding socket failed, using fallback")
            sdr.bind(('', 0xc6, 0x0, 0x0))
        # socket timeout only applies to receive
        sdr.settimeout(3.0)
        # set blocking sendto() option so we are sure we know if and if succesful sent
        return sdr

    def device_discover_callback(self, remote):
        print(f"Remote found:0 {remote.get_64bit_addr()}")

    def device_discover_end(self, status):
        print("End discover")
        print(status)

    def __get_serial_endpoint_socket(self):
        global StealthMode
        # TODO: make COM port a parameter
        sd = ZigBeeDevice("COM2", 115200)
        try:
            print("Create xbee interface")
            sd.open()
            sd.add_expl_data_received_callback(self.recv_explicit)

            print("Create network interface")
            self._nw = sd.get_network()
            self._nw.set_discovery_options({DiscoveryOptions.DISCOVER_MYSELF,
                                            DiscoveryOptions.APPEND_DD})

            self._nw.add_device_discovered_callback(self.device_discover_callback)
            # TODO: should only be executed with request
            #self._nw.start_discovery_process()

        except Exception as e:
            print(f"Could not open xbee interface: {e}")
        # sd = socket.socket(socket.AF_ZIGBEE, socket.SOCK_DGRAM, socket.ZBS_PROT_TRANSPORT)
        # if self.__xb_get_radio_series() == 1:
        #     sd.bind(('', 0x0, 0x0, 0x0))
        # else:
        #     try:
        #         sd.bind(('', 0xe8, 0x0, 0x0))
        #         StealthMode = False
        #     except Exception as err:
        #         print("ZIGBEE: binding socket failed. likely Owlet.py running, performing fallback to stealth mode")
        #         sd.bind(('', 0xcf, 0x0, 0x0))
        #         StealthMode = True
        # sd.settimeout(RESPONSE_TIMEOUT)
        return sd

    def __seco_get_cli_endpoint_socket(self):
        sdcli = socket.socket(socket.AF_ZIGBEE, socket.SOCK_DGRAM, socket.ZBS_PROT_TRANSPORT)
        try:
            sdcli.bind(('', ZIGBEE_CLI_EP, 0x0, 0x0))
        except Exception as err:
            print("binding to CLI socket failed")
        # socket timeout only applies to receive
        sdcli.settimeout(RESPONSE_TIMEOUT)
        return sdcli

    def __seco_get_tag_endpoint_socket(self):
        sdtag = socket.socket(socket.AF_ZIGBEE, socket.SOCK_DGRAM, socket.ZBS_PROT_TRANSPORT)
        try:
            sdtag.bind(('', ZIGBEE_TAG_EP, 0x0, 0x0))
        except Exception as err:
            print("binding to TAG socket failed")
        # socket timeout only applies to receive
        sdtag.settimeout(RESPONSE_TIMEOUT)
        return sdtag


class LuCoDisplay(threading.Thread):
    def __init__(self, sender):
        threading.Thread.__init__(self)
        self.sender = sender

    def run(self):
        while not StopRunning:
            self.sender.recv()
            # TODO: needs to be refactored
            # self.sender.recvcli()
            # self.sender.recvtag()


class LuCoSend(threading.Thread):

    def __init__(self, sender):
        threading.Thread.__init__(self)
        self.sender = sender
        self.cliautotable = [ \
            ("GET UID", 15),
            ("version", 15), \
            ("ipv4", 15), \
            ("time", 15), \
            ("ipc", 15), \
            ("temp", 15), \
            ("REGISTRATION", 15), \
            ("ADC", 15), \
            ("GPS", 15), \
            ("SYS02", 15), \
            ("SYS03", 15), \
            ("SYS04", 15), \
            ("SYX01", 15), \
            ("RFI01", 15), \
            ("POS01", 15), \
            ("COM01", 15), \
            ("PHC01", 15), \
            ("AT^SJAM=5", 15), \
            ("AT+CFUN?", 15), \
            ("AT+CREG=2", 15), \
            ("AT+CREG?", 15), \
            ("AT+CGREG=2", 15), \
            ("AT+CGREG?", 15), \
            ("AT+COPS?", 15), \
            ("AT^SMONI", 30), \
            ("AT^SMONI=255", 30), \
            ("AT^SMONP", 120), \
            ("AT^SMONP=255", 120), \
            ("AT^SXEONS", 180),
        ]
        self.clicmdtable = [ \
            ("AT^SRADC=1", 20), \
            ]

    def run(self):
        global sendmsg, climsg, tagmsg, cliautomode, clicmdmode, nextclitime
        while not StopRunning:
            if (cliautomode > 0):
                if nextclitime == 1.0:
                    print("AUTOMODE: timeout on last command, aborting AUTOMODE, please check XBEE connection")
                    cliautomode = 0
                    nextclitime = 0.0
                else:
                    # we are in automode so check if we need to send
                    if nextclitime <= time.process_time():
                        try:
                            climsg = self.cliautotable[cliautomode - 1][0]
                            nextclitime = time.process_time() + self.cliautotable[cliautomode - 1][1]
                            print(
                                "AUTOMODE, sending %s (timeout %ds)" % (climsg, self.cliautotable[cliautomode - 1][1]))
                            cliautomode += 1
                        except:
                            climsg = ""
                            cliautomode = 0
                            nextclitime = 0.0
            elif (clicmdmode > 0):
                if nextclitime == 1.0:
                    print("AUTOMODE: timeout on last command, aborting AUTOMODE, please check XBEE connection")
                    clicmdmode = 0
                    nextclitime = 0.0
                else:
                    # we are in automode so check if we need to send
                    if nextclitime <= time.perf_counter():
                        try:
                            climsg = self.clicmdtable[clicmdmode - 1][0]
                            nextclitime = time.perf_counter() + self.clicmdtable[clicmdmode - 1][1]
                            print("AUTOMODE, sending %s (timeout %ds)" % (climsg, self.clicmdtable[clicmdmode - 1][1]))
                            clicmdmode += 1
                        except:
                            climsg = ""
                            clicmdmode = 0
                            nextclitime = 0.0
            if len(sendmsg):
                self.sender.send(sendmsg)
                sendmsg = ""
            elif len(climsg):
                self.sender.sendcli(climsg)
                climsg = ""
            elif len(tagmsg):
                self.sender.sendtag(tagmsg)
                tagmsg = ""
            else:
                time.sleep(0.05)  # allow other threads to run


def getNodeID(MAC):
    # global nodelist
    """
    node

    Name

    node – a python object returned from a node discovery

    Attributes

    Name              Type       Description                                   Example
    type              string     node role/type in mesh                        is in ['coordinator', 'router', 'end']
    addr_extended     string     64-bit extended hardware address              "[00:13:a2:00:40:0a:07:8d]!"
    addr_short        string     16-bit network assigned address               "[49c3]!"
    addr_parent       string     16-bit network parent address                 "[fffe]!"
    source_route      tuple      tuple of 16-bit network addresses in the 
                                 source route from the node. First element is
                                 a neighbor of the node. Last element is a 
                                 neighbor of the gateway node.  Empty if the
                                 node is one hop away, or no source route has
                                 been received from the node. Present in 
                                 gateway firmware version 2.15 and later.      ("[1234]!", "[5678]!")
    profile_id        int        node profile ID                               0xC105 or 49413
    manufacturer_id   int        node manufacturer ID                          0x101E or 4126
    label             string     node’s string label (Setting 'NI')            "TK103U"
    device_type       int        node’s device type (Setting 'DD'). Upper
                                 16 bits contain the module type. Lower 16 
                                 bits contain the product type. Will be 0 
                                 if the node does not support 'DD'             0x00030001
    """

    # update node list from SeCo without refresh
    try:
        node_list = zigbee.get_node_list(refresh=False, discover_digi=False, discover_zigbee=False)
    except socket.timeout as timeout:
        print(" %011.3f: Get Node list failed: %s" % (time.process_time(), str(timeout)))
        pass
    except:
        print(" %011.3f: Unknown except" % (time.process_time()))
        traceback.print_exc(file=sys.stdout)

    # find node based on MAC address    
    try:
        nodes = [y for y in node_list if (y.addr_extended == ("[" + MAC + "]!"))]
    except ValueError:
        return ""

    if len(nodes):
        return nodes[0].label
    else:
        return ""


# internal functions & classes
def report_tag(message, tagdata):
    # first check if we have a info tag answer
    global Tx_count, Rx_count
    global prompt

    try:
        remote_addr = message.get_64bit_addr()
        remote_addr_hex = ":".join(["%02x" % x for x in remote_addr.address])

        remote_node_id = message.get_16bit_addr()
        remote_node_id_hex = "".join(["%02x" % x for x in remote_node_id.address])
        ep = message.dest_endpoint
        profile_id = message.profile_id
        opt = message.source_endpoint  # maybe another stuff
        clusterid = message.cluster_id  # addr[3]

        if displayRaw:
            print("%s" % binascii.hexlify(tagdata))
        if clusterid == 0x0011:
            # it was an explicit Tx so try to interpret it as a command
            ep = message.dest_endpoint
            if ep == 0xb2:
                print("\r%011.3f: Info from %s (%s)" % (
                    time.process_time(), str(repr(remote_addr_hex)).strip("'"), str(repr(remote_node_id_hex))))
                while len(tagdata):
                    # d10c7400000020226400453feb5e
                    owlet_cmd = tagdata[0]
                    if owlet_cmd == "\xd1" or owlet_cmd == "\xd9" or owlet_cmd == "\xa9":
                        # standard or extended tags information points returned
                        tag_table = info_tag_table
                    elif owlet_cmd == "\xaa" or owlet_cmd == "\x8a" or owlet_cmd == "\x88" or owlet_cmd == "\x83":
                        # config or extended calibration tag returned
                        tag_table = config_tag_table
                    elif owlet_cmd == "\xa1":
                        datalength = int(binascii.hexlify(tagdata[1:2]), 16)
                        if datalength > 10:
                            tag_table = info_tag_table
                        else:
                            # data query braodcast
                            print("              -DATA QUERY BROADCAST REQUEST")
                            return
                    elif owlet_cmd == "\xb0":
                        datalength = int(binascii.hexlify(tagdata[1]), 16)
                        if datalength > 10:
                            tag_table = info_tag_table
                        else:
                            # auto commisioning request
                            print("              -AUTO COMMISSIONING BROADCAST REQUEST")
                            return
                    elif owlet_cmd == "\xb1":
                        datalength = int(binascii.hexlify(tagdata[1]), 16)
                        if datalength > 10:
                            tag_table = info_tag_table
                        else:
                            # gps info request
                            print("              -GPS INFORMATION BROADCAST REQUEST")
                            return
                    elif owlet_cmd == "\xc0":
                        # sensor trigger
                        print("              -SENSOR TRIGGER from %s" % (tagdata[3:-1]))
                        return
                    elif owlet_cmd == "\xd4":
                        # group command
                        print("              -GROUP COMMAND %s" % (binascii.hexlify(tagdata[3:-1])))
                        return
                    elif owlet_cmd == "\x60":
                        # power loss
                        print("              -POWER LOSS")
                        return
                    elif owlet_cmd == "\xd6":
                        # manual command
                        print("              -MANUAL COMMAND %s" % (binascii.hexlify(tagdata[3:-1])))
                        return
                    elif owlet_cmd == "\x80":
                        # timestamp
                        tz = ord(tagdata[9])
                        if tz > 36:
                            tz = 24 - tz
                        elif tz > 24:
                            tz = tz - 12
                        elif tz > 12:
                            tz = 12 - tz
                        print("              -TIME STAMP %02u-%02u-%02u %02u:%02u:%02u TZ=%d" % (ord(tagdata[3]),
                                                                                                 ord(tagdata[4]),
                                                                                                 ord(tagdata[5]),
                                                                                                 ord(tagdata[6]),
                                                                                                 ord(tagdata[7]),
                                                                                                 ord(tagdata[8]), tz))
                        return
                    elif owlet_cmd == "\xe0":
                        # factory command
                        if tagdata[3] == '\x22':  # version
                            print("              -FIRMWARE VERSION: %u.%u.%u.%u" % (
                                ord(tagdata[4]), ord(tagdata[5]), ord(tagdata[6]), ord(tagdata[7])))
                        else:
                            print("              -FACTORY COMMAND 0x%02x: %s" % (
                                ord(tagdata[3]), binascii.hexlify(tagdata[4:-1])))
                        return
                    elif owlet_cmd == "\xe1":
                        # bootloader command        
                        if tagdata[4] == '\xa1':  # version
                            print("              -FIRMWARE VERSION: %u.%u.%u.%u" % (
                                ord(tagdata[5]), ord(tagdata[6]), ord(tagdata[7]), ord(tagdata[8])))
                        else:
                            print("              -BOOTLOADER COMMAND 0x%02x: %s" % (
                                ord(tagdata[4]), binascii.hexlify(tagdata[5:-1])))
                        return
                    elif owlet_cmd == "\x00":
                        Rx_count += 1
                        if Tx_count:
                            print("%011.3f: ping %d:%d (%d%%) from %s" % (
                                time.process_time(), Tx_count, Rx_count, (100 * Rx_count) / Tx_count,
                                str(repr(addr[0])).strip("'")))
                        return
                    else:
                        print("              -UNRECOGNIZED COMMAND 0x%02x: %s" % (
                            ord(owlet_cmd), repr(binascii.hexlify(tagdata[1:]))))
                        return

                    # debugging: d10c7400000020226400453feb5e
                    datalength = int(binascii.hexlify(tagdata[1]), 16)
                    # debugging = 0x0c
                    # strip header
                    datalength -= 2
                    # debugging = 10
                    tagdata = tagdata[2:]
                    # debugging = 7400000020226400453feb5e
                    while datalength >= 1:
                        # parse through infotags sequence
                        print("              ", end=' ')
                        tag_type = ord(tagdata[0])
                        # debugging = 0x74 = enx01
                        try:
                            tagindex = [y[0] for y in tag_table].index(tag_type)
                        except ValueError:
                            print("Unrecognized tag type " + str(tag_type), end=' ')
                            return

                        if tag_type == 255:
                            print("-QUERY ALL COMMAND 0x%02X" % (ord(owlet_cmd)))
                        else:
                            # 
                            if tag_type != 0xb0:
                                print("-<", tag_table[tagindex][1], "> ", end=' ')
                            else:
                                if len(tagdata) < 7:
                                    print("requested <snsxx> not present in controller")
                                    tagdata = ""
                                    break
                                else:
                                    print("<%s%02d>" % (tag_table[tagindex][1], ord(tagdata[2])), end=' ')

                            # 8a001b01b001ff44455f434f4c5f303635343536375f3030320000c1
                            #              D E _ C G N _ 0 6 5 4 5 6 7 _ 0 0 2
                            # 01020304050607080910111213141516171819202122232425262728
                            #          01020304050607080910111213141516171819202122
                            # (0xb0, "sns", 22,    [("idx", 1, E_UDEC), ("SIT", 1, E_HEX), ("SNI", 20, E_STRING)]), \

                            if tag_table[tagindex][2] > 0:
                                if tag_table[tagindex][2] == 255:
                                    tagdatalen = ord(tagdata[2])
                                    mytagdata = tagdata[2:]
                                else:
                                    tagdatalen = tag_table[tagindex][2]
                                    mytagdata = tagdata[1:]
                                    # debugging 0044455f434f4c5f303635343536375f3030320000001e
                                tagcontent = tag_table[tagindex][3]
                                for tc in tagcontent:
                                    name = tc[0]
                                    length = tc[1]
                                    if len(mytagdata) < length + 1:
                                        print("unexpected end of data for %s:%s, got %d, expected %d..." % (
                                            tag_table[tagindex][1], name, len(mytagdata), length))
                                        break
                                    tagdatalen -= length
                                    if tc[2] == E_HEX:
                                        fstr = "0x%%0%dx" % (length * 2)
                                        data = fstr % int(binascii.hexlify(mytagdata[0:length]), 16)
                                    elif tc[2] == E_HEXSTRING:
                                        if length == 255:
                                            length = len(mytagdata)
                                        data = binascii.hexlify(mytagdata[0:length])
                                    elif tc[2] == E_5VOLT:
                                        fstr = "%04.2fV"
                                        funpack = '>B'
                                        data = mytagdata[0:length]
                                        data = fstr % (3.5 + struct.unpack(funpack, data)[0] / 100.0)
                                    elif tc[2] == E_VOLT:
                                        fstr = "%04.2fV"
                                        funpack = '>B'
                                        data = mytagdata[0:length]
                                        data = fstr % (struct.unpack(funpack, data)[0] / 10.0)
                                    elif tc[2] == E_UDEC:
                                        fstr = "%%%du" % (math.log(2 ^ (length * 8), 10))
                                        funpack = '>B'
                                        if length > 1:
                                            funpack = '>H'
                                        if length > 2:
                                            funpack = '>L'
                                        if length == 3:
                                            data = "\x00"
                                        else:
                                            data = ""
                                        data += mytagdata[0:length]
                                        data = fstr % struct.unpack(funpack, data)[0]
                                    elif tc[2] == E_S2B:
                                        fstr = "%3d"
                                        data = ord(mytagdata[1])
                                        if ord(mytagdata[0]) == 1:
                                            data -= data
                                        data = fstr % data
                                    elif tc[2] == E_SDEC:
                                        fstr = "%%%dd" % (math.log(2 ^ (length * 8), 10) + 1)
                                        funpack = '>b'
                                        if length > 1:
                                            funpack = '>h'
                                        if length > 2:
                                            funpack = '>l'
                                        value = struct.unpack(funpack, mytagdata[0:length])[0]
                                        data = fstr % value
                                    elif tc[2] == E_PF:
                                        fstr = "%5.2f"
                                        pf = (ord(mytagdata[0]) & 0x7F) / 100.0
                                        if (ord(mytagdata[0]) & 0x80):
                                            pf = -pf
                                        data = fstr % pf
                                    elif tc[2] == E_TEMP:
                                        fstr = "%7.2f (0x%s)"
                                        maxt = int(binascii.hexlify(mytagdata[0:2]), 16) / 100.0
                                        data = fstr % (maxt, binascii.hexlify(mytagdata[0:2]))
                                    elif tc[2] == E_STRING:
                                        # debugging: length = 21 tagdata = 44455f434f4c5f303635343536375f3030320000001e
                                        #                                  000102030405060708091011121314151617181920 
                                        data = str(mytagdata[0:length]).strip('\x00 ')
                                    elif tc[2] == E_VERSION:
                                        fstr = "%d.%d.%d.%d"
                                        data = fstr % (
                                            ord(mytagdata[0]), ord(mytagdata[1]), ord(mytagdata[2]), ord(mytagdata[3]))
                                    elif tc[2] == E_TIMESTAMP:
                                        if mytagdata[0] == '\xff':
                                            data = "..-..-.. ..:..:.."
                                            if mytagdata[3] != '\xff':
                                                fstr = "%02u:%02u:%02u"
                                                data = fstr % (ord(mytagdata[3]), ord(mytagdata[4]), ord(mytagdata[5]))
                                        else:
                                            fstr = "%02u-%02u-%02u %02u:%02u:%02u"
                                            data = fstr % (
                                                ord(mytagdata[0]), ord(mytagdata[1]), ord(mytagdata[2]),
                                                ord(mytagdata[3]),
                                                ord(mytagdata[4]), ord(mytagdata[5]))
                                    elif tc[2] == E_UTIMESTAMP:
                                        # time in sec since 1-1-2000 00:00
                                        if mytagdata[0] == '\xff':
                                            data = "..-..-.. ..:..:.."
                                        else:
                                            data = struct.unpack('>L', mytagdata[0:length])[0]
                                            data += time.mktime((2000, 1, 1, 0, 0, 0, 0, 0, -1))
                                            data = "%s" % (time.ctime(data))
                                    elif tc[2] == E_UTCTIMESTAMP:
                                        # time in sec since 1-1-2000 00:00 UTC
                                        if mytagdata[0] == '\xff':
                                            data = "..-..-.. ..:..:.."
                                        else:
                                            data = struct.unpack('>L', mytagdata[0:length])[0]
                                            data += time.mktime((2000, 1, 1, 0, 0, 0, 0, 0, -1))
                                            data = "%s" % (time.ctime(data))
                                    elif tc[2] == E_ACTIME:
                                        data = struct.unpack('>H', mytagdata[0:length])[0]
                                        if data == 0xFFFF:
                                            data = "None"
                                        else:
                                            fstr = "%02u:%02u"
                                            data = fstr % (data / 60, data % 60)
                                    elif tc[2] == E_S2B:
                                        fstr = "%3d"
                                        data = ord(mytagdata[1])
                                        if ord(mytagdata[0]) == 1:
                                            data -= data
                                        data = fstr % data
                                    print(name, ": ", data, " ", end=' ')
                                    mytagdata = mytagdata[length:]
                                # end for
                                if tagdatalen:
                                    print("\n\rdecode length did not match actual tag length", end=' ')
                        print("")
                        taglength = tag_table[tagindex][2]
                        if taglength == 255:
                            taglength = ord(tagdata[2]) + 1
                        datalength -= (taglength + 2)
                        tagdata = tagdata[taglength + 2:]

                    tagdata = tagdata[1:]
            else:
                # print("\n\r%011.3f: %s (EP:0x%02X, PR:0x%04X, CL:0x%04X, OPT:0x%02x, ID:%s, data: %s ):" % (
                #     time.process_time(), str(remote_addr_hex), ep, profile_id, clusterid, opt, remote_node_id_hex,
                #     binascii.hexlify(tagdata)))
                pass

        elif clusterid == 0x0095:
            # it was Join Notification/Device Announce
            # 104884455:xbee:DEBG:RECV:
            # 104884455:xbee:DEBG:   0: 7e 00 3b 91 00 13 a2 00  40 99 45 a1 22 48 e8 e8  ~.;.....@.E."H..
            # 104884455:xbee:DEBG:  10: 00 95 c1 05 02 22 48 00  13 a2 00 40 99 45 a1 44  ....."H....@.E.D
            # 104884455:xbee:DEBG:  20: 45 5f 43 4f 4c 5f 30 36  35 34 35 36 37 5f 30 31  E_COL_0654567_01
            # 104884455:xbee:DEBG:  30: 32 00 ff fe 01 03 c1 05  10 1e 00 0a 00 00 fe     2..............

            ep = message.dest_endpoint
            if ep == 0xe8:
                mac_string = tagdata[2:10]
                mac_addr = ""
                for c in mac_string:
                    mac_addr += "%02X:" % c
                mac_addr = mac_addr[:-1]
                net_addr = struct.unpack('<H', tagdata[0:2])[0]
                ni = ""
                i = 10
                while tagdata[i] != 0:
                    ni += chr(tagdata[i])
                    i += 1
                parent_addr = struct.unpack('>H', tagdata[i + 1:i + 3])[0]
                dev_type = tagdata[i + 3]
                src_evt = tagdata[i + 4]
                manuf_id = struct.unpack('>H', tagdata[i + 7:i + 9])[0]
                if dev_type == 0:
                    dev_string = "coordinator"
                elif dev_type == 1:
                    dev_string = "router"
                elif dev_type == 2:
                    dev_string = "end device"
                else:
                    dev_string = "unknown"
                result_string = "MAC=%s (%04X), NI=%s, parent=0x%04X, device type=%s" % (
                    mac_addr, net_addr, ni, parent_addr, dev_string)
                if src_evt == 1:
                    result_string += ", trigger:pushbutton"
                elif src_evt == 2:
                    result_string += ", trigger:joinevent (JN)"
                elif src_evt == 3:
                    result_string += ", trigger:powercycle (JN)"
                if manuf_id == 0x101e:
                    result_string += ", Manufacturer is DIGI"
                else:
                    result_string += ", Unknown manufacturer: 0x%04X" % (manuf_id)
                print("%011.3f: Info from %s" % (time.process_time(), str(mac_addr).strip("'")))
                print("              -JOIN NOTIFICATION: %s" % (result_string))
        else:
            print("\n\r%011.3f: %s (EP:0x%02X, PR:0x%04X, CL:0x%04X, OPT:0x%02x, ID:%d, data: %s ):" % (
                time.process_time(), remote_addr_hex, ep, profile_id, clusterid, opt, remote_node_id_hex,
                binascii.hexlify(tagdata)))

        #TODO: how to handle multiple messages for same command
        #print("%s" % prompt, end=' ')

    except Exception as e:
        print(
            "\n\rException during processing data from %s: %s" % (str(repr(remote_addr_hex)), repr(binascii.hexlify(tagdata))))
        traceback.print_exc(file=sys.stdout)


# internal functions & classes
def report_cli(addr, clidata):
    # first check if we have a info tag answer
    global prompt
    global last_message_time
    global cliautomde, clicmdmode, nextclitime

    if not hasattr(report_cli, "clireturn"):
        report_cli.clireturn = ""

    try:
        if ((time.process_time() - last_message_time) > 1.0):
            print("\r%011.3f: Info from %s (%s) " % (
                time.process_time(), str(repr(addr[0])).strip("'"), getNodeID(str(repr(addr[0])).strip("[]!'"))))
        if displayRaw:
            print("\n%s" % binascii.hexlify(clidata))
        last_message_time = time.process_time()
        report_cli.clireturn += clidata
        adcvalindex = report_cli.clireturn.find("SRADC: 1,")
        if adcvalindex > 0:
            adcvalindex = adcvalindex + 9
            adcvalend = adcvalindex + report_cli.clireturn[adcvalindex:].find("\r")
            print("\nDimoutput value: %4.1f V\n" % (
                    (6592.0 + 499.0) / 499000.0 * int(report_cli.clireturn[adcvalindex:adcvalend])))

        if "[CLI]>" in report_cli.clireturn:
            print("%s" % clidata)
            report_cli.clireturn = ""
            if cliautomode == 0 and clicmdmode == 0:
                print("%s" % prompt, end=' ')
            else:  # next statemetn triggers next CLI message can be send in automode
                nextclitime = time.process_time()
        else:
            print("%s" % clidata, end=' ')

    except:
        print(
            "\n\rException during processing data from %s: %s" % (str(repr(addr[0])), repr(binascii.hexlify(clidata))))
        traceback.print_exc(file=sys.stdout)


def to_unified_MAC(mac):
    node_address = '[' + mac.strip('[]!\n\r').lower() + ']!'
    return node_address.strip('[]!')


def is_valid_MAC(mac):
    mac_address = to_unified_MAC(mac)
    a = re.compile('^([a-fA-F0-9]{2}[:]?){7}[a-fA-F0-9]{2}$').search(mac_address)
    if a:
        return True, mac_address
    else:
        return False, "00:00:00:00:00:00:ff:ff"


def is_valid_lower_MAC(mac):
    mac_address = to_unified_MAC(mac)
    a = re.compile('^([a-fA-F0-9]{2}[:]?){3}[a-fA-F0-9]{2}$').search(mac_address)
    if a:
        return True, mac_address
    else:
        return False, ""


def getbcastargs(arguments, max_args):
    arg_err = 0
    arg_message = ""
    # defaults
    b_type = 1
    b_group = 0xffff
    b_delay = 6
    b_offset = 0
    b_accuracy = 0
    b_mac = 0x00000000

    try:
        b_args = arguments.strip().split(None)
        if len(b_args) >= 1:
            b_type = int(b_args[0], 0)
            if b_type == 1:
                if len(b_args) >= 2:
                    # correct number of arguments, a second argument was given
                    try:
                        b_group = int(b_args[1], 0)
                        if b_group < 1 or b_group > 0xFFFF:
                            print("group value needs to be between 1 and 0xFFFF, %x given" % (b_group))
                        else:
                            if len(b_args) >= 3:
                                try:
                                    b_offset = int(b_args[2], 0)
                                    if b_offset < 0 or b_offset > 0xFF:
                                        print("offset value needs to be between 0 and 0xFF, 0x%02x given" % (b_offset))
                                    else:
                                        if len(b_args) >= 4:
                                            try:
                                                b_delay = int(b_args[3], 0)
                                                if b_delay < 0 or b_delay > 0xFF:
                                                    print("delay value needs to be between 0 and 255, %d given" % (
                                                        b_delay))
                                                else:
                                                    if len(b_args) >= 5:
                                                        try:
                                                            b_accuracy = int(b_args[4], 0)
                                                            print("accuracy %d" % b_accuracy)
                                                            if b_accuracy < 0 or b_accuracy > 0xFF:
                                                                print(
                                                                    "accuracy value needs to be between 0 and 255, %d given" % (
                                                                        b_accuracy))
                                                        except ValueError:
                                                            arg_err = 4
                                                            print("Unrecognized dataquery accuracy value " + str(
                                                                b_args[4]))
                                            except ValueError:
                                                arg_err = 3
                                                print("Unrecognized dataquery delay value " + str(b_args[3]))
                                except ValueError:
                                    arg_err = 2
                                    print("Unrecognized dataquery offset value " + str(b_args[2]))
                    except ValueError:
                        arg_err = 1
                        print("Unrecognized dataquery group value " + str(b_args[1]))
                else:
                    print("unrecognized dqb type 1 arguments %s" % (arguments))

            elif (b_type == 2) or (b_type == 3):
                valid_low_addr, maclow_addr = is_valid_lower_MAC(b_args[1])
                if valid_low_addr:
                    b_mac = int(maclow_addr.replace(":", ""), 0)
                else:
                    arg_err = 0xf2
                    print("invalid lower MAC address: %s" % (b_args[1]))
            elif b_type == 4:
                arg_err = 0xf4
                print("Data Query Broadcast Type 4 not supported yet")
            else:
                arg_err = 0xff
                print("Data Query Broadcast Type %d not recognized" % (b_type))

    except ValueError:
        arg_err = 0xff
        print("Unrecognized argument " + arguments)
    if arg_err == 0:
        # assemble hex string
        if b_type == 1:
            if max_args == 5:
                arg_message = chr(b_type) + chr(b_group / 256) + chr(b_group % 256) + chr(b_offset) + chr(
                    b_delay) + chr(b_accuracy)
            else:
                arg_message = chr(b_type) + chr(b_group / 256) + chr(b_group % 256) + chr(b_offset) + chr(b_delay)
        elif (b_type == 2) or (b_type == 3):
            arg_message = chr(b_mac >> 24) + chr(((b_mac >> 16) % 0x100) >> 8) + chr(((b_mac >> 8) % 0x100) >> 8) + chr(
                b_mac % 0x100)

    else:
        arg_message = ""

    return arg_err, arg_message


def xor_parity(message):
    parity = 0
    for c in message:
        parity ^= ord(c)
    return parity & 0xff


def handle_command(sender, command):
    """
    docstring for handle_command    
    """
    global displayRaw
    global sendmsg
    global climsg
    global tagmsg
    global RangeTest
    global Tx_count, Rx_count
    global bb_overrule
    global cur_ver
    global send_redundancy
    global myLuCoRedundancyThread
    global cliautomode, clicmdmode

    cur_ver = 0xFFFFFFFF
    numargs = 0

    broadcast_allowed = True

    RangeTest = 0

    if command == "!exit" or command == "!quit":
        return False
    # for not having errors during empty commands
    if not command.strip():
        return
    try:
        message = ""
        climessage = ""
        tagmessage = ""
        if command[0] == "!":
            command = command[1:]
            if command[0:5] == "nodes":
                options = command[5:].strip()
                node_discover = False
                sorton = ""
                if "refresh" in options:
                    node_discover = True
                    options = options.replace("refresh", "")
                if "l" in options:
                    sorton = "l"
                if "s" in options:
                    sorton = "s"
                if "t" in options:
                    sorton = "t"
                if "e" in options:
                    sorton = "e"
                if "h" in options:
                    sorton = "h"
                sender.display_nodes(discover=node_discover, sorton=sorton)
            elif command == "xbnr0":
                sender.send_netreset()
            elif command == "raw":
                displayRaw = not displayRaw
                print("toggle raw mode now: %s" % repr(displayRaw))
            elif command == "bcok":  # not in help, only for real experts...
                bb_overrule = not bb_overrule
                print("toggle broadcast overrule mode: %s" % repr(bb_overrule))
            elif command == "help" or command == "?":
                usage()
            elif command == "b":
                sender.changeaddr("00:00:00:00:00:00:ff:ff")
            elif command[0:3] == "tag":
                for byte in command[3:].split():
                    try:
                        tagmessage += chr(int(byte, 16))
                    except ValueError:
                        print("Discarded unexpected hex byte in input string: %s" % byte)
                tagmessage += chr(xor_parity(tagmessage))
            elif command[0:2] == "ul":
                tagmessage = "\xe0\x04\x12"
                numargs = 0
                for byte in command[2:].split():
                    try:
                        tagmessage += chr(int(byte))
                        numargs += 1
                    except ValueError:
                        print("Discarded unexpected valuebyte in input string: %s" % byte)
                if numargs != 2:
                    print("invalid number of arguments: expected exactly 2")
                    tagmessage = ""
                else:
                    tagmessage += chr(xor_parity(tagmessage))
            elif command == "xbtest":
                # E0 03 92 01 70
                tagmessage = "\xe0\x03\x92\x01"
                tagmessage += chr(xor_parity(tagmessage))
            elif command == "calread":
                # 8A 02 74 FC
                tagmessage = "\x8A\x02\x74\xFC"
                tagmessage += chr(xor_parity(tagmessage))
                broadcast_allowed = False
            elif command == "caldef":
                # 83 12 74 A0 45 F7 00 00 00 00 36 50 00 00 00 00 30 E9 00 48
                tagmessage = "\x83\x12\x74\xA0\x32\x5C\x3C\x00\x00\x00\x3B\x8C\xCB\x00\x00\x00\x30\xD3\x8A"
                tagmessage += chr(xor_parity(tagmessage))
                broadcast_allowed = False
            elif command == "analyze":
                if sender.IsBroadcast():
                    print("command is not allowed as broadcast, please switch to single address using !a command")
                else:
                    cliautomode = 1  # send thread should pick this up
            elif command == "readdim":
                if sender.IsBroadcast():
                    print("command is not allowed as broadcast, please switch to single address using !a command")
                else:
                    clicmdmode = 1  # send thread should pick this up
            elif command[0:2] == "aa":
                valid_addr, mac_addr = is_valid_MAC(command[2:].strip())
                if valid_addr:
                    sender.addaddr(mac_addr)
            elif command[0] == "a":
                valid_addr, mac_addr = is_valid_MAC(command[1:].strip())
                if valid_addr:
                    sender.changeaddr(mac_addr)
        else:
            #            climessage = command + chr(0x0D) + chr(0x0A)
            #            climessage = command + chr(0x0D)
            #            climessage = command + chr(0x0A)
            climessage = command

        if bb_overrule:
            broadcast_allowed = True

        if len(tagmessage):
            if sender.IsBroadcast() and broadcast_allowed == False:
                tagmsg = ""
                print("Configuration or Calibration commands not allowed as broadcast")
            else:
                tagmsg = tagmessage  # send thread should pick this up
        if len(message):
            if sender.IsBroadcast() and broadcast_allowed == False:
                sendmsg = ""
                print("Configuration or Calibration commands not allowed as broadcast")
            else:
                sendmsg = message  # send thread should pick this up
        if len(climessage):
            if sender.IsBroadcast() and broadcast_allowed == False:
                climsg = ""
                print("CLI not allowed as broadcast")
            else:
                climsg = climessage  # send thread should pick this up
    except Exception as e:
        print(repr(e), e)


def command_line(zb_addr="00:00:00:00:00:00:ff:ff!"):
    global StopRunning
    global StealthMode
    global prompt
    global myLuCoRedundancyThread
    global cliautomode, clicmdmode

    command = ""
    console = code.InteractiveConsole()
    sender = ZBSender(zb_addr)
    if StealthMode:
        prompt = "OW3_CLI (`!help` for help)% "
    else:
        prompt = "OW3_CLI (`!help` for help)> "
    # start receive thread
    myLuCoRxThread = LuCoDisplay(sender)
    print("Starting recieve thread")
    myLuCoRxThread.start()
    myLuCoTxThread = LuCoSend(sender)
    print("Starting transmit thread")
    myLuCoTxThread.start()

    while command != "!exit" and command != "!quit":
        if cliautomode > 0 or clicmdmode > 0:
            time.sleep(1.0)
        else:
            command = console.raw_input(prompt)
            RangeTest = 0
            command = command.strip()
            handle_command(sender, command)


    StopRunning = True
    print("Waiting for threads to stop")
    myLuCoTxThread.join()
    myLuCoRxThread.join()
    print("Done...")
    return 0


def usage():
    """
    print the usage message    
    """
    print("Usage: \n\t%s" % sys.argv[0])
    print(" local and Xbee commands are preceeded with `!`")
    #    print "       !mcureset           :perform a HW reset through XBee IO"
    print("       !a<MAC ADDRESS>     :set transmission address to <MAC ADDRESS> in form of 00:00:00:00:00:00:00:00")
    print("       !aa<MAC ADDRESS>    :add transmission address <MAC ADDRESS> in form of 00:00:00:00:00:00:00:00")
    print("       !b                  :set transmission address to broadcast (00:00:00:00:00:00:ff:ff)")
    print("       !nodes {refresh} {(l,t,s,e,h)}): display list of nodes")
    print("                              optionally with refresh of node list")
    print(
        "                              optionally sorted on label, type, short address, extended address or hop count")
    print("       !xbnr0              :send out special net reset command to xbee node")
    print(
        "       !xbtest             :send out special command to start continuous XBEE sending for certification only")
    print("       !analyze            :send out sewries of commands for analysis")
    print("       !caldef             :reset calibration to manufacturing default")
    print("       !calread            :read energy calibration values")
    print("       !ul <on> <off>      :toggle UL mode in sec on, sec off, switch off if on = 0")
    print("       !readdim            :read voltage on DIM output")
    print("       !raw                :toggle display of raw hex packages received")
    print("")
    print("       <any other text>   :send text to CLI")
    print('type "!exit" or "!quit" to leave')
    return


def main(argv=[]):
    """program starter function"""

    """
    timeout = 10
    print "Enter something:",
    """
    try:
        print(MY_NAME, " ", MY_VERSION, " starting at %s (%011.3f)" % (time.ctime(), time.perf_counter()))

        if len(argv) > 1:
            if argv[1] == "-h":
                usage()
            else:
                command_line(argv[1])
        else:
            command_line()
        return 0

    except Exception as err:
        print("Command Line parsing failed: ", err)
        traceback.print_exc(file=sys.stdout)
    return 1


if __name__ == '__main__':
    sys.exit(main(sys.argv[::]))
