"""
# Filename:     P7CM_FCT.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   23/04/2021
# Last Update:  23/04/2021
#
# Version:      1.0
# Filetype:     
# Description:  Interface to DALI over uart
# STATUS:       Stable
# Limitation:   None
"""
from hat.comms.g4_interface_cli import WebsocketInterface
from hat.utils.utils import retry, list_to_hex_string
from enum import Enum, auto
import json
from functools import reduce
import time
import logging
from typing import List
from logging.handlers import RotatingFileHandler
from hat.comms.DaliUsb import DaliUsb

LOG_FILENAME = 'DaliUartInterface.txt'

log = logging.getLogger(__name__)
stream_handler = logging.StreamHandler()
stream_handler.setLevel("DEBUG")
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
stream_handler.setFormatter(formatter)
# add ch to logger
log.addHandler(stream_handler)

# create handler for file logging
# Add the log message handler to the logger
file_handler = RotatingFileHandler(
    LOG_FILENAME, maxBytes=10000000, backupCount=5)

# set formatter for file_handler
file_handler.setFormatter(formatter)
log.addHandler(file_handler)

log.setLevel("DEBUG")

CONFIGS = {
    "nTries": 4,  # number of retries

}


class ReceiveState(Enum):
    EXPECT_START_BYTE = 1,
    EXPECT_LEN_BYTE = 2,
    EXPECT_DATA = 3,
    EXPECT_CRC = 4,
    COMPLETE = 5


class InterfaceType(Enum):
    DALI = auto(),
    USB = auto()


DEFAULT_TIMEOUT = 5


class P7CM_FCT:
    START_BYTE = 0xE0

    # Commands for FCT
    CMD_MCU_VERSION = 0x04
    CMD_PRODUCT_KEY = 0x01
    CMD_PRODUCTION_SN = 0x02
    CMD_MPU_VERSION = 0x03
    CMD_MCU_BOOTLOADER = 0x05
    CMD_GEMALTO_VERSION = 0x80
    CMD_IMEI = 0x81
    CMD_IMSI = 0x82
    CMD_XBEE_VERSION = 0x90
    CMD_XBEE_EUI = 0x91
    CMD_SWITCH_RELAY = 0x10
    CMD_NO_LOAD_BIT = 0x11
    CMD_I2C_DEVICES = 0x40
    CMD_PHOTOCELL = 0x48
    CMD_RFID_SWITCH = 0x50
    CMD_RFID_DATA = 0x51
    CMD_LSI_INPUT = 0x18

    def __init__(self, address: str, interface_type: InterfaceType = InterfaceType.USB, timeout: int = DEFAULT_TIMEOUT):
        self._intf = None
        self._timeout = timeout
        self._address = address
        self._open = False

        if interface_type not in InterfaceType:
            raise Exception(f"Invalid interface type: {interface_type}")

        self._interface_type = interface_type

    def is_open(self):
        return self._open

    def open(self, address=None, ):
        if self.is_open():
            print("Dali interface Already opened!")
            return

        if address is not None:
            self._address = address

        print(f"Opening P7CM FCT interface {self._address}")
        if self._interface_type == InterfaceType.DALI:
            self._intf = WebsocketInterface(self._address, "dali", timeout=self._timeout)
        elif self._interface_type == InterfaceType.USB:
            self._intf = DaliUsb(port=self._address, baudrate=115200, timeout=self._timeout)

        self._open = True

    @staticmethod
    def make_json(data):
        package = {
            "type": "dali_cmd",
            "data": [x for x in data]
        }
        package["len"] = len(package["data"])
        package_json = json.dumps(package)
        return package_json

    @staticmethod
    def calculate_CRC(data):
        return reduce((lambda a, b: a ^ b), data)

    @staticmethod
    def make_package(command, data=None):
        if data is None:
            data = []

        cmd_data = []
        cmd_data.append(P7CM_FCT.START_BYTE)
        cmd_data.append(1 + len(data) + 1)  # start byte + command byte + len(data)
        cmd_data.append(command)

        if len(data):
            cmd_data += data

        cmd_data.append(P7CM_FCT.calculate_CRC(cmd_data))

        json_data = P7CM_FCT.make_json(cmd_data)

        return {"raw": cmd_data,
                "json": json_data}

    def get_packet(self):
        # FIXME: workaround for large messages that dali interface cannot handle
        MAX_MESSAGE_DATA = 28

        response = {}
        state = ReceiveState.EXPECT_START_BYTE
        expected_len = 0

        response["message_data"] = []
        response["complete_data"] = []
        start_time = time.time()

        while not state == ReceiveState.COMPLETE and time.time() - start_time < self._timeout:
            try:
                if self._interface_type == InterfaceType.DALI:
                    data = self._intf.read()
                    aux = json.loads(data)
                    if "dali_type" not in aux:
                        continue

                    new_hex_data = [int(x, 16) for x in aux["hex_data"].strip().split(" ")]
                    if len(new_hex_data) != aux["len"]:
                        raise ValueError(f"Wrong len")
                elif self._interface_type == InterfaceType.USB:
                    data = self._intf.get_data()
                    new_hex_data = [ord(data)]

                for hex_byte in new_hex_data:
                    if state == ReceiveState.EXPECT_START_BYTE:
                        if hex_byte == P7CM_FCT.START_BYTE:
                            state = ReceiveState.EXPECT_LEN_BYTE
                        response["complete_data"].append(hex_byte)
                    elif state == ReceiveState.EXPECT_LEN_BYTE:
                        expected_len = hex_byte
                        state = ReceiveState.EXPECT_DATA
                        response["complete_data"].append(hex_byte)
                    elif state == ReceiveState.EXPECT_DATA:
                        response["message_data"].append(hex_byte)
                        response["complete_data"].append(hex_byte)

                        if len(response["message_data"]) == expected_len - 1:
                            state = ReceiveState.EXPECT_CRC
                        elif len(response["message_data"]) == MAX_MESSAGE_DATA:
                            print("Not able to receive complete message")
                            state = ReceiveState.COMPLETE

                    elif state == ReceiveState.EXPECT_CRC:
                        CRC = hex_byte
                        expected_CRC = P7CM_FCT.calculate_CRC(response["complete_data"])
                        if CRC != expected_CRC:
                            raise Exception(f"CRC dont match: {response['complete_data']}")
                        response["complete_data"].append(hex_byte)
                        state = ReceiveState.COMPLETE
                        # message complete
                    else:
                        raise Exception("Unrecognized state")

            except Exception as e:
                state = ReceiveState.EXPECT_START_BYTE
                print(f"Problem: {e}")

        if state != ReceiveState.COMPLETE:
            raise Exception("Package not completely received")

        # log.debug(f"Get package: {binascii.hexlify(response['complete_data'])}")
        return response

    def _send_packet(self, command: int, data: List[int]):
        """
        Send a packet

        Parameters
        ----------
        command: int
            command to send to be added when creating the packet
        data : List of int
            data to be send
        """
        if data is None:
            data = []
        # log.debug(f"Send: {command:02x} {list_to_hex_string(data)}")

        package = P7CM_FCT.make_package(command, data)

        log.debug(f"Send: {list_to_hex_string(package['raw'])}")

        # FIXME: simplify
        if self._interface_type == InterfaceType.DALI:
            self._intf.write(package["json"])
            tx = self.get_packet()
            if tx["complete_data"] != package["raw"]:  # send package should also be captured
                raise Exception("Bad package TX")
        elif self._interface_type == InterfaceType.USB:
            self._intf.send_data(package["raw"])

        time.sleep(0.1)  # sleep 100ms

    @retry(Exception, CONFIGS["nTries"], 2, 1)
    def execute_fct_cmd(self, command, data=None, conversion="RAW"):
        # check if interface is opened
        if not self._open:
            self.open()

        self._send_packet(command, data)

        response = self.get_packet()  # in dali, we hear an echo of the send package
        response = self.get_packet()  # get real response

        log.debug(f"Recv: {list_to_hex_string(response['complete_data'])}")

        if response["message_data"][0] != command:  # compare response command
            raise Exception("Bad command in response")

        response_data = response["message_data"][1:]

        if conversion == "RAW":
            conv_data = response_data
        elif conversion == "VERSION":
            conv_data = ".".join(map(str, response_data))
        elif conversion == "STRING":
            conv_data = "".join(map(chr, response_data))
        elif conversion == "HEX":
            conv_data = "".join(map(lambda x: f"{x:02x}", response_data))
        else:
            raise ValueError(f"Conversion type {conversion} not recognized")

        return conv_data

    def get_mcu_version(self):
        log.debug("Getting MCU version")
        version = self.execute_fct_cmd(P7CM_FCT.CMD_MCU_VERSION, conversion="VERSION")
        log.debug(f"MCU version: {version}")
        return version

    def get_product_key(self):
        log.debug("Get product key")
        product_key = self.execute_fct_cmd(P7CM_FCT.CMD_PRODUCT_KEY, conversion="STRING")
        log.debug(f"Product Key: {product_key}")
        return product_key

    def get_production_serial_number(self):
        log.debug("Get production serial number")
        serial_number = self.execute_fct_cmd(P7CM_FCT.CMD_PRODUCTION_SN, conversion="STRING")
        log.debug(f"Production serial number: {serial_number}")
        return serial_number

    def get_mpu_version(self):
        log.debug("Get MPU version")
        version = self.execute_fct_cmd(P7CM_FCT.CMD_MPU_VERSION, conversion="VERSION")
        log.debug(f"MPU version: {version}")
        return version

    def get_mcu_bootloader_version(self):
        log.debug("Getting MCU bootloader version")
        version = self.execute_fct_cmd(P7CM_FCT.CMD_MCU_BOOTLOADER, conversion="VERSION")
        log.debug(f"MCU bootloader version: {version}")
        return version

    def get_gemalto_version(self):
        log.debug("Get gemalto version")
        version = self.execute_fct_cmd(P7CM_FCT.CMD_GEMALTO_VERSION, conversion="STRING")
        log.debug(f"Gemalto version: {version}")
        return version

    def get_imei(self):
        log.debug("Get imei")
        imei = self.execute_fct_cmd(P7CM_FCT.CMD_IMEI, conversion="STRING")
        log.debug(f"IMEI: {imei}")
        return imei

    def get_imsi(self):
        log.debug("Get imsi")
        imsi = self.execute_fct_cmd(P7CM_FCT.CMD_IMSI, conversion="STRING")
        log.debug(f"IMSI: {imsi}")
        return imsi

    def get_xbee_version(self):
        log.debug("Get xbee version")
        version = self.execute_fct_cmd(P7CM_FCT.CMD_XBEE_VERSION, conversion="HEX")
        log.debug(f"Xbee version: {version}")
        return version

    def get_eui(self):
        log.debug("Get eui")
        eui = self.execute_fct_cmd(P7CM_FCT.CMD_XBEE_EUI, conversion="HEX")
        log.debug(f"EUI: {eui}")
        return eui

    def switch_relay(self, state: bool, zero_cross: bool) -> bool:
        log.debug(f"Setting relay to {state} using zero cross {zero_cross}")
        data = (not zero_cross) << 1 | state
        state = bool(self.execute_fct_cmd(P7CM_FCT.CMD_SWITCH_RELAY, [data], conversion="VERSION"))
        log.debug(f"Relay state: {state}")
        return state

    def get_no_load_bit(self) -> (bool, float):
        log.debug("Get no load bit")
        data = self.execute_fct_cmd(P7CM_FCT.CMD_NO_LOAD_BIT, conversion="RAW")

        no_load = bool(data[0])
        voltage_mv = (data[1] << 16) + (data[2] << 8) + data[3]
        log.debug(f"No load bit: {no_load}, voltage: {voltage_mv}")
        return no_load, voltage_mv

    def get_i2c_devices(self):
        log.debug("Get I2C devices")
        data = self.execute_fct_cmd(P7CM_FCT.CMD_I2C_DEVICES, conversion="RAW")
        error_code = data[0]
        addresses = data[1:]
        log.debug(f"I2C, Error code: {error_code}, addresses: {addresses}")
        return error_code, addresses

    def get_photocell(self):
        log.debug("Get photocell value")
        data = self.execute_fct_cmd(P7CM_FCT.CMD_PHOTOCELL, [1], conversion = "RAW")
        lux = (data[1] << 8) + data[2]
        log.debug(f"Photocell value: {lux} lux")
        return lux

    def set_rfid_switch(self, state: bool):
        log.debug(f"Set RFID switch: {state}")
        data = self.execute_fct_cmd(P7CM_FCT.CMD_RFID_SWITCH, [int(state)], conversion="RAW")
        if state != data[0]:
            raise Exception("Set RFID didn't work")
        log.debug("Set RFID switch done")
        return

    def get_rfid_data(self):
        log.debug("Get RFID data")
        data = self.execute_fct_cmd(P7CM_FCT.CMD_RFID_DATA, conversion="RAW")
        log.debug(f"RFID data: {data}")
        return data

    def set_sensor_input_test(self, state: bool):
        log.debug(f"LSI sensor test: {state}")
        data = self.execute_fct_cmd(P7CM_FCT.CMD_LSI_INPUT, [int(bool(state))], conversion="RAW")
        if state != data[0]:
            raise Exception("Set Sensor input test didn't work")
        return


if __name__ == "__main__":
    p7cm = P7CM_FCT("COM114", timeout=100)
    version = p7cm.get_mcu_version()
    print(f"version: {version}")
    print("end!")
