import serial
import logging
import time
import re

log = logging.getLogger("G4 interface")
handler = logging.StreamHandler()
handler.setLevel("DEBUG")
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
handler.setFormatter(formatter)

# add ch to logger
log.addHandler(handler)

log.setLevel("DEBUG")

LTE_prompt = "lte $ \r\nlte $ "
SLEEP_TIME = 0.1

prompts = {
    "bash": {"prompt": "$ ", "enter_cmd": "quit"},
    "lte": {"prompt": "\r\nlte $ \r\nlte $ ", "enter_cmd": "lte-shell"}
}


class G4:
    def __init__(self, com_port, baudrate=115200, timeout=10):
        log.info("Connecting to G4 module")
        self._timeout = timeout
        self._interface = serial.Serial(port=com_port, baudrate=baudrate, timeout=timeout)
        self._mode = "lte"
        self._prompt = prompts["bash"]["prompt"]
        # mode = self.get_current_mode()

    def get_current_mode(self):
        self._interface.flush()
        self._interface.read_all()
        self.write("\n")
        time.sleep(1)
        data = self._interface.read_all().decode("ascii")

        for mode, confs in prompts.items():
            if data == confs["prompt"] :
                log.info(f"In mode {mode}")
                self._mode = mode
                self._prompt = confs["prompt"]
                break
        else:
            raise Exception("No compatible mode found! data: %s" % data)
            # print("None")
        print(data)

    def _read_until(self, prompt=None):
        if prompt is None:
            prompt = self._prompt
        data = self._interface.read_until(prompt.encode("ascii")).decode("utf-8")
        log.debug(f"Receiving: {data}")
        return data

    # def synchronize(self, prompt=LTE_prompt):
    #     self.write("\n")
    #     data = self.read_all(prompt)

    def write(self, command, flush=True):
        log.debug(f"Sending: {command}")
        if not command[-1] == "\n":
            command += "\n"

        command = command.encode("ascii")
        self._interface.write(command)

        if flush:
            self._interface.flushOutput()

    def read_all(self, prompt=None):
        if prompt is None:
            prompt = self._prompt
        data = self._read_until(prompt)
        return data

    def go_to_mode(self, mode):
        if mode != self._mode:
            self.write(prompts[mode]["enter_cmd"])
            time.sleep(SLEEP_TIME)
            self.get_current_mode()

    def execute_command(self, mode, command, sleep_time=SLEEP_TIME, check_errors=False):
        self.go_to_mode(mode)
        self._interface.flush()
        self.write(command)
        time.sleep(sleep_time)
        if mode == "lte":
            # needed due to way lte shell works
            self.write("\n")
            debug = self.read_all()
        data = self.read_all()

        if check_errors and "CME" in data:
            log.error("Errors in response: %s", data)
            raise Exception(f"Error in response: {data}")

        data = data.replace(command, "")
        data = data.replace(self._prompt, "")

        return data

    def sim_locked(self):
        sim_state = self.execute_command("lte", "AT+CPIN?\n")
        if "SIMIN" in sim_state or "SIM PIN" in sim_state:
            return True
        elif "READY" in sim_state:
            return False
        else:
            log.info("Not read correctly")

    def get_imsi(self):
        imsi_text = self.execute_command("lte", "AT+CIMI")
        return imsi_text.split()[0]

    def get_register_gprs(self):
        register_gsm = self.execute_command("lte", "AT+CGREG?")
        match = re.search(r"\+CGREG:\s(\d),(\d)", register_gsm)
        if match:
            unsolicited = int(match.group(1))
            status = int(match.group(2))
            return unsolicited, status
        else:
            raise Exception("Could not find register status, %s", register_gsm)

    def get_register_gsm(self):
        register_gsm = self.execute_command("lte", "AT+CREG?")
        match = re.search(r"\+CREG:\s(\d),(\d)", register_gsm)
        if match:
            unsolicited = int(match.group(1))
            status = int(match.group(2))
            return unsolicited, status
        else:
            raise Exception("Could not find register status, %s", register_gsm)

    def lte_network_operator(self):
        data = self.execute_command("lte", "AT+COPS?").splitlines()[1]
        return data

    def network_info(self):
        data = self.execute_command("lte", "AT+QNWINFO").splitlines()[1]
        return data

    def unlock_sim(self, pin):
        lock_state = self.sim_locked()

        log.debug(f"SIM card is lock: {lock_state}")
        if lock_state:
            data = self.execute_command("lte", f'AT+CPIN="{pin}"')
            lock_state = self.sim_locked()
            if lock_state:
                print("Could not unlock SIM")
            else:
                log.info("Sim unlock")

        else:
            log.info("Sim already unlocked")

    def lte_assign_APN(self):
        data = self.execute_command('lte', 'AT+QICSGP=1,1,"net2.vodafone.pt","vodafone","vodafone",1')
        return data

    def lte_ping(self, website):
        # ip = utils.get_ip_from_hostname(website)
        data = self.execute_command("lte", f'AT+QPING=1,"{website}",10,10', 1)

        match = re.findall(r"\d+\.\d+\.\d+", data)

        if not match:
            log.error("Could not perform ping, maybe it is not connected")
            return False
        else:
            log.info(f"Could ping {website}!")
            return True

    def lte_configure_http(self):
        # //Example of how to send HTTP GET response.
        # //Configure the PDP context ID as 1.
        data = self.execute_command("lte", 'AT+QHTTPCFG="contextid",1')

        # //Allow to output HTTP response header.
        data = self.execute_command("lte", 'AT+QHTTPCFG="responseheader",1')

        # //Query the state of context.
        data = self.execute_command("lte", 'AT+QIACT?')

        # Assign APN
        self.lte_assign_APN()

        # //Configure PDP context 1
        data = self.execute_command("lte", 'AT+QIACT=1')

        # Query state of context
        data = self.execute_command("lte", 'AT+QIACT?')

    def lte_set_http_url(self, website):
        if not website.startswith("http") and not website.startswith("https"):
            raise Exception(f"Wrong format of website {website}")

        # lte mode
        self.go_to_mode("lte")
        #
        self.write(f"AT+QHTTPURL={len(website)},80\n")
        time.sleep(0.1)
        # self._interface.read_until("CONNECT")
        self.write(website + "\n")
        self.read_all()
        data = self.read_all()
        self.write("\n")
        data = self.read_all()
        return True if "OK" in data else False

    def lte_get_http_read(self):
        data = self.execute_command("lte", "AT+QHTTPGET=80")
        dev.read_all("\r\n")
        data = dev.read_all("\r\n")
        match = re.findall(r"\+QHTTPGET:.(\d+),(\d+),(\d+)", data)
        if not match:
            raise Exception("Could not read from device")

        errorCode = match[0][0]
        httpCode = match[0][1]
        length = int(match[0][2])

        # read data using UART
        self.write("AT+QHTTPREAD=80")
        self._interface.timeout = 20
        start_time = time.time()
        data = self._interface.read(length).decode("ascii")
        elapsed_time = time.time() - start_time
        print(f"Elapsed time: {elapsed_time}")
        print(len(data))

        return data

    def lte_get_http_read_file(self, aux_file="RAM:website.txt"):
        data = self.execute_command("lte", "AT+QHTTPGET=80")
        self.read_all("\r\n")
        data = self.read_all("\r\n")
        match = re.findall(r"\+QHTTPGET:.(\d+),(\d+),(\d+)", data)
        if not match:
            raise Exception("Could not read from device")

        errorCode = match[0][0]
        httpCode = match[0][1]
        length = int(match[0][2])

        # read http response to a file
        data = self.execute_command("lte", f'AT+QHTTPREADFILE="{aux_file}",80')
        if "CME" in data:
            print(f"Error in response: {data}")

        return data

    def read_file(self, filename):
        # check file existance
        data = self.execute_command("lte", f'AT+QFLST="{filename}"', check_errors=True)

        match = re.search(r"\+QFLST:\s\"(.+)\",(\d+)", data)
        if match:
            size = int(match.group(2))
        else:
            raise Exception(f"Filename {filename} not found!")

        # open file
        data = self.execute_command("lte", f'AT+QFOPEN="{filename}",2', check_errors=True)
        if "CME" in data:
            print(f"Error in response: {data}")
            return

        file_handler = int(re.findall("QFOPEN:\s+(\d+)\r\n", data)[0])
        # read data using UART

        read_size = 0
        blocksize = 512
        aux = ""
        while True:
            data = self.execute_command("lte", f"AT+QFREAD={file_handler},{blocksize}")
            aux += re.search("\r\nCONNECT \d+\r\n(.+)\r\nOK\r\n", data, re.MULTILINE).group(1)
            if len(aux) >= size:
                log.info("All data read")
                break

        data = self.execute_command("lte", f'AT+QFCLOSE={file_handler}')

        return data

    def lte_open_socket(self, host_ip, host_port, mode):
        """
        :param host_ip: server ip
        :param host_port: server port
        :param mode: should use TCP or UDP
        :return: bool, true if open socket successfully, False otherwise
        """
        data = self.execute_command("lte", f'AT+QIOPEN=1,0,"{mode}","{host_ip}",{host_port},0,0')
        return True if "OK" in data else False

    def lte_send_data_socket(self, message, timeout=5):
        """

        :param message: message to send
        :return:
        """
        cmd = f'AT+QISEND=0,{len(message)}'
        self.write(cmd)
        data = self.read_all(f"{cmd}")
        self.write(message)

        data = self.read_all("SEND OK")
        data = self.execute_command("lte", "\n")

        # starttime = time.time()
        #
        # while not "SEND OK" in data and time.time() - starttime < timeout:
        #     data += self.read_all("\n")
        #
        # if not "SEND OK" in data:
        #     log.warning("Not ok in message")

        return data


    def lte_recv_data_socket(self, max_data=1500):
        data = self.execute_command("lte", f"AT+QIRD=0,{max_data}")
        text = re.search(r"\+QIRD:\s\d+\r\n(.+)\r\n\r\nOK", data)

        if text:
            text = text.group(1)

        return text