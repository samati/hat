import string
import struct
import time
from typing import List
from digi.xbee.devices import ZigBeeDevice, RemoteZigBeeDevice
from digi.xbee.models.address import XBee64BitAddress
from digi.xbee.models.message import XBeeMessage
from digi.xbee.exception import TimeoutException
from digi.xbee.models.status import TransmitStatus
from digi.xbee.models.options import DiscoveryOptions
from digi.xbee.util.utils import enable_logger
from hat.utils.utils import retry, list_to_hex_string
import binascii

import logging
from logging.handlers import RotatingFileHandler

LOG_FILENAME = 'ZigbeeInterface.txt'

# hold the configuration for all
CONFIGURATIONS = {
    "nTries": 4,  # number of tries in communication
    "timeout": 10,  # seconds to wait for a timeout,
    "network_timeout": 20,  # timeout for network discovery
    "CLI_prompt": "[CLI]>",  # default prompt
    "CLI_src_endpoint": 0xB3,  # Source endpoint for CLI
    "CLI_dst_endpoint": 0xB3,  # Destination endpoint for CLI
    "CLI_cluster_ID": 0x0011,  # cluster ID for CLI
    "CLI_profile_ID": 0xC105,  # profile ID for CLI
    "FCT_src_endpoint": 0xB2,  # Source endpoint for FCT
    "FCT_dst_endpoint": 0xB2,  # Destination endpoint for FCT
    "FCT_cluster_ID": 0x0011,  # cluster ID for FCT
    "FCT_profile_ID": 0xC105,  # profile ID for FCT
    "ZB_LOG_SENT_LEVEL": logging.DEBUG,  # level of logger for ZIGBEE sent packet
    "ZB_LOG_RECV_LEVEL": logging.DEBUG  # level o loger for ZIGBEE receive packet
}

log = logging.getLogger(__name__)
stream_handler = logging.StreamHandler()
stream_handler.setLevel("DEBUG")
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)-18s - %(levelname)-5s - %(message)s')

# add formatter to ch
stream_handler.setFormatter(formatter)
# add ch to logger
log.addHandler(stream_handler)

# create handler for file logging
# Add the log message handler to the logger
file_handler = RotatingFileHandler(
    LOG_FILENAME, maxBytes=10000000, backupCount=5)

# set formatter for file_handler
file_handler.setFormatter(formatter)
log.addHandler(file_handler)
log.setLevel("DEBUG")

# logger for digi
digi_log = logging.getLogger("digi.xbee.devices")
digi_log_handler = logging.StreamHandler()
stream_handler.setLevel("DEBUG")

# logger for digi
digi_format_string = "%(asctime)s - %(name)-18s - %(levelname)-5s - %(message)s"
digi_log_formatter = logging.Formatter(digi_format_string)
digi_log_handler.setFormatter(digi_log_formatter)
digi_log.addHandler(digi_log_handler)
digi_log.setLevel(CONFIGURATIONS["ZB_LOG_SENT_LEVEL"])

# logger for digi reader
digi_log = logging.getLogger("digi.xbee.reader")
digi_log_handler = logging.StreamHandler()
stream_handler.setLevel("DEBUG")

# logger for digi
digi_log_formatter = logging.Formatter(digi_format_string)
digi_log_handler.setFormatter(digi_log_formatter)
digi_log.addHandler(digi_log_handler)
digi_log.setLevel(CONFIGURATIONS["ZB_LOG_RECV_LEVEL"])

# Printable characters
printable = set(string.printable)


class ZigbeeInterface:
    ROBOT_LIBRARY_SCOPE = "TEST"

    def __init__(self, com_port: str):
        """
        Class to interact with a P7CM over Zigbee

        Parameters
        ----------
        com_port: str, COM address to connect to a zigbee dongle and speak with a P7CM
        """
        self._prompt = CONFIGURATIONS["CLI_prompt"]  # prompt to be used for CLI commands
        self._local_zb = None  # to hold the reference to locall zigbee interface
        self._open = False
        self._com_port = com_port
        self._join_event = {}
        self._remote_zb = None  # to hold reference of te remote device we want to communicate
        log.debug("Zigbee interface load for com port %s", com_port)

    def is_open(self) -> bool:
        """
        Return the state of the connection

        Returns
        -------
        bool -> True if connection is open
        """
        return self._open

    def open(self, com_port: str = None):
        """
        Open zigbee interface UART connection

        Parameters
        ----------
        com_port: str, optional
            COM port to connection. If not provided, will use COM port provided on creation
        """
        log.debug("Zigbee interface opening...")
        if self.is_open():
            print("Zigbee interface already opened!")
            return
        if com_port is not None:
            self._com_port = com_port

        self._local_zb = ZigBeeDevice(self._com_port, 115200)
        self._local_zb.open()

        if not self._local_zb.is_open():
            raise Exception("Could not open local zigbee interface")

        self._open = True

        # self._local_zb.add_data_received_callback(lambda msg: self.receive_callback(msg))
        # FIXME: if this is added this will create problems on the second command sent
        # self._local_zb.add_packet_received_callback(lambda msg: self.receive_callback(msg))

    def network_discovery(self, timeout: int = CONFIGURATIONS["network_timeout"]) -> list:
        """
        Start a new network discovery blocking call

        Parameters
        ----------
        timeout: int, default=20
            default number of seconds to wait for network discovery

        Returns
        -------
        list of ZigbeeNodes
            list of zigbee nodes discovered during network search
        """
        if not self.is_open:
            self.open()

        self._network = self._local_zb.get_network()

        self._network.set_discovery_options({DiscoveryOptions.DISCOVER_MYSELF,
                                             DiscoveryOptions.APPEND_DD})

        self._network.set_discovery_timeout(timeout)  # set discover timeout

        log.debug(f"Start network discovery with timeout = {timeout}s")
        # Start the discovery process and wait for it to be over.
        self._network.start_discovery_process()

        while self._network.is_discovery_running():
            time.sleep(0.5)

        # Get the list of the nodes in the network.
        nodes = self._network.get_devices()

        # return nodes found during discovery
        return nodes

    def discover_node(self, eui: str, timeout: int = CONFIGURATIONS["network_timeout"]):
        """
        Start a new network discovery
        Try to find the specific EUI

        Parameters
        ----------
        eui: string
            address of eui 64bits
        timeout: int, default=20
            Maximum time for network search
        """
        eui = eui.upper()
        log.info(f"Trying to find EUI: {eui}")
        nodes = self.network_discovery(timeout)
        for node in nodes:
            address = binascii.hexlify(node.get_64bit_addr().address).decode("ascii").upper()
            log.debug(f"Zigbee address found: {address}")
            if address == eui:
                return True

        return False

    def set_prompt(self, prompt: str):
        """
        Set prompt to be used for CLI commands

        Parameters
        ----------
        prompt: str
            text to search to conclude a CLI command
        """
        self._prompt = prompt

    def receive_callback(self, message):
        print(f"Data, received: {message}")
        if message.cluster_id == 0x95:
            if message.dest_endpoint == 0xe8:
                data = message.rf_data
                mac = message.x64bit_source_addr
                mac_addr = data[2:10]
                net_addr = struct.unpack('<H', data[0:2])[0]
                ni = ""
                i = 10
                while data[i] != 0:
                    ni += chr(data[i])
                    i += 1
                parent_addr = struct.unpack('>H', data[i + 1:i + 3])[0]
                dev_type = data[i + 3]
                src_evt = data[i + 4]
                manuf_id = struct.unpack('>H', data[i + 7:i + 9])[0]
                if dev_type == 0:
                    dev_string = "coordinator"
                elif dev_type == 1:
                    dev_string = "router"
                elif dev_type == 2:
                    dev_string = "end device"
                else:
                    dev_string = "unknown"
                result_string = "MAC=%s (%04X), NI=%s, parent=0x%04X, device type=%s" % (
                    mac_addr, net_addr, ni, parent_addr, dev_string)
                if src_evt == 1:
                    result_string += ", trigger:pushbutton"
                elif src_evt == 2:
                    result_string += ", trigger:joinevent (JN)"
                elif src_evt == 3:
                    result_string += ", trigger:powercycle (JN)"
                if manuf_id == 0x101e:
                    result_string += ", Manufacturer is DIGI"
                    manufacture = "DIGI"
                else:
                    result_string += ", Unknown manufacturer: 0x%04X" % (manuf_id)
                    manufacture = "Other"
                print("%011.3f: Info from %s" % (time.process_time(), str(mac_addr).strip("'")))
                print(f"              -JOIN NOTIFICATION: {result_string}")

                if mac_addr not in self._join_event:
                    self._join_event[mac_addr] = []

                self._join_event[mac_addr].append({
                    "eui": mac_addr,
                    "time": time.asctime(),
                    "type": src_evt,
                    "manufacture": manufacture
                })

    def get_join_events(self, eui=None):
        if eui is None:
            return self._join_event
        elif eui in self._join_event:
            return self._join_event[eui]
        else:
            return []

    def reset_join_events(self, eui=None):
        if eui is None:
            self._join_event = {}
        elif eui in self._join_event:
            del self._join_event[eui]

    def get_join_events_number(self, eui):
        return len(self._join_event[eui]) if eui in self._join_event else 0

    def set_remote(self, eui):
        """
        Set remote device that we want to communicate

        Parameters
        ----------
        eui: str
            EUI of the remote device that we want to communicate

        """
        # if not connected, try to connect with a device
        if not self.is_open():
            self.open()

        log.info(f"Setting remote zigbee address {eui}")

        # set remote device
        self._remote_zb = RemoteZigBeeDevice(self._local_zb,
                                             XBee64BitAddress.from_hex_string(eui.replace(":", "")))

    @retry(Exception, 4, 2, 1)
    def get_at_parameter(self, at: str, convert: str = "hex"):
        log.debug(f"Zigbee: Getting At command: {at}")
        value = self._remote_zb.get_parameter(at)
        if convert == "hex":
            value_conv = "0x" + binascii.hexlify(value).decode("ascii")
        elif convert == "str":
            value_conv = "".join(filter(lambda x: x in printable, value.decode("ascii")))
        else:
            log.warnig(f"Convert type: {convert}")
            value_conv = value

        return value_conv

    def _read_explicitly_from_remote(self, timeout):
        data = self._local_zb.read_expl_data_from(self._remote_zb, timeout)
        data_ascii = data.data.decode("ascii")
        log.debug(f"Receive commands: {data_ascii}")
        return data_ascii

    def _write_command_to_cli(self, command: str):
        """
        Write command to CLI

        Parameters
        ----------
        command: str
            Command to send to CLI
        """
        log.debug(f"Send CLI command: {command}")
        status = self._local_zb.send_expl_data(self._remote_zb, command,
                                               CONFIGURATIONS["CLI_src_endpoint"],
                                               CONFIGURATIONS["CLI_dst_endpoint"],
                                               CONFIGURATIONS["CLI_cluster_ID"],
                                               CONFIGURATIONS["CLI_profile_ID"])

        if status.transmit_status != TransmitStatus.SUCCESS:
            raise Exception(f"Could not send data successfully, res: {status}")

    @retry(Exception, CONFIGURATIONS["nTries"], 2, 1)
    def execute_command(self, command: str, timeout: int = CONFIGURATIONS["timeout"]):
        """
        Execute a command in the CLI.

        Parameters
        ----------
        command: str
            Command to be execute in the CLI
        timeout: int, optional
            Maximum timeout to wait for an response

        Returns
        -------
        str
            Text response for the command
        """
        log.info(f"Execute CLI command: {command}")
        if not self.is_open():
            self.open()
        self._write_command_to_cli(command)

        starttime = time.time()
        data = ""

        try:
            while time.time() - starttime < timeout:
                message = self._local_zb.read_data(timeout)
                if message.remote_device != self._remote_zb:
                    # not from required device
                    continue

                message_txt = message.data.decode("ascii")
                data += message_txt

                # FIXME: add read explicity
                # data += self._read_explicitly_from_remote(timeout)
                if data.endswith(self._prompt):
                    data = data[:-len(self._prompt)]
                    log.debug(f"Message received: {data}")

                    return data
        except TimeoutException as e:
            print(f"Could not read all information, command {command}, data {data}")

        raise Exception(f"Command: {command}, Could not get all data: {data}")

    @retry(Exception, CONFIGURATIONS["nTries"], 2, 1)
    def execute_fct_command(self, data: List[int], timeout: int = CONFIGURATIONS["timeout"]) -> List[int]:
        """
        Execute FCT command over zigbee

        Parameters
        ----------
        data: list of int
            Data to send to remote zigbee devices

        timeout: int, default=10
            Maximum time to wait for an response

        Returns
        -------
        list of int
            Data returned from the device
        """
        if isinstance(data, list):
            data = bytearray(data)

        log.info(f"Executing FCT command wih data: {list_to_hex_string(data)}")
        message = self._local_zb.send_expl_data(self._remote_zb, data,
                                                CONFIGURATIONS["FCT_src_endpoint"],
                                                CONFIGURATIONS["FCT_dst_endpoint"],
                                                CONFIGURATIONS["FCT_cluster_ID"],
                                                CONFIGURATIONS["FCT_profile_ID"]
                                                )

        if message.transmit_status != TransmitStatus.SUCCESS:
            raise Exception("Could not send message")

        starttime = time.time()
        while time.time() - starttime < timeout:
            message = self._local_zb.read_data(timeout)
            from_remote = self._is_message_from_remote_zb(message)
            log.debug(f"Message received from remote: {from_remote}")

            if from_remote:  # check if message is from remote device
                response_data = message.data
                return response_data

        raise Exception("Could not get an response in time")
        # res = self._local_zb.read_expl_data_from(self._remote_zb, timeout)

    def _is_message_from_remote_zb(self, message: XBeeMessage):
        """
        Check if message receive is from remote zigbee device

        Parameters
        ----------
        message: XBeeMessage
            Message received from zigbee

        Returns
        -------
        bool
            True if message is from remote device
        """
        is_from_remote = message.remote_device._64bit_addr == self._remote_zb._64bit_addr
        return is_from_remote

    def close(self):
        """
        Try to close zigbee interface
        """
        # log.debug("Zigbee Interface closing!")
        if not self.is_open():
            # print("Zigbee already closed")
            return

        try:
            if self._local_zb is not None:
                self._local_zb.close()
                del self._local_zb
        except Exception as e:
            print(f"Could not close local zigbee interface: {e}")

    def __del__(self):
        self.close()


if __name__ == "__main__":
    serial_port = "COM77"
    remote_device_addr = "00:13:A2:00:41:BC:BF:5D"

    xb = ZigbeeInterface(serial_port)
    xb.set_remote(remote_device_addr)

    # print(f"data {data}")
