import time
import pyvisa
import logging
from logging.handlers import RotatingFileHandler

LOG_FILENAME = 'Instrument.txt'
nTries = 3

log = logging.getLogger(__name__)
stream_handler = logging.StreamHandler()
stream_handler.setLevel("DEBUG")
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
stream_handler.setFormatter(formatter)
# add ch to logger
log.addHandler(stream_handler)

# create handler for file logging
# Add the log message handler to the logger
file_handler = RotatingFileHandler(
    LOG_FILENAME, maxBytes=10000000, backupCount=5)

# set formatter for file_handler
file_handler.setFormatter(formatter)
log.addHandler(file_handler)

log.setLevel("DEBUG")

### defaults
DEFAULT_DELAY = 0.1
DEFAULT_TIMEOUT = 10000  # miliseconds of timeout
DEFAULT_VISA_LIBRARY = r"C:\Windows\System32\visa32.dll"


class Instrument:
    def __init__(self, address, config_file=None, write_termination="\n", execute_IDN=True, query_delay=DEFAULT_DELAY, reset=False,
                 clear=True, check_errors=True, timeout=DEFAULT_TIMEOUT, **kwargs):
        self._address = address
        # old delay for query
        self._query_delay = query_delay
        self._check_errors = check_errors
        self._timeout = timeout
        self._execute_IDN = execute_IDN
        self._reset = reset
        self._write_termination = write_termination
        self._clear = clear
        self._intf = None
        self._kwargs = kwargs
        self._open = False  # store the state of connection
        self._config_file = None  # configuration file

    def is_open(self):
        """
        Return if interface is open
        """
        return self._open
    def open(self, address=None):
        if self._open:
            log.warning("Already openned!")
            return

        if address is not None:
            self._address = address

        rm = pyvisa.ResourceManager(DEFAULT_VISA_LIBRARY)
        objs = rm.list_resources()

        if self._address not in objs:
            raise Exception(f"Could not find {self._address} in list of connected devices: {objs}")

        self._intf = rm.open_resource(self._address, write_termination=self._write_termination,
                                      query_delay=self._query_delay,
                                      timeout=self._timeout, **self._kwargs)
        self._intf.timeout = self._timeout

        self._open = True  # interface is openned

        if self._execute_IDN:
            idn = self.get_IDN()
            print(f"IDN: {idn}")
            # log.info(f"IDN: {idn}")

        # check errors by default
        if self._reset:
            self.reset()

        if self._clear:
            self.clear()

        if self._config_file is not None:
            self.load_config(self._config_file)

    def get_IDN(self) -> str:
        idn = self.query("*IDN?")
        return idn

    def reset(self):
        # log.info("Reset")
        self.write("*RST")

    def clear(self):
        # log.info("Clear")
        # self.clear()
        self.write("*CLS", raise_exceptions=False)

    def check_errors(self, command, sleep_time, raise_exceptions):

        # sleep a bit between commands
        time.sleep(sleep_time)

        errors = []
        has_error = False
        start_time = time.time()
        elapsed_time = 0
        while elapsed_time < self._timeout:
            errors.append(self.get_system_error())
            if "no error" in errors[-1].lower():
                break
            else:
                has_error = True
            elapsed_time = time.time() - start_time
        if elapsed_time > self._timeout:
            raise Exception(f"Timeout with command {command} while checking for errors!")
        if has_error:
            if raise_exceptions:
                raise Exception(f"Error(s) '{errors}' after command: {command}")
            else:
                log.warning(f"Error(s) '{errors}' after command: {command}")

    def write(self, command, raise_exceptions=True):
        for i in range(nTries):
            try:
                if not self.is_open():
                    self.open()

                self._intf.write(command)

                if self._check_errors:
                    self.check_errors(command, self._query_delay, raise_exceptions)
                break
            except Exception as e:
                log.exception(f"Problem during command: {e}")
                if i < nTries - 1:
                    log.info("Trying again...")
                    time.sleep(0.5)
                else:
                    log.info("Total tries exceeded")
                    if raise_exceptions:
                        raise e

    def query(self, command):
        if not self.is_open():
            self.open()

        response = self._intf.query(command).strip()
        return response

    def get_system_error(self):
        cmd = "SYSTem:ERRor?"
        error = self.query(cmd)
        return error

    def close(self):
        try:
            if self._intf is None:
                return
            self._intf.close()
            self._open = False  # interface is closed
        except Exception as e:
            print(f"Some error closing interface")

    def __del__(self):
        # log.debug("Delete instrument")
        self.close()
