# reference: https://github.com/sideeffffect/illumeasure/blob/master/illumeasure.py
# seems to have code for this, just needs to adapt

import logging
from functools import reduce
from logging.handlers import RotatingFileHandler
from hat.drivers.Instruments import Instrument
import serial

LOG_FILENAME = 'logs/iluminance_driver.txt'

log = logging.getLogger("Supercap")
stream_handler = logging.StreamHandler()
stream_handler.setLevel("DEBUG")
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
stream_handler.setFormatter(formatter)
# add ch to logger
log.addHandler(stream_handler)

# create handler for file logging\
# Add the log message handler to the logger
file_handler = RotatingFileHandler(
    LOG_FILENAME, maxBytes=10000000, backupCount=5)

# set formatter for file_handler
file_handler.setFormatter(formatter)
log.addHandler(file_handler)

log.setLevel("DEBUG")

### defaults
DEFAULT_DELAY = 0.5

INIT_CMD = "541   "  # command to init remote, 3 spaces


class IlluminanceDriver(Instrument):
    def __init__(self, serial_port):
        self._init_char = "\x02"  # start of frame
        self._term_char = "\x03"  # end of transmission

        # super(IlluminanceDriver, self).__init__(serial_port, "", False, baud_rate=9600, data_bits=7)

        # connection
        self._intf = serial.Serial(serial_port, baudrate=9600, bytesize=7, stopbits=1, parity=serial.PARITY_EVEN)

        connect_message = IlluminanceDriver.message_encode_short(0, INIT_CMD, "")
        # print(connect_message)
        message_encode = connect_message.encode("ascii")

        data = self.query(message_encode)
        res = IlluminanceDriver.parse_response_message(data)
        if res["command"] != 54 or res["status"] != "    ":  # four spaces:
            raise Exception(f"Invalid response, {data}")

    def get_iluminance(self):
        # read iluminance
        iluminace_message = IlluminanceDriver.message_encode_short(0, "10", "0200")
        # print(iluminace_message)
        message_encode = iluminace_message.encode("ascii")

        data = self.query(message_encode)
        res = IlluminanceDriver.parse_response_iluminance(data)
        log.debug(f"Iluminance res {res}")
        return res["iluminance"]

    @staticmethod
    def parse_float(data):
        try:
            sign = -1 if data[0] == "-" else 1
            value = int(data[1:5])
            exp = 10 ** (int(data[5]) - 4)
            value_complete = sign * value * exp
        except ValueError as e:
            log.debug("Problem in conversion: %s", e)
            value_complete = 0

        return value_complete

    @staticmethod
    def parse_response_iluminance(message):
        res = {}
        message = message.decode("ascii")
        if message[0] != "\x02":
            raise Exception("Wrong start charater")
        res["head"] = int(message[1:3])
        res["command"] = int(message[3:5])
        res["status"] = message[5:9]
        res["data"] = message[9:9 + 18]
        start_idx = 9

        i = 0
        data = message[start_idx + i * 6: start_idx + (i + 1) * 6]
        res["iluminance"] = IlluminanceDriver.parse_float(data)

        i += 1
        data = message[start_idx + i * 6: start_idx + (i + 1) * 6]
        res["delta"] = IlluminanceDriver.parse_float(data)

        i += 1
        data = message[start_idx + i * 6: start_idx + (i + 1) * 6]
        res["percentage"] = IlluminanceDriver.parse_float(data)

        if message[27] != "\x03":
            raise Exception("Wrong stop charater")

        expected_bcc = IlluminanceDriver.compute_bcc(message[1:28])
        bcc = message[28:30]
        if expected_bcc.lower() != bcc.lower():
            raise Exception("Wrong bcc")
        if message[30:32] != "\x0d\x0a":
            return Exception("wrong termination")

        return res

    @staticmethod
    def parse_response_message(message):
        res = {}
        message = message.decode("ascii")
        if message[0] != "\x02":
            raise Exception("Wrong start charater")
        res["head"] = int(message[1:3])
        res["command"] = int(message[3:5])
        res["status"] = message[5:9]
        if message[9] != "\x03":
            raise Exception("Wrong stop charater")

        expected_bcc = IlluminanceDriver.compute_bcc(message[1:10])
        bcc = message[10:12]
        if expected_bcc != bcc:
            raise Exception("Wrong bcc")
        if message[12:14] != "\x0d\x0a":
            return Exception("wrong termination")

        return res

    def query(self, command, timeout=10):
        log.debug(f"Command: {command}")
        self._intf.write(command)
        data = self._intf.read_until()
        return data

    @staticmethod
    def compute_bcc(data):
        res = map(ord, data)
        res = reduce(lambda x, y: x ^ y, res)
        return "%02x" % res

    @staticmethod
    def message_encode_short(receptorHead, command, parameter):
        bccable = "%02d" % receptorHead
        bccable += command + parameter + "\x03"  # 0x03 for ETX
        bccResult = IlluminanceDriver.compute_bcc(bccable)
        cmd = "\x02" + bccable + bccResult + "\x0D\x0A"  # \x02 for STX, \x0D for CR, \x0A for LF
        return cmd


def check_complete_command():
    receptorHead = 1
    command = "10"
    parameter = "0200"
    mesg = IlluminanceDriver.message_encode_short(receptorHead, command, parameter)
    expect_mesg = "\x0201100200\x0301\x0d\x0a"

    print(f"Mesg: {mesg}, expected mesg: {expect_mesg}")

    assert mesg == expect_mesg


def check_bcc_implementation():
    message = "01100200\x03"
    bcc = "01"

    bcc_compt = IlluminanceDriver.compute_bcc(message)
    print(f"BCC: {bcc}, BCC computed: {bcc_compt}")
    assert bcc == bcc_compt


if __name__ == "__main__":
    print("Testing illuminance setup")
    check_complete_command()
    check_bcc_implementation()
    # serial_port = "ASRL20::INSTR"
    serial_port = "COM71"
    dev = IlluminanceDriver(serial_port)

    log.setLevel("INFO")

    for i in range(100):
        iluminance = dev.get_iluminance()
        print(f"Iluminance: {iluminance}")

    print("end!")
