import logging
from logging.handlers import RotatingFileHandler
from hat.drivers.Instruments import Instrument

LOG_FILENAME = 'DC_source.txt'

log = logging.getLogger(__name__)
stream_handler = logging.StreamHandler()
stream_handler.setLevel("DEBUG")
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
stream_handler.setFormatter(formatter)
# add ch to logger
log.addHandler(stream_handler)

# create handler for file logging
# Add the log message handler to the logger
file_handler = RotatingFileHandler(
    LOG_FILENAME, maxBytes=10000000, backupCount=5)

# set formatter for file_handler
file_handler.setFormatter(formatter)
log.addHandler(file_handler)

log.setLevel("DEBUG")

AC_SOURCE_MODES = {"FIXED", "LIST", "PULSE", "STEP", "SYNTH", "INTERHAR"}
DEFAULT_TIMEOUT = 10


class DCSource(Instrument):
    def __init__(self, address, write_termination="\r", execute_IDN=True, **kwargs):
        super(DCSource, self).__init__(address, write_termination, execute_IDN, timeout=DEFAULT_TIMEOUT, **kwargs)

    def reset(self):
        sleep_time = 20
        log.debug(f"Doing reset, waiting {sleep_time} seconds before continuing")
        cmd = "*RST"
        self._intf.write(cmd)

        self.check_errors(cmd, sleep_time, True)  # user manual referes waiting 7 seconds

    @staticmethod
    def convert_channel_str(channel):
        if isinstance(channel, int):
            channel = f"CH{channel}"
        if channel not in ["CH1", "CH2", "CH3"]:
            raise Exception(f"Invalid channel: {channel}")
        return channel

    def select_channel(self, channel):
        channel = DCSource.convert_channel_str(channel)
        self.write(f"INST {channel}")

    def get_current_channel(self):
        return self.query("INST?")

    def get_voltage_dc(self, channel):
        voltage = float(self.query(f"measure:voltage:DC? {DCSource.convert_channel_str(channel)}"))
        return voltage

    def set_voltage_dc(self, channel, voltage):
        self.select_channel(channel)
        self.write("voltage %.1f" % voltage)

    def get_current(self, channel):
        # get current
        current = float(self.query(f"MEAS:CURR? {DCSource.convert_channel_str(channel)}"))
        return current

    def set_current(self, channel, current):
        self.select_channel(channel)
        self.write("current %.3f" % current)

    def get_output_state(self, channel):
        self.select_channel(channel)
        state = True if "1" in self.query("CHAN:OUTPut?") else False
        return state

    def set_output_state(self, channel, state):
        self.select_channel(channel)
        self.write(f"CHAN:output {'ON' if state else 'OFF'}")
        # self.write(f"output:relay {'ON' if state else 'OFF'}")

    def get_operating_condition(self, channel):
        channel=DCSource.convert_channel_str(channel)
        status = int(self.query(f"STATus:OPERation:INSTrument:ISUMmary{channel[2]}:CONDition?"))
        res = {}
        res["CV"] = bool(status & 1)
        res["CC"] = bool(status & 2)
        res["CAL"] = bool(status & 4)
        res["OUTPUT"] = bool(status & 8)
        return res


if __name__ == "__main__":
    log.info("DC source interface")

    address = "USB0::0x05E6::0x2230::802895020757510003::INSTR"
    dev = DCSource(address, reset=False, query_delay=1)
    channel = "CH1"

    dev.set_output_state(channel, False)
    print(f"output: {dev.get_output_state(channel)}")
    dev.set_output_state(channel, True)
    print(f"output: {dev.get_output_state(channel)}")

    # set voltage
    dev.set_voltage_dc(channel, 1)
    print(f"voltage {dev.get_voltage_dc(channel)}")

    # set current
    dev.set_current(channel, 0.010)
    print(f"Current {dev.get_current(channel)}")

    print(f"status: {dev.get_operating_condition(channel)}")
    print(f"status: {dev.get_operating_condition('CH2')}")
    print(f"status: {dev.get_operating_condition('CH3')}")
    log.info("End!")
