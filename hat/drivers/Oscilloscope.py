import os
import pyvisa as visa
from pyvisa.resources import MessageBasedResource
import re
import numpy as np
import matplotlib.pyplot as plt
import logging
from hat.drivers.Instruments import Instrument

# LOG_FILENAME = 'logs/PM_1000.txt'

log = logging.getLogger(__name__)
stream_handler = logging.StreamHandler()
stream_handler.setLevel("DEBUG")
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
stream_handler.setFormatter(formatter)
# add ch to logger
log.addHandler(stream_handler)

# create handler for file logging
# Add the log message handler to the logger
# file_handler = RotatingFileHandler(
#     LOG_FILENAME, maxBytes=10000000, backupCount=5)

# set formatter for file_handler
# file_handler.setFormatter(formatter)
# log.addHandler(file_handler)

log.setLevel("DEBUG")


class Oscilloscope(Instrument):
    def __init__(self, address, write_termination="\n", execute_IDN=True):
        super(Oscilloscope, self).__init__(address, write_termination=write_termination, execute_IDN=execute_IDN,
                                           reset=False, check_errors=False, clear=False)

    def open(self):
        # visa.log_to_screen()
        rm = visa.ResourceManager()
        self._intf = rm.open_resource(self._address, resource_pyclass=MessageBasedResource)
        print(self._intf.query('*IDN?'))

        self.turn_off_command_header()

    def query(self, command):
        return self._intf.query(command)

    def _get_waveform_data(self, channel, start, lenght):
        return

    def set_waveform_setup(self, first_point, length=100000):
        self.write(f"WAVEFORM_SETUP SP,0,NP,{length},FP,{first_point},SN,0")

    def query_raw(self, command):
        self.write(command)
        return self._intf.read_raw()

    def set_trigger_mode(self, mode="SINGLE"):
        self.write(f"TRIG_MODE {mode}")

    def get_opc(self):
        return "1" in self.query("*OPC?")

    def is_acquisition_completed(self):
        self.wait_acquisition(1)
        return self.get_opc()

    def wait_acquisition(self, timeout=10):
        self.write(f"WAIT {timeout}")

        if not self.get_opc():
            raise TimeoutError(f"Timeout waiting for answer!")

    def get_waveform(self, channel):
        """ only tested for 100k points, can have problems for more data """
        self.set_waveform_setup(0, 10e6)  # set start of waveform
        # wavesetup = self.query("WAVEFORM_SETUP?")
        wavedesc = self.query(f"{channel}:INSPECT? 'WAVEDESC'")

        wavedesc_dict = {k: v.strip() for k, v in re.findall("(.*?)\s*:\s(.+)\s*", wavedesc)}
        n_points = int(wavedesc_dict['WAVE_ARRAY_1'])
        # points = self.query(f"{channel}:INSPECT?")
        # TODO: add support for longer waveform
        self.set_waveform_setup(0, n_points)  # set start of waveform

        data = self.query_raw(f"{channel}:WAVEFORM? DAT1")

        idx = 0
        while data[idx] != ord(","):
            idx += 1
        points = data[idx + 1:]
        if points[0] != ord("#"):
            raise ValueError("Not starting with #")

        length = points[1] - ord("0")

        data_size = int(points[2:2 + length])
        data_points = points[2 + length:2 + length + data_size]

        data_float = np.fromstring(data_points, '<i1')

        # vertical gain
        vertical_gain = float(wavedesc_dict["VERTICAL_GAIN"])
        vertical_offset = float(wavedesc_dict["VERTICAL_OFFSET"])

        # horizontal
        horizontal_interval = float(wavedesc_dict["HORIZ_INTERVAL"])
        horizontal_offset = float(wavedesc_dict["HORIZ_OFFSET"])

        voltage = data_float * vertical_gain - vertical_offset
        time = np.arange(len(voltage)) * horizontal_interval + horizontal_offset

        return time, voltage

    def write(self, command):
        self._intf.write(command)

    def turn_off_command_header(self):
        self.write("CHDR OFF")

    @staticmethod
    def show_waveform(time, voltage):
        plt.plot(time, voltage)
        plt.xlabel("Time: S")
        plt.ylabel("Voltage: V")
        plt.title("Waveform")
        plt.show()

    def get_screen_capture(self, filename, directory=None):
        """
        Get wavefrom screen from Lecroy and save it in directory and filename

        Tested file format: jpg

        Parameters
        ----------
        filename
        directory

        Returns
        -------

        """
        if directory is not None:
            img_filename = os.path.join(directory, filename)
        else:
            img_filename = filename

        self.write("HCSU DEV, JPEG, AREA, DSOWINDOW, PORT, NET")
        self.write("SCDP")
        # Read the Binary Data
        screen_data = self._intf.read_raw()
        # Save the Binary Data Locally
        with open(img_filename, 'wb+') as f:
            f.write(screen_data)

    def arm_single_trigger(self):
        self.write("TRMD SINGLE")
        self.write("ARM")


# COOM_HEADER turn on header
# CHDR OFF


if __name__ == "__main__":
    log.info("Staring test with oscilloscope")

    # address = "TCPIP0::192.168.8.190::inst0::INSTR"
    address = "VICP::192.168.8.127"

    osc = Oscilloscope(address)
    osc.open()
    osc.arm_single_trigger()

    osc.wait_acquisition()
    # osc.get_screen_capture("Screen.jpg")
    # Python Version 3.7.0
    # This Python example assumes proper reference to the VISA library
    # Please refer to application notes and sections of the manual of initial setups

    # Send Oscilloscope Commands to Configure Screen Image Format
