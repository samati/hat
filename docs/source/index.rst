.. HAT documentation master file, created by
   sphinx-quickstart on Tue Apr 20 19:34:55 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to HAT's documentation!
===============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Introduction
============
Hardware Automation Testing, also know as Hat, is an test automation library

.. automodule:: hat.comms.P7CM_
   :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
