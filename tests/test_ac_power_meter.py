import unittest
from hat.drivers.AcPowerMeterInstrument import AcPowerMeterInstrument

ac_power_meter_address = "USB0::0xABCD::0x03E8::100008201733::INSTR"


class MyTestCase(unittest.TestCase):
    def test_ac_power_meter_open(self):
        pm = AcPowerMeterInstrument(ac_power_meter_address)
        pm.open()

    def test_ac_power_meter_measurements(self):
        pm = AcPowerMeterInstrument(ac_power_meter_address)
        pm.open()

        # active power
        pm.add_meas_active_power()

        power = pm.get_active_power()

        print(f"Active power {power}")

        self.assertTrue(isinstance(power, float))

    def test_ac_power_meter_frequency(self):
        pm = AcPowerMeterInstrument(ac_power_meter_address)
        pm.open()

        pm.add_meas_frequency()

        freq = pm.get_frequency()

        print(f"Freq: {freq}")


if __name__ == '__main__':
    unittest.main()
