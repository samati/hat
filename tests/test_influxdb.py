import unittest
import os
from hat.utils import influxdb_utils

token = "U9RQWFyu5ZJ5deUnQGc8orPxVYP31opAh1ztgwuH8qurEKxmvNGwONTDJS1_y89U3piTOkqmYKGVSFrSZi-cyw=="
org = "hw_lab"
buck_name = "test_results"


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.db = influxdb_utils.InfluxInterface("localhost", 8086, token, org, buck_name)

    def test_insert_point(self):
        self.db.insert_point("power", ["controller", "Luco 1"], ["active power", 20])

    def test_insert_10_points(self):
        from random import randint, random
        import time

        for i in range(10):
            self.db.insert_point("power", ["controller", "Luco 1"], ["active power", randint(18, 19) + random()])
            time.sleep(1)
