import unittest
from hat.comms.DaliUsb import DaliUsb

USB_port = "COM114"


class DaliUsbTest(unittest.TestCase):
    def setUp(self):
        self.dali_usb = DaliUsb(USB_port)

    def test_send_data(self):
        self.dali_usb.send_data([0xE0, 0x02, 0x00, 0xE2])

    def test_get_data(self):
        self.dali_usb.get_data()
