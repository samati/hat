import unittest
import os
from hat.utils import DataStorage

class MyTestCase(unittest.TestCase):
    def test_create_file(self):
        data = DataStorage.DataStorage()
        data.open("file.xls")

        data.add_item("b", 2)
        data["a"] = 1
        data.close()

    def test_add_multiple_item(self):
        data = DataStorage.DataStorage()
        data.open("file.xls")

        data.add_item("parameter", "test", "value", 100, new_line=True)
        data.add_item("parameter", "test2", "value", 200, new_line=True)

        data.close()

        print(data)


if __name__ == '__main__':
    unittest.main()
