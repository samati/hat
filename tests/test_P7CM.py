import unittest
import time
import shutil
import os
from hat.comms import P7CM

EUI = "00:13:A2:00:41:BB:E7:53"
COM_port = "COM77"
USB_CLI = "COM193"


class MyTestCase(unittest.TestCase):
    def test_energy_metering(self):
        p7cm = P7CM.P7CM(COM_port, "ZIGBEE", EUI)
        p7cm.open()

        version = p7cm.get_version()
        print(f"Version: {version}")

        values = p7cm.get_ac_metering()
        print(f"Values: {values}")

    def test_usb_connected(self):
        p7cm = P7CM.P7CM(COM_port, "ZIGBEE", EUI)
        connected = p7cm.is_usb_connected()
        self.assertTrue(connected, "USB should be connected.")

    def test_time_copy_files(self):
        p7cm = P7CM.P7CM(COM_port, "Serial", EUI)
        p7cm.open()
        p7cm._intf.set_prompt("\r\nOK\r\n\n[CLI]>")
        starttime = time.time()

        dirname = os.path.join("../data", "copy_folder")
        if not os.path.exists(dirname):
            os.mkdir(dirname)
        else:
            shutil.rmtree(dirname)
        p7cm.copy_files(dirname)

        elapsedtime = time.time() - starttime
        print(f"Elapsed time: {elapsedtime}")

    def test_dali_init_fct(self):
        p7cm = P7CM.P7CM(COM_port, "ZIGBEE", EUI)
        p7cm.open()

        p7cm.set_dali_dimming_fct(0, 200)

        res = p7cm.perform_dali_reinit()
        print(f"Drivers: {res}")

        p7cm.set_xbee_fct_mode(True)
        response_data = p7cm.set_dali_fct_mode(True)
        res = p7cm.perform_dali_reinit()
        print(f"Drivers: {res}")

        p7cm.set_xbee_fct_mode(True)
        response_data = p7cm.set_dali_fct_mode(False)
        res = p7cm.perform_dali_reinit()
        print(f"Drivers: {res}")

    def test_1_10v_dimming_fct(self):
        p7cm = P7CM.P7CM(COM_port, "ZIGBEE", EUI)
        p7cm.open()

        p7cm.set_1_10v_dimming_fct(100)

        data = p7cm.get_1_10v_feedback_fct()
        print(f"Data: {data}")

    def test_gps(self):
        GPS_COMPORT = "COM198"
        p7cm = P7CM.P7CM(COM_port, "ZIGBEE", EUI)
        p7cm.open()

        data = p7cm.get_gps_configs_at()
        print(f"data: {data}")

        data = p7cm.get_gps_position_fct(GPS_COMPORT)
        print(f"data: {data}")


    def test_scfg(self):
        p7cm = P7CM.P7CM(COM_port, "ZIGBEE", EUI)
        p7cm.open()

        p7cm.get_gemalto_at_scfg()

    def test_file_copy_usb(self):
        p7cm = P7CM.P7CM(USB_CLI, "SERIAL", cli=True)
        p7cm.open()

        version = p7cm.get_version()

        self.assertNotEqual(version, "Version should not be empty")

        data = p7cm.read_file("A:/Configuration/JsonDB")

        print(f"Data: {data}")

    def test_dali(self):
        relay_address = "COM131"
        relay_config = r"..\scripts\robot_lib\relay_config.json"
        from hat.drivers import RelayInterface
        relay = RelayInterface.RelayInterface(relay_address, relay_config)
        relay.set_position_by_label("dali")
        relay.set_position_by_label("dali_power")
        relay.set_position_by_label("dali_led")
        time.sleep(3)

        p7cm = P7CM.P7CM(COM_port, "ZIGBEE", EUI)
        p7cm.open()

        p7cm.execute_command("dali 1")
        res = p7cm._intf.execute_fct_command([0x83, 0x04, 0x81, 0x01, 0x17, 0x10])
        print(f"Res: {res}")

        res = p7cm.set_dali_fct_mode(False)
        print(f"Res: {res}")

        value = p7cm.perform_dali_reinit()
        print(f"Value: {value}")

        res = p7cm.set_dali_dimming_fct(1, 254)
        print(f"Res: {res}")








if __name__ == '__main__':
    unittest.main()
