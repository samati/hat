import unittest
import time
from hat.comms.ZigbeeInterface import ZigbeeInterface

ZB_com_port = "COM2"
UUT = "0013A20041B472A5"


class MyZigbeeCase(unittest.TestCase):
    def setUp(self):
        self.zb = ZigbeeInterface(ZB_com_port)
        self.zb.open()

    def test_zigbee_interface_fct(self):
        """
        Check if FCT commands over zigbee interface are working
        """
        self.zb.set_remote(UUT)
        res = self.zb.execute_fct_command([0xE0, 0x02, 0x00, 0xE2])
        self.assertEqual(res, bytearray(b'\xe0\x03\x00\x00\xe3'), "Incorrect message")

    def test_zigbee_version(self):
        self.zb.set_remote(UUT)
        version = self.zb.execute_command("version")
        self.assertTrue(version, "version not received")

    def test_zigbee_network(self):
        nodes = self.zb.network_discovery()
        print(f"Nodes: {nodes}")

    def test_zigbee_node(self):
        result = self.zb.discover_node(UUT)
        self.assertTrue(result, "It should find the nodes!")
        print(f"Node detection: {result}")

    def tearDown(self) -> None:
        self.zb.close()


if __name__ == '__main__':
    unittest.main()
