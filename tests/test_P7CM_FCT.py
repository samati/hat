import unittest
import time
from hat.comms.P7CM_FCT import P7CM_FCT, InterfaceType
from hat.drivers.RelayInterface import RelayInterface

ip_address = "192.168.8.180"
DALI_USB_PORT = "COM114"
RELAY_BOARD = "COM133"
relay_config = r"C:\PythonCode\hat\scripts\robot_lib\relay_config.json"


class P7CM_FCT_USB(unittest.TestCase):
    def setUp(self) -> None:
        self.relay = RelayInterface(RELAY_BOARD, relay_config)
        self.relay.set_position_by_label("uart")
        time.sleep(5)
        self.p7cm_uart = P7CM_FCT(DALI_USB_PORT, InterfaceType.USB)

    def test_Uart_command(self):
        message = self.p7cm_uart.get_mcu_version()
        self.assertRegex(message, r"\d+\.\d+\.\d+\.\d+", "Message not equal")


class P7CM_FCT_Websocket(unittest.TestCase):
    def test_P7CM_FCT_open(self):
        p7cm = P7CM_FCT(ip_address)
        p7cm.open()

    def test_P7CM_FCT_read_version(self):
        p7cm = P7CM_FCT(ip_address)
        p7cm.open()
        res = {}
        res["version"] = p7cm.get_mcu_version()
        self.assertTrue(len(res["version"]))

        res["product_key"] = p7cm.get_product_key()
        self.assertTrue(len(res["version"]))

        res["production_serial_number"] = p7cm.get_production_serial_number()
        self.assertTrue(len(res["production_serial_number"]))

        print(f"Res: {res}")

    def test_P7CM_FCT_relay(self):
        relay_address = "COM141"
        relay_config = r"..\scripts\robot_lib\relay_config.json"
        from hat.drivers import RelayInterface
        relay = RelayInterface.RelayInterface(relay_address, relay_config)
        relay.set_position_by_label("uart")
        time.sleep(3)

        # open P7CM fct
        p7cm = P7CM_FCT(ip_address)
        p7cm.open()
        res = {}
        res["version"] = p7cm.get_mcu_version()
        self.assertTrue(len(res["version"]))

    def test_P7CM_FCT_MPU(self):
        relay_address = "COM131"
        relay_config = r"..\scripts\robot_lib\relay_config.json"
        from hat.drivers import RelayInterface
        relay = RelayInterface.RelayInterface(relay_address, relay_config)
        relay.set_position_by_label("uart")
        time.sleep(3)

        p7cm = P7CM_FCT(ip_address)
        p7cm.open()
        res = {}

        res["version"] = p7cm.get_mcu_version()
        self.assertTrue(len(res["version"]))

        res["version"] = p7cm.get_mpu_version()
        self.assertTrue(len(res["version"]))

        res["version"] = p7cm.get_gemalto_version()
        print(f"Gemalto: {res['version']}")
        self.assertTrue(len(res["version"]))

        res["version"] = p7cm.get_imei()
        print(f"imei: {res['version']}")
        self.assertTrue(len(res["version"]))

        res["version"] = p7cm.get_imsi()
        print(f"imsi: {res['version']}")
        self.assertTrue(len(res["version"]))

        res["version"] = p7cm.get_xbee_version()
        print(f"Xbee version: {res['version']}")
        self.assertTrue(len(res["version"]))

        res["version"] = p7cm.get_eui()
        print(f"EUI: {res['version']}")
        self.assertTrue(len(res["version"]))

        res["rfid"] = p7cm.get_rfid_data()
        print(f"RFID: {res['rfid']}")
        self.assertTrue(len(res["rfid"]))

    def test_P7CM_FCT_RFID(self):
        relay_address = "COM131"
        relay_config = r"..\scripts\robot_lib\relay_config.json"
        from hat.drivers import RelayInterface
        relay = RelayInterface.RelayInterface(relay_address, relay_config)
        relay.set_position_by_label("uart")
        time.sleep(3)

        p7cm = P7CM_FCT(ip_address)
        p7cm.open()
        res = {}

        res["rfid"] = p7cm.get_rfid_data()
        print(f"RFID: {res['rfid']}")
        self.assertTrue(len(res["rfid"]))

    def test_LSI_sensor(self):
        relay_address = "COM131"
        relay_config = r"..\scripts\robot_lib\relay_config.json"
        from hat.drivers import RelayInterface
        relay = RelayInterface.RelayInterface(relay_address, relay_config)
        relay.set_position_by_label("uart")
        time.sleep(3)

        p7cm = P7CM_FCT(ip_address)
        p7cm.open()
        res = {}

        res["rfid"] = p7cm.set_sensor_input_test("true")
        print(f"RFID: {res['rfid']}")
        self.assertTrue(len(res["rfid"]))

        res["rfid"] = p7cm.set_sensor_input_test("false")
        print(f"RFID: {res['rfid']}")
        self.assertTrue(len(res["rfid"]))


if __name__ == '__main__':
    unittest.main()
