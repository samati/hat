from hat.comms.g4_interface_cli import G4
import re
import os
import pandas as pd
import json
import time
import sys
from hat.utils import programmer

# Not used, as each device has their own certificates
CFG_KEY_CLIENT_CERT = '193'
CFG_KEY_CLIENT_PVKEY = '8'
# Thales Certificates (26 Nov 2021)
CFG_KEY_SUBCA_CERT = '221'
CFG_KEY_ROOT_CERT = '171'

# Factory configuration, method 2 (new):
#   xmodem recv misc (use xmodem to send the config file to the device)
#   factory apply

def helper():
    print("Usage:        ./recalibrate_g4_xmodem_thales.py [socket_type] [controller_type] [operation_type] [device_id|optional]")
    print("Socket Types:          [NEMA] | [ZHAGA]")
    print("Controller Types:      [LIFT] | [MESH]")
    print("Operation Types:       [full] | [recalibrate]")
    print("     [full]          - Install Firmwares + Calibrate + Certificates")
    print("     [recalibrate]   - Calibrate + Certificates Only")
    print("e.g.:")
    print("     ./recalibrate_g4_xmodem_thales.py NEMA LIFT full")
    print("     ./recalibrate_g4_xmodem_thales.py ZHAGA MESH recalibrate")
    print("     ./recalibrate_g4_xmodem_thales.py ZHAGA LIFT recalibrate 14EFCF18FFFF0001")


def update_factory_configuration (factory_config, cal_data):
    """
    This function runs through the factory configurations in factory_config and updates them with the values in cal_data
    Returns the updated factory_config, ready to be saved to a json file
    """

    i = 0

    for config in factory_config["configs"]:
        if "CFG_KEY_LIFT" in config["args"].values():
            factory_config["configs"][i]["args"]["value"] = cal_data['CFG_KEY_LIFT']
        elif "CFG_KEY_NEMA" in config["args"].values():
            factory_config["configs"][i]["args"]["value"] = cal_data['CFG_KEY_NEMA']
        elif "CFG_KEY_RELAY_OFF_DELAY" in config["args"].values():
            factory_config["configs"][i]["args"]["value"] = str(cal_data['relay_off'])
        elif "CFG_KEY_RELAY_ON_DELAY" in config["args"].values():
            factory_config["configs"][i]["args"]["value"] = str(cal_data['relay_on'])
        elif "CFG_KEY_DEV_EUI" in config["args"].values():
            factory_config["configs"][i]["args"]["value"] = cal_data['EUI']
        elif "CFG_KEY_STPM_CORRECT_CURRENT" in config["args"].values():
            factory_config["configs"][i]["args"]["value"] = str(cal_data['current_factor'])
        elif "CFG_KEY_STPM_CORRECT_VOLTAGE" in config["args"].values():
            factory_config["configs"][i]["args"]["value"] = str(cal_data['volt_factor'])
        elif "CFG_KEY_HW_VERSION" in config["args"].values():
            factory_config["configs"][i]["args"]["value"] = cal_data['hw_version']
        i += 1

    return factory_config


def verify_configurations(g4, operation_type, device_id):
    Success = True
    #TODO: in factory mode, check if All LWM2M datapoints exist....
    with open(f"calibration\\After_{operation_type}_{device_id}_complete.txt", "w") as completion_file:
        g4.flush_input()
        g4.set_global_loglevel(4)

        file_output = ''

        # efr_version = g4.get_mesh_fw_version()
        # # simple retry mechanism, as sometimes EFR is not yet available
        # retry_counter = 0
        # while "Invalid" in efr_version:
        #     time.sleep(2)
        #     efr_version = g4.get_mesh_fw_version()
        #     retry_counter += 1
        #     if retry_counter == 20:  # too many retries
        #         raise Exception(f"Error reading EFR32 version! EFR Not Responding. FAILED!")

        # Validate certificates in Flash
        #client_cert = g4.lwm2m_read_cert('Client certifcate')
        client_cert = g4.execute_command_slow('lwm2m read factory-cfg 0 client_cert')
        # if CFG_KEY_CLIENT_CERT not in client_cert:
        if ' 0' in client_cert:
            Success = False
            print("Invalid Client Certificate!!!")
        #root_cert = g4.lwm2m_read_cert('Root certifcate')
        root_cert = g4.execute_command_slow('lwm2m read factory-cfg 0 root_cert')
        if CFG_KEY_ROOT_CERT not in root_cert:
            Success = False
            print("Invalid Root Certificate!!!")
        #subca_cert = g4.lwm2m_read_cert('subCA certificate')
        subca_cert = g4.execute_command_slow('lwm2m read factory-cfg 0 subca_cert')
        if CFG_KEY_SUBCA_CERT not in subca_cert:
            Success = False
            print("Invalid SubCA Certificate!!!")
        #client_pvkey = g4.lwm2m_read_cert('Client private key')
        client_pvkey = g4.execute_command_slow('lwm2m read factory-cfg 0 client_pvkey')
        # if CFG_KEY_CLIENT_PVKEY not in client_pvkey:
        if ' 0' in client_pvkey:
            Success = False
            print("Invalid Private Key!!!")

        file_output += "Client Certificate XOR:" + client_cert
        file_output += "\nRoot Certificate XOR:" + root_cert
        file_output += "\nSubCA Certificate XOR:" + subca_cert
        file_output += "\nPrivate Key XOR:" + client_pvkey

        file_output += "\n" + g4.sys_info()

        # if controller_type == "LIFT":
        #     file_output += "\n" + g4.execute_command("lwm2m read 32774 0 0")  # Read Tunnel for Lift

        # Use LMWM2M reads and writes
        # g4.execute_command_slow("lwm2m write self_test 0 self_test_counter 5")
        # file_output += "\n" + g4.execute_command("lwm2m read self_test 0 self_test_counter")  # Read Self Test Counter
        # file_output += "\n" + g4.execute_command("lwm2m read 3 0 3")  # Read FW Version

        if config["STM_FW"] not in file_output:
            Success = False
            print(f'Wrong MCU FW loaded, expected: { config["STM_FW"] }, current: {file_output}, FAILED!\n')

        if config["EFR_FW"] not in file_output:
            Success = False
            print(f'Wrong EFR32 FW loaded, expected: { config["EFR_FW"] }, current: {file_output}, FAILED!\n')

        if Success == True:
            file_output += '\n-----------------------------'
            file_output += '\nComplete, PASSED!'
            file_output += f'\nProcess Time to {operation_type} : {time.time() - start_time}'
            file_output += '\n-----------------------------\n'
        else:
            file_output += '-----------------------------'
            file_output += 'Complete, FAILED!'
            file_output += f'Process Time to {operation_type} : {time.time() - start_time}'
            file_output += '-----------------------------\n'

        completion_file.write(file_output)
        completion_file.close()

    return Success

if __name__ == "__main__":

    start_time = time.time()

    if len(sys.argv) < 4:
        print("Please verify input Parameters")
        helper()
        exit()
    else:
        if "NEMA" not in sys.argv[1] and "ZHAGA" not in sys.argv[1]:
            print("Unknown Socket Type:", sys.argv[1])
            helper()
            exit()
        if "LIFT" not in sys.argv[2] and "MESH" not in sys.argv[2]:
            print("Unknown Controller Type:", sys.argv[2])
            helper()

            exit()
        if "full" not in sys.argv[3] and "recalibrate" not in sys.argv[3]:
            print("Unknown Operation Type:", sys.argv[3])
            helper()
            exit()

    socket_type = sys.argv[1]
    controller_type = sys.argv[2]
    operation_type = sys.argv[3]
    device_id = None

    if len(sys.argv) == 5: # Device ID was provided:
        if len(sys.argv[4]) != 16:
            print("Invalid Device ID :", sys.argv[4])
            print("\n Device ID must Have 16 Alphanumeric Characters. e.g. 14EFCF18FFFF0001\n")
            exit()
        else:
            device_id = sys.argv[4]

    # Open config file with local environment settings
    with open("recalibrate_g4_config.json") as f:
        config = json.load(f)

        #   STM and EFR Firmware Packages:
    if socket_type == "ZHAGA" and controller_type == "LIFT":
        # TCXO files
        efr_complete_FW = config["efr_complete_FW"]
        stm_hex = config["stm_hex"]
    else:
        # No-TCXO Files
        efr_complete_FW = config["efr_complete_FW_NO_TCXO"]
        stm_hex = config["stm_hex_NO_TCXO"]


    # Validate files:
    if not os.path.isfile(config["stm_exe"]):
        raise Exception(f'Cannot find {config["stm_exe"]}')
    elif not os.path.isfile(config["efr_exe"]):
        raise Exception(f'Cannot find {config["efr_exe"]}')
    elif not os.path.isfile(config["flex_app_hex"]):
        raise Exception(f'Cannot find {config["flex_app_hex"]}')
    elif not os.path.isfile(config["efr_complete_FW"]):
        raise Exception(f'Cannot find {config["efr_complete_FW"]}')
    elif not os.path.isfile(config["stm_hex"]):
        raise Exception(f'Cannot find {config["stm_hex"]}')
    elif not os.path.isfile(config["efr_complete_FW_NO_TCXO"]):
        raise Exception(f'Cannot find {config["efr_complete_FW_NO_TCXO"]}')
    elif not os.path.isfile(config["stm_hex_NO_TCXO"]):
        raise Exception(f'Cannot find {config["stm_hex_NO_TCXO"]}')

    # Open factory config file with local environment settings
    with open("factory_config_xmodem.json") as f:
        factory_config = json.load(f)

    # Overall success status of the process
    Success = True

    # Open G4 CLI Port:
    g4 = G4(config["com_port"])

    g4.set_global_loglevel(4)

    factory_config_filename=""

    # Get Data from Device if Device ID is not provided
    if device_id is None:
        # Get EUI, volt_factor, current_factor, relay_on, relay_off, hw_version
        cal_data = g4.get_cal_data()
        if "hw_version" not in cal_data.keys(): # No Hw version successfully read from device
            cal_data["hw_version"] = "4.0"
        elif len(cal_data["hw_version"]) > 3: # Old Hw version successfully read from device
            cal_data["hw_version"] = "4.0"

        # Add socket type configuration to cal_data
        if socket_type == "NEMA":
            cal_data["CFG_KEY_NEMA"] = "true"
        else:
            cal_data["CFG_KEY_NEMA"] = "false"
        # Add controller type configuration to cal_data
        if controller_type == "LIFT":
            cal_data["CFG_KEY_LIFT"] = "true"
        else:
            cal_data["CFG_KEY_LIFT"] = "false"

        device_id = cal_data['EUI']

        # Update the default settings loaded from factory_config_xmodem.json (factory_config) with the values read from the device (cal_data)
        factory_config = update_factory_configuration(factory_config, cal_data)

        # Create factory_config_xmodem_DEVICEID.json file with the information read from the device
        factory_config_filename = f"calibration\\factory_config_xmodem_" + cal_data['EUI'] + ".json"
        if not os.path.exists("calibration"):
            os.makedirs("calibration")

        if os.path.exists(factory_config_filename):
            print(f"Data for {cal_data['EUI']} already saved! Not overwriting the file")
        else:
            with open(factory_config_filename, "w") as f:
                json.dump(factory_config, f, indent=1)
    else:
        # device_id was provided by the user. Open the already existent file with the factory configurations
        factory_config_filename = f"calibration\\factory_config_xmodem_" + device_id + ".json"
        with open(factory_config_filename) as f:
            factory_config = json.load(f)

    # from here onwards, we only use factory_config

    ##### functions in order #####
    ## [operation_type] == full
    if operation_type == "full":
        # get_data = True
        program_flex_app = False
        install_efr_complete_FW = True
        install_stm = True
        factory_wipe = True
        set_data = True
        #write_certificate = True
        set_exedra_tst_bootstrap = False
        set_mesh_network = False

    ## [operation_type] == full_mesh
    elif operation_type == "full_mesh":
        # get_data = True
        program_flex_app = False
        install_efr_complete_FW = True
        install_stm = True
        factory_wipe = True
        set_data = True
        #write_certificate = True
        set_exedra_tst_bootstrap = False
        set_mesh_network = True

    ## [operation_type] == flash
    elif operation_type == "flash":
        # get_data = True
        program_flex_app = False
        install_efr_complete_FW = True
        install_stm = True
        factory_wipe = False
        set_data = False
        #write_certificate = False
        set_exedra_tst_bootstrap = False
        set_mesh_network = False

    elif operation_type == "recalibrate":
        ## [operation_type] == recalibrate
        # get_data = False
        program_flex_app = False
        install_efr_complete_FW = False
        install_stm = False
        factory_wipe = True
        set_data = True
        #write_certificate = True
        set_exedra_tst_bootstrap = False
        set_mesh_network = False
    else:
        raise Exception("Invalid operation_type! FAILED!")

    if factory_wipe:
        print("Sending factory wipe")
        g4.fcfg_wipe()
        g4.wait_reset(90)

    if program_flex_app:
        print("install Flex app")
        programmer.install_stm32_fw(config["stm_exe"], config["flex_app_hex"], "0x8000000")
        print("Wait flex app start")
        time.sleep(20)
        g4.write("\r\nzigbee-power on\r\n")  # To Power ON the EFR chip
        time.sleep(3)

    if install_efr_complete_FW:
        print("Installing EFR32 complete FW...")
        print(f"\nEFR32 Firmware:    {efr_complete_FW}\n")
        try:
            programmer.install_efr32_fw(config["efr_exe"], efr_complete_FW)
        except:
            raise Exception("Make sure EFR programmer is connected!")

        print("EFR32 complete FW installation successful!\n")

    if install_stm:
        print("Installing STM32 complete FW...")
        print(f"\nSTM32 Firmware:    {stm_hex}\n")
        programmer.install_stm32_fw(config["stm_exe"], stm_hex)
        print("STM32 complete FW installation successful!\n")
        g4.wait_reset()
        # time.sleep(20)
        g4.set_global_loglevel(4)
    else:
        print("No firmware STM32 installed!")

    if set_data:

        if g4.lwm2m_get_fcfg_persisted():
            raise Exception("Cannot write calibration with persisted, FAILED!")

        try:
            g4.upload_xmodem_configurations(factory_config_filename)
        except:
            raise Exception("Failed to write file via xmodem")

        if not g4.apply_factory_config(sleep_time=7):
            raise Exception("Failed to apply factory configurations")

        Success = verify_configurations(g4, operation_type, device_id)

        if Success == True:
            print('\033[32m' + '-----------------------------')
            print('\033[32m' + 'Complete, PASSED!')
            print('\033[32m' + f'Process Time to {operation_type} : {time.time() - start_time}')
            print('\033[32m' + '-----------------------------\n')
            exit()
        else:
            print('\033[31m' + '-----------------------------')
            print('\033[31m' + 'Complete, FAIL!')
            print('\033[31m' + '-----------------------------\n')
            exit()

        # Code shall not reach here! TO BE REMOVED
        print("Resetting the device after factory Configurations have been applied! Timeout: 120s")
        g4.reset()
        g4.wait_reset(120)
        # g4.flush_input()

    if set_exedra_tst_bootstrap:

        bootstrap_server_uri = ''

        if controller_type == "LIFT":
            # Set TST Exedra Bootstrap Server for Lift Nodes (IPv4)
            bootstrap_server_uri = "coap://10.100.99.1:5683"
            # Set TST Central Bridge connection in Lift Nodes
            g4.execute_command_slow("lwm2m write 32774 0 0 10.100.193.5")

        elif controller_type == "MESH":
            # Set TST Exedra Bootstrap Server for Mesh Nodes (IPv6)
            bootstrap_server_uri = "coap://[fdf7:a36c:4641:139::f1]:5683"

        g4.execute_command_slow("lwm2m write self_test 0 self_test_counter 5")
        g4.execute_command_slow("lwm2m bootstrap " + bootstrap_server_uri)
        g4.execute_command_slow("lwm2m bootstrap")

    # Do this at the end, only when everything has been verified OK
    with open(f"calibration\\After_{operation_type}_{device_id}_complete.txt", "w") as completion_file:
        g4.flush_input()
        g4.set_global_loglevel(4)

        file_output = ''

        efr_version = g4.get_mesh_fw_version()
        # simple retry mechanism, as sometimes EFR is not yet available
        retry_counter = 0
        while "Invalid" in efr_version:
            time.sleep(2)
            efr_version = g4.get_mesh_fw_version()
            retry_counter += 1
            if retry_counter == 20:  # too many retries
                raise Exception(f"Error reading EFR32 version! EFR Not Responding. FAILED!")

        # Validate certificates in Flash
        client_cert = g4.lwm2m_read_cert('Client certifcate')
        if CFG_KEY_CLIENT_CERT not in client_cert:
            Success = False
            print("Invalid Client Certificate!!!")
        root_cert = g4.lwm2m_read_cert('Root certifcate')
        if CFG_KEY_ROOT_CERT not in root_cert:
            Success = False
            print("Invalid Root Certificate!!!")
        subca_cert = g4.lwm2m_read_cert('subCA certificate')
        if CFG_KEY_SUBCA_CERT not in subca_cert:
            Success = False
            print("Invalid SubCA Certificate!!!")
        client_pvkey = g4.lwm2m_read_cert('Client private key')
        if CFG_KEY_CLIENT_PVKEY not in client_pvkey:
            Success = False
            print("Invalid Private Key!!!")

        file_output += "Client Certificate XOR:" + client_cert
        file_output += "\nRoot Certificate XOR:" + client_cert
        file_output += "\nSubCA Certificate XOR:" + client_cert
        file_output += "\nPrivate Key XOR:" + client_cert

        file_output += "\n" + efr_version

        if controller_type == "LIFT":
            file_output += "\n" + g4.execute_command("lwm2m read 32774 0 0")  # Read Tunnel for Lift
        g4.execute_command_slow("lwm2m write self_test 0 self_test_counter 5")
        file_output += "\n" + g4.execute_command("lwm2m read self_test 0 self_test_counter")  # Read Self Test Counter
        file_output += "\n" + g4.execute_command("lwm2m read 3 0 3")  # Read FW Version

        if set_mesh_network:
            retry_counter = 0
            response = g4.execute_command('lwm2m write 3440 0 20 "HYPERION_LOAD"')
            while "Invalid" in response:
                time.sleep(5)
                response = g4.execute_command('lwm2m write 3440 0 20 "HYPERION_LOAD"')
                retry_counter += 1
                if retry_counter == 5:  # too many retries
                    raise Exception(f"Error writing WiSun network Name")
        else:
            print("Mesh Network Name Not Set!")
        file_output += "\n" + g4.execute_command('lwm2m read 3440 0 20')
        file_output += "\n" + g4.sys_info()
        file_output += "\n Operation Time: " + str(time.time() - start_time)

        completion_file.write(file_output)
        completion_file.close()

    # if operation_type == "flash":
    #     g4.fsclear(wait_end=True)


    if config["STM_FW"] not in file_output:
        Success = False
        print(f'Wrong MCU FW loaded, expected: { config["STM_FW"] }, current: {file_output}, FAILED!')

    if config["EFR_FW"] not in file_output:
        Success = False
        print(f'Wrong EFR32 FW loaded, expected: { config["EFR_FW"] }, current: {file_output}, FAILED!')

    if Success == True:
        print('\033[32m' + '-----------------------------')
        print('\033[32m' + 'Complete, PASSED!')
        print('\033[32m' + f'Process Time to {operation_type} : {time.time() - start_time}')
        print('\033[32m' + '-----------------------------\n')
    else:
        print('\033[31m' + '-----------------------------')
        print('\033[31m' + 'Complete, FAIL!')
        print('\033[31m' + '-----------------------------\n')
