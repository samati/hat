import time
import logging

import hat.utils.DataStorage
from hat.drivers import Oscilloscope
from hat.utils import utils
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import scipy as sy
import scipy.fftpack as syfp
from hat.comms import g4_interface_cli

log = logging.getLogger(__name__)
stream_handler = logging.StreamHandler()
stream_handler.setLevel("DEBUG")
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
stream_handler.setFormatter(formatter)
# add ch to logger
log.addHandler(stream_handler)

log.setLevel("DEBUG")


def calculate_RMS(voltage):
    return np.sqrt(np.mean(voltage ** 2))


def calculate_frequency(t, u):
    dt = time[1] - time[0]
    # Do FFT analysis of array
    FFT = sy.fft.fft(u)

    # Getting the related frequencies
    freqs = syfp.fftfreq(len(u), dt)  ## added dt, so x-axis is in meaningful units

    # Create subplot windows and show plot
    plt.subplot(211)
    plt.plot(t, u)
    plt.xlabel('Time')
    plt.ylabel('Amplitude')
    plt.subplot(212)
    abs_fft = sy.log10(abs(FFT))  # get max frequency
    freq_max = freqs[np.argmax(abs_fft)]
    plt.plot(freqs, sy.log10(abs(FFT)), '.')  ## it's important to have the abs here
    plt.xlim(-100, 100)  ## zoom into see what's going on at the peak
    plt.show()


def calculate_phase(voltage, voltage_RMS):
    return


def create_voltage_waveform(time, voltage_RMS, freq, phase):
    wav = np.sqrt(2) * voltage_RMS * np.sin(2 * np.pi * freq * time + 2 * np.pi * phase / 360)
    return wav


def iterative_search_frequency(time, voltage, frequency_guess=50):
    voltage_RMS = calculate_RMS(voltage)

    deviation = 1
    freq_resolution = 0.1

    phase_step = 1

    freqs = np.arange(frequency_guess - deviation, frequency_guess + deviation, freq_resolution)
    phases = np.arange(0, 360, phase_step)

    res = pd.DataFrame()
    idx = 0

    for freq in freqs:
        # print(f"[Freq]: {freq}Hz")

        for phase in phases:
            # print(f"[Phase]: {phase}º")
            wav = create_voltage_waveform(time, voltage_RMS, freq, phase)
            error = np.sqrt(np.sum((voltage - wav) ** 2))

            res.loc[idx, "Freq"] = freq
            res.loc[idx, "Phase"] = phase
            res.loc[idx, "Error"] = error
            idx += 1
            #
            # plt.plot(time, wav)
            # plt.plot(time, voltage)
            # plt.close()
    min_idx = res["Error"].idxmin()
    freq_min = res.loc[min_idx, "Freq"]
    phase_min = res.loc[min_idx, "Phase"]

    #
    # wav_best = np.sqrt(2) * voltage_RMS * np.sin(2 * np.pi * freq_min * time + 2 * np.pi * phase_min / 360)
    # plt.figure()
    # plt.plot(time, voltage)
    # plt.plot(time, wav_best)
    # plt.show()

    # print(res)
    res_dic = {}
    res_dic["freq_min"] = freq_min
    res_dic["phase_min"] = phase_min
    return res_dic


def get_delay(time, voltage_ac, voltage_out, relay_signal=None, method="raise", threshold=15, plot=False):
    voltage_RMS = calculate_RMS(voltage_ac)

    res = iterative_search_frequency(time, voltage_ac, 60)
    estimated_voltage = create_voltage_waveform(time, voltage_RMS, res["freq_min"], res["phase_min"])
    diff_wav = estimated_voltage - voltage_out

    # calculate_frequency(time_ac, voltage_ac)
    if method == "waveform":

        idx = np.where(np.abs(diff_wav) > threshold)[0][-1]  # last point bigger than threshold
    elif method == "raise":
        idx = np.where(np.abs(voltage_out) > threshold)[0][0]
    else:
        raise Exception(f"Method {method}, not implemented")

    delay = time[idx]

    if plot:
        plt.plot(time, voltage_ac)
        plt.plot(time, estimated_voltage)
        plt.plot(time, voltage_out)
        plt.axvline(delay, color="r")
        plt.show()

    phase_rad = 2 * np.pi * res["freq_min"] * delay + 2 * np.pi * res["phase_min"] / 360
    phase_degree = phase_rad * 360 / (2 * np.pi)

    voltage_by_phase = np.sqrt(2) * voltage_RMS * np.sin(phase_rad)
    res_dict = {}
    if relay_signal is not None:
        # try to get relay time
        relay_digital = np.array(relay_signal) > 1.75
        relay_command_switch_idx = np.where(relay_digital)[0][0]
        relay_command_switch_time = time[relay_command_switch_idx]
        relay_delay = delay - relay_command_switch_time
        res_dict["relay_delay"] = relay_delay

    res_dict["delay"] = delay
    res_dict["idx"] = idx
    res_dict["phase_rad"] = phase_rad
    res_dict["phase_degree"] = phase_degree
    res_dict["switch_voltage"] = voltage_by_phase

    phase_error = phase_degree % 180
    if phase_error > 90:
        phase_error = 180 - phase_error
    res_dict["phase_error_degree"] =phase_error


    return res_dict


if __name__ == "__main__":
    print("Relay measurement script")

    # connecton address
    g4_com_port = "COM50"
    address = "VICP::192.168.8.190"

    # connect to devices
    osc = Oscilloscope.Oscilloscope(address)
    g4 = g4_interface_cli.G4(g4_com_port)
    g4.set_global_loglevel(4)

    data = hat.utils.DataStorage.DataStorage("relay_switching")

    for i in range(10):
        g4.set_relay_state(False)
        osc.set_trigger_mode("STOP")
        time.sleep(3)

        osc.set_trigger_mode("SINGLE")

        g4.set_relay_state(True)

        time.sleep(3)
        osc.wait_acquisition(10)


        time_ac, voltage_ac = osc.get_waveform("C2")
        wav_time, voltage_out = osc.get_waveform("C3")
        wav_time, voltage_relay = osc.get_waveform("C4")

        res_dict = get_delay(wav_time, voltage_ac, voltage_out, voltage_relay)

        data["Test #"] = i
        for key, value in res_dict.items():
            data[key] = value

        data.start_next_test()
        print(f"switch voltage = {res_dict}")
        print(f"Phase error= {res_dict['phase_error_degree']}º")
        print(f"Voltage = {res_dict['switch_voltage']}V")

    data.close()
    print("end!")
