from hat.comms.g4_interface_cli import G4
import re
import os
import pandas as pd
import json
import time
import sys
from hat.utils import programmer


# def parse_file(filename):
# data = pd.DataFrame()

# with open(os.path.join(dirname, filename)) as f:
# # print(f"file open")
# for line in f:
# # print(line)
# res = parse_line(line)
# for key, value in res.items():
# data.loc[res["EUI"], key] = value

# return data


# def parse_line(line):
# res_dict = re.search(
# "EUI:(?P<EUI>.+)\sCERT_0:(?P<CERT_0>.+)\sCERT_1:(?P<CERT_1>.+)\sXOR0:(?P<XOR0>\d+)\sXOR1:(?P<XOR1>\d+)", line)
# if not res_dict:
# raise Exception(f"Not match in line {line}")
# return res_dict.groupdict()


def write_certificate_func(g4, sleeptime=1):
    root = [
        "MIIBojCCAUmgAwIBAgIFAPf29t8wCgYIKoZIzj0EAwIwMTEOMAwGA1UECgwFV2lT",
        "VU4xEzARBgNVBAMMCldpU1VOIFJvb3QxCjAIBgNVBAUTATEwIBcNMTcwNzA0MjEw",
        "NDAyWhgPOTk5OTEyMzEyMzU5NTlaMDExDjAMBgNVBAoMBVdpU1VOMRMwEQYDVQQD",
        "DApXaVNVTiBSb290MQowCAYDVQQFEwExMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcD",
        "QgAEVrJidrjt1nWFVA/VOA/1XKARMBv9t+9XbnErRKJhwudMEUguImA6Ns5TS6o6",
        "wDDGjYF6JzO1htL1+MTNFkId+KNMMEowDwYDVR0TAQH/BAUwAwEB/zARBgNVHQ4E",
        "CgQIQP089aoIBgswDgYDVR0PAQH/BAQDAgEGMBQGA1UdIAEB/wQKMAgwBgYEVR0g",
        "ADAKBggqhkjOPQQDAgNHADBEAiBYqSiYL0Tu8D/zzU8TajVkzKMw6qkku+UIezPn",
        "2Tv6rAIgP5+YwqTLC9QTfuz2/XN6Ta5Hd4zgyLSvc1BXm94r06k="
    ]

    client_cert = [
        "MIIB3jCCAYWgAwIBAgIFAKixNr0wCgYIKoZIzj0EAwIwMTEOMAwGA1UECgwFV2lT",
        "VU4xEzARBgNVBAMMCldpU1VOIFJvb3QxCjAIBgNVBAUTATEwIBcNMTkwOTI5MjA1",
        "MjIxWhgPOTk5OTEyMzEyMzU5NTlaMAAwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNC",
        "AAQ4EsatK3OlLNZKk8BLIzKACGGly15B4gbF+kZXEA80zupzGaa6pkdNIMRXz2Xt",
        "PKfukDscscotRwHbO+/PRKkKo4G4MIG1MFEGA1UdIwRKMEiACED9PPWqCAYLoTWk",
        "MzAxMQ4wDAYDVQQKDAVXaVNVTjETMBEGA1UEAwwKV2lTVU4gUm9vdDEKMAgGA1UE",
        "BRMBMYIFAPf29t8wDgYDVR0PAQH/BAQDAgOIMC0GA1UdEQEB/wQjMCGgHwYIKwYB",
        "BQUHCASgEzARBgkrBgEEAYLkJQEEBNAAAAIwIQYDVR0lAQH/BBcwFQYJKwYBBAGC",
        "5CUBBggrBgEFBQcDAjAKBggqhkjOPQQDAgNHADBEAiBe+v3HRb4gmCthI7to7xHr",
        "qbw+qKa+tS/g0C7FN7v6wwIgdBtHOII/KHrCPydtWknp3EA3CO19lptL8EkSxupL",
        "LJg="
    ]

    client_pvkey = [
        "rynVdiNEov9Wt7qdvp0FhZmz5Sz3sJV0lPW4o3SF6oI6BKJW7tKKPF9u48cWXD6P"
        "0ouUJtjFdiyojcQzQggFERD8+JD40uOYtJDem742+lM2uRdXVFpTA1+PfeZ54Jd8"
        "hxRBjAZJ88tZ1aLAO1SK6sYOfqVYiSPZiOIvZSIGmiSvgRssc7jvrSOhWersSRsq"
        "T41LkFGQljg9rgo6AXrmrQMotsd71YVwO0yDKnhI"
    ]

    g4.lwm2m_write_cert("Root certifcate", "".join(root), 213, sleeptime)
    g4.lwm2m_write_cert("subCA certificate", "".join(root), 213, sleeptime)
    g4.lwm2m_write_cert("Client certifcate", "".join(client_cert), 193, sleeptime)
    g4.lwm2m_write_cert("Client private key", "".join(client_pvkey), 26, sleeptime)


if __name__ == "__main__":

    with open("recalibrate_g4_config.json") as f:
        config = json.load(f)

    # G4 CLI Port:
    g4 = G4(config["com_port"])
    g4.set_global_loglevel(4)

    # Get Device ID from device
    device_id = g4.lwm2m_get_eui()

    # Verify Device Type
    # LIFT NEMA  = 20, LIFT ZHAGA = 10, MESH NEMA  = 28, MESH ZHAGA = 18
    # [socket_type]=ZHAGA && [controller_type]=LIFT uses TCXO firmware
    # [controller_type]=MESH has different bootstrap server than [controller_type]=LIFT
    if device_id[6] == "2":
        socket_type = "NEMA"
    elif device_id[6] == "1":
        socket_type = "ZHAGA"
    else:
        raise Exception("Invalid Device ID read from device, FAILED!")

    if device_id[7] == "0":
        controller_type = "LIFT"
    elif device_id[7] == "8":
        controller_type = "MESH"
    else:
        raise Exception("Invalid Device ID read from device, FAILED!")

    # Use input parameters: [operation_type]

    if len(sys.argv) < 2:
        print("Please verify input Parameters:")
        print("     ./recalibrate_g4.py [operation_type]")
        print("e.g.:")
        print("     ./recalibrate_g4.py full")
        print("     ./recalibrate_g4.py full_mesh")
        print("     ./recalibrate_g4.py recalibrate")
        print("     ./recalibrate_g4.py flash")
        exit()
    else:
        if "full" not in sys.argv[1] and "full_mesh" not in sys.argv[1] and "recalibrate" not in sys.argv[1] and "flash" not in sys.argv[1]:
            print("Unknown Operation Type:", sys.argv[1])
            print("\nOperation Types:       [full] / [flash] /[recalibrate]\n")
            print("[full]          - Install Firmwares + Calibrate + Certificates\n")
            print("[full_mesh]          - Install Firmwares + Calibrate + Certificates + Set Wisun Network to HYPERION_LOAD\n")
            print("[flash]          - Install Firmwares Only\n")
            print("[recalibrate]   - Calibrate + Certificates Only\n")
            exit()

    operation_type = sys.argv[1]

    ##### functions in order #####
    ## [operation_type] == full
    if operation_type == "full":
        get_data = True
        program_flex_app = False
        install_efr_complete_FW = True
        install_stm = True
        factory_wipe = True
        set_data = True
        write_certificate = True
        bootstrap_exedra = False
        set_mesh_network = False

    ## [operation_type] == full_mesh
    if operation_type == "full_mesh":
        get_data = True
        program_flex_app = False
        install_efr_complete_FW = True
        install_stm = True
        factory_wipe = True
        set_data = True
        write_certificate = True
        bootstrap_exedra = False
        set_mesh_network = True

    ## [operation_type] == flash
    elif operation_type == "flash":
        get_data = True
        program_flex_app = False
        install_efr_complete_FW = True
        install_stm = True
        factory_wipe = False
        set_data = False
        write_certificate = False
        bootstrap_exedra = False
        set_mesh_network = False

    elif operation_type == "recalibrate":
        ## [operation_type] == recalibrate
        get_data = False
        program_flex_app = False
        install_efr_complete_FW = False
        install_stm = False
        factory_wipe = True
        set_data = True
        write_certificate = True
        bootstrap_exedra = False
        set_mesh_network = False

        device_id = g4.lwm2m_get_eui()

        if len(device_id) != 16:
            print("Invalid Device ID :", device_id)
            print("\n Device ID must Have 16 Characters\n")
            exit()
        #Load previously created JSON file
        json_file = "calibration/" + device_id + ".json"

    else:
        raise Exception("Invalid operation_type! FAILED!")

    # only used if get_data=False
    # json_file = ""

    #   STM and EFR Firmware Packages:
    if socket_type == "ZHAGA" and controller_type == "LIFT":
        # TCXO files
        efr_complete_FW = config["efr_complete_FW"]
        stm_hex = config["stm_hex"]
    else:
        # No-TCXO Files
        efr_complete_FW = config["efr_complete_FW_NO_TCXO"]
        stm_hex = config["stm_hex_NO_TCXO"]

    print(f"\nSTM32 Firmware:    {stm_hex}\n")
    print(f"\nEFR32 Firmware:    {efr_complete_FW}\n")

    # Validate config files:
    if not os.path.isfile(config["stm_exe"]):
        raise Exception (f'Cannot find {config["stm_exe"]}')
    elif not os.path.isfile(config["efr_exe"]):
        raise Exception (f'Cannot find {config["efr_exe"]}')
    elif not os.path.isfile(config["flex_app_hex"]):
        raise Exception (f'Cannot find {config["flex_app_hex"]}')
    elif not os.path.isfile(config["efr_complete_FW"]):
        raise Exception (f'Cannot find {config["efr_complete_FW"]}')
    elif not os.path.isfile(config["stm_hex"]):
        raise Exception (f'Cannot find {config["stm_hex"]}')
    elif not os.path.isfile(config["efr_complete_FW_NO_TCXO"]):
        raise Exception (f'Cannot find {config["efr_complete_FW_NO_TCXO"]}')
    elif not os.path.isfile(config["stm_hex_NO_TCXO"]):
        raise Exception (f'Cannot find {config["stm_hex_NO_TCXO"]}')


    # Get Data from Device
    if get_data:
        g4_data = g4.get_cal_data()
        if "hw_version" not in g4_data.keys():
            g4_data["hw_version"] = "20200701"

        filename = f"calibration\\{g4_data['EUI']}.json"
        if not os.path.exists("calibration"):
            os.makedirs("calibration")

        if os.path.exists(filename):
            print(f"Data for {g4_data['EUI']} already saved!")
        else:
            with open(filename, "w") as f:
                json.dump(g4_data, f)
    else:
        with open(json_file) as f:
            g4_data = json.load(f)

    if program_flex_app:
        print("install Flex app")
        programmer.install_stm32_fw(config["stm_exe"], config["flex_app_hex"], "0x8000000")
        print("Wait flex app start")
        time.sleep(20)
        g4.write("\r\nzigbee-power on\r\n")  # To Power ON the EFR chip
        time.sleep(3)

    print("Install firmwares")

    if install_efr_complete_FW:
        print("Installing EFR32 complete FW...")
        try:
            programmer.install_efr32_fw(config["efr_exe"], efr_complete_FW)
        except:
            raise Exception("Make sure EFR programmer is connected!")

        print("EFR32 complete FW installation successful!\n")

    if install_stm:

        print("Installing STM32 complete FW...")
        programmer.install_stm32_fw(config["stm_exe"], stm_hex)
        print("STM32 complete FW installation successful!\n")
        g4.wait_reset()
        time.sleep(20)
        g4.set_global_loglevel(4)
    else:
        print("No firmware STM32 installed!")

    # if factory_wipe and g4.lwm2m_get_fcfg_persisted():
    if factory_wipe:
        g4.fcfg_wipe()
        g4.wait_reset()
        time.sleep(50)

    # always set global log level
    g4.set_global_loglevel(4)

    if set_data:  # set data

        # if g4.lwm2m_get_fcfg_persisted():
        #    raise Exception("Cannot write calibration with persisted, FAILED!")

        # write remaining calibration parameters
        g4.lwm2m_set_eui(g4_data["EUI"])
        g4.lwm2m_set_voltage_factor(g4_data["volt_factor"])
        g4.lwm2m_set_current_factor(g4_data["current_factor"])
        g4.lwm2m_set_relay_on(g4_data["relay_on"])
        g4.lwm2m_set_relay_off(g4_data["relay_off"])
        g4.lwm2m_set_hw_version(g4_data["hw_version"])

        if write_certificate:
            try:
                # g4.lwm2m_write_cert("client", data.loc[g4_data["EUI"], "CERT_1"], data.loc[g4_data["EUI"], "XOR1"])
                # g4.lwm2m_write_cert("root", data.loc[g4_data["EUI"], "CERT_0"], data.loc[g4_data["EUI"], "XOR0"])
                write_certificate_func(g4)
            except:
                g4.lwm2m_set_fcfg_persisted()
                g4.reset(wait_end=False)
                raise Exception("Exception Caught In Certificates writing. FAILED!!!")

        g4.lwm2m_set_fcfg_persisted()
        g4.fsclear(wait_end=True)

        g4.set_global_loglevel(4)
        # Mesh Zhaga
        time.sleep(10)

        if not g4.lwm2m_get_fcfg_persisted():
            raise Exception("Could not save calibration, FAILED!")

    # TODO: add verify calibration data, fw versions

    if bootstrap_exedra:

        bootstrap_server_uri = ''

        if controller_type == "LIFT":
            # Set TST Exedra Bootstrap Server for Lift Nodes (IPv4)
            bootstrap_server_uri = "coap://10.100.99.1:5683"
            # Set TST Central Bridge connection in Lift Nodes
            g4.execute_command("lwm2m write 32774 0 0 10.100.193.5")
            g4.execute_command("lwm2m write location 0 latitude 38.7225")
            g4.execute_command("lwm2m write location 0 longitude -9.23684")

        elif controller_type == "MESH":
            # Set TST Exedra Bootstrap Server for Mesh Nodes (IPv6)
            bootstrap_server_uri = "coap://[fdf7:a36c:4641:139::f1]:5683"

        g4.execute_command("lwm2m write self_test 0 self_test_counter 5")
        # g4.set_bootstrap_server(bootstrap_server_uri)
        g4.execute_command("lwm2m bootstrap " + bootstrap_server_uri)
        g4.execute_command("lwm2m bootstrap")

    # Do this at the end, only when everything has been verified OK
    with open(f"calibration\\After_{operation_type}_{g4_data['EUI']}_complete.txt", "w") as f:
        g4.set_global_loglevel(4)

        efr_version = g4.get_mesh_fw_version()
        # simple retry mechanism, as sometimes EFR is not yet available
        retry_counter = 0
        while "Invalid" in efr_version:
            time.sleep(5)
            efr_version = g4.get_mesh_fw_version()
            retry_counter += 1
            if retry_counter == 5:  # too many retries
                raise Exception(f"Error reading EFR32 version! EFR Not Responding. FAILED!")

        res = efr_version

        if controller_type == "LIFT":
            res += "\n" + g4.execute_command_slow("lwm2m read 32774 0 0")  # Read Tunnel for Lift
        g4.execute_command_slow("lwm2m write self_test 0 self_test_counter 5")
        res += "\n" + g4.execute_command_slow("lwm2m read self_test 0 self_test_counter")  # Read Self Test Counter
        res += "\n" + g4.execute_command_slow("lwm2m read 3 0 3")  # Read FW Version

        if set_mesh_network:
            retry_counter = 0
            response = g4.execute_command_slow('lwm2m write 3440 0 20 "HYPERION_LOAD"')
            while "Invalid" in response:
                time.sleep(5)
                response = g4.execute_command_slow('lwm2m write 3440 0 20 "HYPERION_LOAD"')
                retry_counter += 1
                if retry_counter == 5:  # too many retries
                    raise Exception(f"Error writing WiSun network Name")
        else:
            print("Mesh Network Name Not Set!")
        res += "\n" + g4.execute_command_slow('lwm2m read 3440 0 20')
        res += "\n" + g4.sys_info()
        f.write(res)

    if operation_type == "flash":
        g4.fsclear(wait_end=True)

    print("Checking if correct data is stored")

    if config["STM_FW"] not in res:
        raise Exception(f'Wrong MCU FW loaded, expected: { config["STM_FW"] }, current: {res}, FAILED!')

    if config["EFR_FW"] not in res:
        raise Exception(f'Wrong EFR32 FW loaded, expected: { config["EFR_FW"] }, current: {res}, FAILED!')

    # if bootstrap_server_uri not in res:
    #    raise Exception(f"Incorrect Bootstrap Server set, expected: {bootstrap_server_uri}, current: {res}, FAILED!")

    print("-----------------------------")
    print("Complete, PASS!")
    print("-----------------------------\n")
