from hat.utils.influxdb_utils import InfluxInterface
from hat.drivers.AcPowerMeterInstrument import AcPowerMeterInstrument
from hat.drivers.RelayInterface import RelayInterface


import time
from random import randint, random

if __name__ == "__main__":
    # variables
    pm_address = "USB0::0xABCD::0x03E8::100008201733::INSTR"

    # database config
    token = "U9RQWFyu5ZJ5deUnQGc8orPxVYP31opAh1ztgwuH8qurEKxmvNGwONTDJS1_y89U3piTOkqmYKGVSFrSZi-cyw=="
    org = "hw_lab"
    buck_name = "test_results"
    host = "localhost"
    port = 8086
    luco_eui = "0013"

    measurement_cycle = 1  # time interval between measurements

    # realy config
    relay_address = "COM133"
    relay_config = r"C:\PythonCode\hat\scripts\robot_lib\relay_config.json"

    # start of application
    print("Connecting to power meter")
    pm = AcPowerMeterInstrument(pm_address)

    pm.clear_measurements()
    pm.add_meas_active_power()

    print("Connecting to relay")
    rel = RelayInterface(relay_address, relay_config)

    print("Configuring relay")
    rel.set_position_by_label("dali")
    rel.set_position_by_label("dali_led")
    rel.set_position_by_label("dali_power")

    print("Connecting to database")
    db = InfluxInterface(host, port, token, org, buck_name)



    # local variables
    cycle = 1

    # start of script
    while True:
        try:
            start = time.time()
            power = -1 * pm.get_active_power()
            db.insert_point("power", ["controller", luco_eui], ["active power", power])
            elapsed = (time.time() - start)
            print(f"Cycle: {cycle}, Power: {power:.2f}W, elaspsed: {elapsed * 1000.0:.3f}ms")
            time.sleep(measurement_cycle - elapsed)

        except Exception as e:
            print(f"Error: {e}")
            print("Continuing")
            time.sleep(1)
        except KeyboardInterrupt:
            print("Terminating script")
            break

    start = time.time()
    for i in range(10):
        db.insert_point("power", ["controller", "Luco 1"], ["active power", randint(18, 19) + random()])

    elapsed = (time.time() - start) / 10.0

    print(f"time per point: {elapsed}")

    print("end!")
