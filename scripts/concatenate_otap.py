"""
# Filename:     concatenate_otap.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   06/05/2021
# Last Update:  06/05/2021
#
# Version:      1.0
# Filetype:     
# Description:
# STATUS:       Stable
# Limitation:   None
"""
import pandas as pd
import numpy as np
import os

dirname = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet IOT\Project Folder\05_Support\20210506_FW_update"

filename = "firmware_update.xlsx"

data = pd.read_excel(os.path.join(dirname, filename), dtype=(str, str))
out = []
for x in data["Value"].to_list():
    if isinstance(x, float):
        out.append("")
    else:
        out.append(x)

string = out[0] +'"' + '","'.join(out[1:]) + '"'
with open("ATcommand2.txt", "w") as f:
    f.write(string)

print(f"AT command: {string}")
