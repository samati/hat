import hat.utils.DataStorage
from hat.comms.LTE_interface import G4
from hat.drivers import DC_source
import time
from hat.utils import utils
import os

if __name__ == "__main__":
    # variables
    address = "USB0::0x05E6::0x2230::802895020757510003::INSTR"
    channel = "CH2"
    COM_port = "COM72"
    serialnumber = "L153"
    n_tests = 10
    dirname = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet FOUR\Project Folder\08_Support\20210406_BG95_evalboard"
    timeout = 5 * 60  # time to wait for registration
    sleep_time = 3*60  # time for G4 to turn off
    delay_start = 15  # time for mcu to startup

    print("Connect to interfaces")
    dc = DC_source.DCSource(address, reset=False, query_delay=1)
    g4 = G4(COM_port)

    data = hat.utils.DataStorage.DataStorage(os.path.join(dirname, f"registration_{serialnumber}.xlsx"))

    # start of test
    print("Start test")

    for i in range(n_tests):
        print(f"Start cycle {i + 1}")
        data["Test #"] = i
        print("Turn on interfaces")
        dc.set_output_state(channel, True)
        start_time = time.time()

        print(f"sleep for MCU to startup {delay_start}")
        time.sleep(delay_start)

        g4._mode = "bash"
        g4.read_all()

        fw_version = g4.execute_command("bash", "fw-ver")

        g4.go_to_mode("lte")

        com_ok = g4.execute_command("lte", "ate")

        imsi_text = g4.get_imsi()
        data["imsi"] = imsi_text

        gsm_status = 0
        gsm_time = 0
        gprs_status = 0
        gprs_time = 0

        while time.time() - start_time < timeout and gsm_status != 5 and gprs_status != 5:
            unsolicited, gsm_status = g4.get_register_gsm()
            unsolicited, gprs_status = g4.get_register_gprs()

            if gsm_time == 0 and gsm_status == 5:
                gsm_time = time.time() - start_time

            if gprs_time == 0 and gprs_status == 5:
                gprs_time = time.time() - start_time

            time.sleep(1)


        operator = g4.lte_network_operator()
        network_info = g4.network_info()

        data["gsm time [s]"] = int(gsm_time)
        data["last gsm status"] = gsm_status

        data["gprs time [s]"] = int(gprs_time)
        data["last gprs status"] = gprs_status

        data["operator"] = operator
        data["Network info"] = network_info

        data.start_next_test()

        print("Turn off")
        dc.set_output_state(channel, False)

        print(f"Sleep {sleep_time} seconds")
        time.sleep(sleep_time)

    data.save()

    # end
    print("end!")
