import time
from hat.drivers import iluminance_driver
from hat.utils import influxdb_utils


def test_luminance(lux_meas, db, interval, tags):
    while True:
        lux = lux_meas.get_iluminance()
        print(f"Insert point: {lux}")
        db.insert_point("luminance", tags,
                        {"luminance": int(lux)}
                        )
        print(f"Sleeping for {interval}seconds")
        time.sleep(interval)


if __name__ == "__main__":
    print("Start lux measurement")

    # variables
    com_port = "COM71"
    db_server = "192.168.1.89"
    db_port = 8086
    db_user = "grafana"
    db_pass = "grafana_password"
    db_name = "test_luminance"

    # interval
    time_interval = 30  # seconds
    tags = {
        "device": "meter",
        "location": "home"
    }

    lux_meas = iluminance_driver.IlluminanceDriver(com_port)

    db = influxdb_utils.InfluxInterface(db_server, db_port, db_user, db_pass, db_name)

    test_luminance(lux_meas, db, time_interval, tags)
