from hat.comms import LTE_interface

com_port = "COM23"
baudrate = 115200

# connect to a device
print("Connecting to G4...")
dev = LTE_interface.G4(com_port, baudrate)

# print(dev.execute_command("lte-shell"))
print(dev.unlock_sim(8082))

print(dev.lte_network_operator())

print(dev.lte_assign_APN())
dev.lte_ping("www.google.com")
dev.lte_ping("www.google-wrong.com")

dev.lte_configure_http()

dev.lte_set_http_url("https://www.bing.com")

filename = "RAM:website.txt"
data = dev.lte_get_http_read_file(filename)
data = dev.read_file(filename)
print("End!")