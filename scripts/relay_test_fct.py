from hat.drivers import MultimeterInstrument
from hat.comms import P7CM_FCT
from hat.utils import DataStorage
import os, time

if __name__ == "__main__":
    # variables
    dali_ip = "192.168.8.180"
    com_port = "COM174"
    use_dali = False

    multimeter_address = "USB0::0x05E6::0x6510::04472337::INSTR"
    mult_config_file = r"C:\PythonCode\hat\scripts\robot_lib\mult_config.json"

    output_dir = r"C:\PythonCode\hat\scripts\data\relay_test"

    # test variables
    measure_line_in = False
    measure_line_out = False
    wait_zero_cross = True
    delay_time = 10
    time_after_relay_switch = 4

    min_line_in_voltage = 220  # minimum line in voltage
    relay_on_max_voltage_difference = 5  # maximum voltage difference between line and line out during relay on
    relay_off_min_voltage_difference = 200  # maximum voltage difference between line and line out during relay on

    print("Staring test")

    # open multimeter interface
    mult = MultimeterInstrument.MultimeterInstrument(multimeter_address, mult_config_file, reset=False)
    mult.open()
    # mult.set_function_range_by_name("LineInToLineOut", "VOLT:AC", 750)

    # open p7cm interface
    if use_dali:
        p7cm = P7CM_FCT.P7CM_FCT(dali_ip, interface_type=P7CM_FCT.InterfaceType.DALI)
    else:
        p7cm = P7CM_FCT.P7CM_FCT(com_port, interface_type=P7CM_FCT.InterfaceType.USB)
    p7cm.open()

    # create data file
    data = DataStorage.DataStorage()
    data.open(os.path.join(output_dir, "relay_test.xlsx"))

    # start loop
    cycle = 1
    while True:
        print(f"Starting cycle {cycle}")
        data["Cycle"] = cycle

        # switch On measurement

        print("Set switch on")
        data["ON: Wait zero cross"] = wait_zero_cross
        p7cm.switch_relay(True, wait_zero_cross)

        time.sleep(time_after_relay_switch)

        # measurement
        if measure_line_in:
            lineInToNeutral = mult.get_voltage_ac_by_name("LineInToNeutral")
            data["ON: Line In to Neutral [V]"] = lineInToNeutral
        if measure_line_out:
            lineOutToNeutral = mult.get_voltage_ac_by_name("LineOutToNeutral")
            data["ON: Line Out to Neutral [V]"] = lineOutToNeutral

        lineInToLineOut = mult.get_voltage_ac()

        # write file

        data["ON: Line Out to Line In [V]"] = lineInToLineOut

        # calculation
        if abs(lineInToLineOut) > relay_on_max_voltage_difference or \
                (measure_line_in and lineInToNeutral < min_line_in_voltage):
            print(f"Error {lineInToLineOut} above max difference")
            if measure_line_in:
                print(f"Line In{lineInToNeutral} below min voltage")
            data["ON: Result"] = "Fail"
            test_failed = True
            break
        else:
            test_failed = False
            data["ON: Result"] = "Pass"

        # switch off
        print("Set switch off")
        data["OFF: Wait zero cross"] = wait_zero_cross
        state = p7cm.switch_relay(False, wait_zero_cross)

        time.sleep(time_after_relay_switch)

        # measurement
        if measure_line_in:
            lineInToNeutral = mult.get_voltage_ac_by_name("LineInToNeutral")
            data["OFF: Line In to Neutral [V]"] = lineInToNeutral
        if measure_line_out:
            lineOutToNeutral = mult.get_voltage_ac_by_name("LineOutToNeutral")
            data["OFF: Line Out to Neutral [V]"] = lineOutToNeutral

        lineInToLineOut = mult.get_voltage_ac()

        # write file
        data["OFF: Line Out to Line In [V]"] = lineInToLineOut

        # calculation
        if abs(lineInToLineOut) < relay_off_min_voltage_difference or \
                (measure_line_in and lineInToNeutral < min_line_in_voltage):
            print(f"Error {lineInToLineOut} below min difference")

            if measure_line_in:
                print(f"Line In{lineInToNeutral} below min voltage")

            data["OFF: Result"] = "Fail"
            test_failed = True
            break
        else:
            test_failed = False
            data["OFF: Result"] = "Pass"

        cycle += 1
        data.start_next_test()
        print(f"Sleep {delay_time} seconds between cycles")
        time.sleep(delay_time)

    data.close()
    print("end!")
