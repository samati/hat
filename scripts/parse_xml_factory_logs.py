import numpy as np
import time
import re
import xml.etree.ElementTree as ET
import os
from hat.utils import DataStorage
import pandas as pd
import datetime


def parse_factory_xml(dirname, filename, data):
    tree = ET.parse(os.path.join(dirname, filename))
    root = tree.getroot()

    # get batch
    factory = root.findall("./FACTORY")

    dut = root.findall("./PANEL/DUT")[0]
    data["filename"] = filename
    data["eui"] = dut.attrib["ID"]

    group = dut.findall("GROUP")[0]

    groups = group.findall("GROUP")
    for element in groups:
        if "Set Calibration" in element.attrib["NAME"]:
            print(f"Name: {element.attrib['NAME']}")
            cal = element
            break

    groups = cal.findall("GROUP")
    for element in groups:
        if "T25_Relay ON Delay From XML Log" in element.attrib["NAME"]:
            print(f"Name: {element.attrib['NAME']}")
            data["relay_on"] = int(element.find("TEST").attrib["VALUE"])
        elif "T25_Relay OFF Delay From XML Log" in element.attrib["NAME"]:
            print(f"Name: {element.attrib['NAME']}")
            data["relay_off"] = int(element.find("TEST").attrib["VALUE"])
        elif "T25_Relay OFF Delay From XML Log" in element.attrib["NAME"]:
            print(f"Name: {element.attrib['NAME']}")
            data["relay_off"] = int(element.find("TEST").attrib["VALUE"])
        elif "T25_Current Factor From XML Log" in element.attrib["NAME"]:
            print(f"Name: {element.attrib['NAME']}")
            data["current"] = float(element.find("TEST").attrib["VALUE"])
        elif "T25_Voltage Factor From XML Log" in element.attrib["NAME"]:
            print(f"Name: {element.attrib['NAME']}")
            data["voltage"] = float(element.find("TEST").attrib["VALUE"])


def parse_all_files(dirnames):
    # xml_filename = os.path.join(dirname, filename)
    data = DataStorage("cal.xls")

    for dirname in dirnames:
        for root, dirs, files in os.walk(dirname):
            for filename in files:
                name, ext = os.path.splitext(filename)
                if ext == ".xml":
                    try:
                        parse_factory_xml(dirname, filename, data)
                    except Exception as e:
                        print(f"Error on file {filename}, error {e}")
                    data.start_next_test()
    data.close()


def color_row(series):
    # print(series)
    if series["Main task"]:
        color = "Lightgreen"
    else:
        color = "Beige"
    return [f"background-color:{color}"] * len(series)


def parse_single_xml(dirname, test_file, xlsx_writer, sheet_name):
    tree = ET.parse(os.path.join(dirname, test_file))
    root = tree.getroot()

    # get batch
    factory = root.findall("./FACTORY")

    dut = root.findall("./PANEL/DUT")[0]
    initial_group = dut.findall("GROUP")
    groups = initial_group[0].findall("GROUP")

    data = pd.DataFrame()
    idx = 0

    for group in groups:
        other_groups = group.findall("GROUP")
        main_name = group.attrib["NAME"]
        data.loc[idx, "MAIN"] = main_name
        data.loc[idx, "Main task"] = True
        for name, value in group.attrib.items():
            data.loc[idx, name] = value

        if other_groups:
            # print("Other groups")
            data.loc[idx, "Has subtasks"] = True

            idx += 1
            for other_group in other_groups:
                data.loc[idx, "MAIN"] = main_name
                data.loc[idx, "Main task"] = False
                data.loc[idx, "Has subtasks"] = False
                for name, value in other_group.attrib.items():
                    data.loc[idx, name] = value
                idx += 1
        else:
            data.loc[idx, "Has subtasks"] = False
            idx += 1

    data = data.astype({"TOTALTIME": float})

    styler = data.style.apply(color_row, axis=1)

    # print(f"Saving to filename {filename}")
    styler.to_excel(xlsx_writer, sheet_name=sheet_name)

    return data


def parse_multiple_xml_files(dirname, output_dir, output_file):
    filename = time.strftime(output_file)

    xlsx_writer = pd.ExcelWriter(os.path.join(output_dir, filename), engine="openpyxl")

    dfs = {}

    for root, sub_dirs, _ in os.walk(dirname):
        for sub_dir in sub_dirs:
            complete_subdir = os.path.join(dirname, sub_dir)
            for _, _, files in os.walk(complete_subdir):
                for i, file in enumerate(files):
                    print(f"Parsing file {file} in dir")
                    sheet_name = f"{sub_dir}_{i}"
                    dfs[sheet_name] = parse_single_xml(complete_subdir, file, xlsx_writer, sheet_name)

            # break  # to only parse on file

    # processing of dataframes
    df_resume = pd.DataFrame()
    df_times = pd.DataFrame()
    times_idx = 0

    idx = 0
    for name, df in dfs.items():
        main_tasks = df[df["Main task"]]
        sub_tasks = df[np.logical_not(df["Main task"])]

        task_without_sub_tasks = df[np.logical_not(df["Has subtasks"])]

        # resume
        total_test_time = np.sum(main_tasks["TOTALTIME"])
        df_resume.loc[idx, "Test name"] = name
        df_resume.loc[idx, "Total test time [s]"] = total_test_time
        df_resume.loc[idx, "Number of tasks"] = df.shape[0]

        # max task time
        max_task = main_tasks.iloc[main_tasks["TOTALTIME"].argmax()]

        df_resume.loc[idx, "Max task name"] = max_task["NAME"]
        df_resume.loc[idx, "Max task time [s]"] = max_task["TOTALTIME"]

        # max sub task
        max_sub_task = task_without_sub_tasks.iloc[task_without_sub_tasks["TOTALTIME"].argmax()]

        df_resume.loc[idx, "Max subtask name"] = max_sub_task["NAME"]
        df_resume.loc[idx, "Max subtask time [s]"] = max_sub_task["TOTALTIME"]

        # wait time
        wait_task = df[df["TYPE"] == "NI_Wait"]

        total_wait_time = np.sum(wait_task["TOTALTIME"])
        df_resume.loc[idx, "Total wait time"] = total_wait_time
        df_resume.loc[idx, "# of wait tasks"] = wait_task.shape[0]

        # try to check time by category
        tasks = {
            "Setup": main_tasks[main_tasks["STEPGROUP"] == "Setup"],
            "Main": main_tasks[main_tasks["STEPGROUP"] == "Main"],
            "Cleanup": main_tasks[main_tasks["STEPGROUP"] == "Cleanup"]
        }

        for task_name, task in tasks.items():
            df_resume.loc[idx, f"# of {task_name} tasks"] = task.shape[0]
            df_resume.loc[idx, f"{task_name} tasks time [s]"] = np.sum(task["TOTALTIME"])

        idx += 1
        # parse timmin gs

        tasks_log_names = {
            "GoldCap_0": [
                {"Task": "Voltage/Current test",
                 "start": "[RC] Connect 24V",
                 "stop": "T2-4 Voltage Measurement"
                 },
                {"Task": "Charge",
                 "start": "T5-6 SuperCap Charge Test",
                 "stop": "T8-9 SuperCap Storage Test"
                 },
                {"Task": "Discharge",
                 "start": "T10 SuperCap Discharger Test",
                 "stop": "[RD] Diconnect 24V"
                 }
            ],
            "LUMA_GoldCap_0": [
                {"Task": "Voltage/Current test",
                 "start": "T1-4 Power ON& Verify Voltage",
                 "stop": "T1-4 Power ON& Verify Voltage"
                 },
                {"Task": "Charge",
                 "start": "T5-6 SuperCap Charge Test",
                 "stop": "T8-9 SuperCap Storage Test"
                 },
                {"Task": "Discharge",
                 "start": "T10 SuperCap Discharger Test",
                 "stop": "T10 SuperCap Discharger Test For All"
                 }
            ],
            "LUMA_MESH_0": [
                {"Task": "Program MCU FW",
                 "start": "T6 MCU Program FW",
                 "stop": "T11 BootUP & Voltage Verify"
                 },
                {"Task": "Program EFR32 FW",
                 "start": "T12/30 EFR32 FW Program & SN FW",
                 "stop": "T12/30 EFR32 FW Program & SN FW"
                 },
                {"Task": "Voltage/Current test",
                 "start": "T1-5 Power ON &Verify",
                 "stop": "T1-5 Power ON &Verify"
                 },
                {"Task": "Light sensor Check(LED Light)",
                 "start": "T17-18 Light Sensor Check",
                 "stop": "T17-18 Light Sensor Check"
                 },
                {"Task": "DALI interface",
                 "start": "T13 DALI Test",
                 "stop": "T13 DALI Test"
                 },
                {"Task": "LED",
                 "start": "T16 LED Check",
                 "stop": "T16 LED Check"
                 },
                {"Task": "Watchdog/reboot",
                 "start": "T23 Watchdog Check",
                 "stop": "T23 Watchdog Check"
                 },
                {"Task": "SPI flash",
                 "start": "T25 SPI Flash Test",
                 "stop": "T25 SPI Flash Test"
                 },
                {"Task": "GPS search satellites",
                 "start": "T19 GPS Function Check",
                 "stop": "T19 GPS Function Check"
                 },
                {"Task": "Wi-SUN PER test",
                 "start": "T19/31 WiSun TXRX Function/Frequency",
                 "stop": "T19/31 WiSun TXRX Function/Frequency"
                 },
                {"Task": "Charge/discharge",
                 "start": "T27 SuperCap Incharge Test",
                 "stop": "T28 SuperCap Discharging For All"
                 }
            ],
            "LUMA_NBIoT_0": [
                {"Task": "Program MCU Test FW",
                 "start": "T6 MCU Program FW",
                 "stop": "T11 BootUP & Voltage Verify"
                 },
                {"Task": "Program EFR Test FW",
                 "start": "T12/30 EFR32 FW Program & SN FW",
                 "stop": "T12/30 EFR32 FW Program & SN FW"
                 },
                {"Task": "Voltage/Current test",
                 "start": "T22 Watchdog Check",
                 "stop": "T22 Watchdog Check"
                 },
                {"Task": "Light sensor Check(LED Light)",
                 "start": "T17-18 Light Sensor Check",
                 "stop": "T17-18 Light Sensor Check"
                 },
                {"Task": "DALI interface",
                 "start": "T13 DALI Test",
                 "stop": "T13 DALI Test"
                 },
                {"Task": "LED",
                 "start": "T16 LED Check",
                 "stop": "T16 LED Check"
                 },
                {"Task": "Watchdog/reboot",
                 "start": "T22 Watchdog Check",
                 "stop": "T22 Watchdog Check"
                 },
                {"Task": "SPI flash",
                 "start": "T25 SPI Flash Test",
                 "stop": "T25 SPI Flash Test"
                 },
                {"Task": "GPS search satellites",
                 "start": "T120 GPS Function Check",
                 "stop": "T120 GPS Function Check"
                 },
                {"Task": "Wi-SUN PER test",
                 "start": "T19/3x WiSun TXRX Function/Frequency",
                 "stop": "T19/3x WiSun TXRX Function/Frequency"
                 },
                {"Task": "Cellular search network",
                 "start": "T21&22&31 eSIM&LTE&GPRS Function Check",
                 "stop": "T21&22&31 eSIM&LTE&GPRS Function Check"
                 }

            ],
            "NEMA LTE_0": [
                {"Task": "Program MCU Test FW",
                 "start": "T1 MCU Program FW Before Testing",
                 "stop": "T11 UUT BootUP"
                 },
                {"Task": "Program EFR Test FW",
                 "start": "T2 Zigbee Program FW Before Testing",
                 "stop": "T3_Measure 3V8 Current After Programming"
                 },
                {"Task": "Voltage/Current test",
                 "start": "[RC] Power ON GS",
                 "stop": "T3_Measure 3V8 Default Current"
                 },
                {"Task": "Light sensor Check(LED Light)",
                 "start": "T16-17 Light Sensor Check",
                 "stop": "T16-17 Light Sensor Check"
                 },
                {"Task": "DALI interface",
                 "start": "T12 DALI Test",
                 "stop": "T12 DALI Test"
                 },
                {"Task": "1~10V",
                 "start": "T13 1-10V Test",
                 "stop": "T13 1-10V Test"
                 },
                # {"Task": "Digi sensor",
                #  "start": "",
                #  "stop": ""
                #  },
                {"Task": "LED",
                 "start": "T15 LED Check",
                 "stop": "T15 LED Check"
                 },
                {"Task": "Watchdog/reboot",
                 "start": "T22 Watchdog Check",
                 "stop": "T22 Watchdog Check"
                 },
                # {"Task": "SPI flash",
                #  "start": "",
                #  "stop": ""
                #  },
                {"Task": "GPS search satellites",
                 "start": "T19 GPS Function Check",
                 "stop": "T19 GPS Function Check"
                 },
                {"Task": "Wi-SUN PER test",
                 "start": "T18 Zigbee Net Test",
                 "stop": "T18 Zigbee Net Test"
                 },
                {"Task": "Cellular search network",
                 "start": "T20-21 eSIM & LTE Function Check",
                 "stop": "T20-21 eSIM & LTE Function Check"
                 }
            ],
            "NEMA Mesh_0": [
                {"Task": "Program MCU Test FW",
                 "start": "T1 MCU Program FW Before Testing",
                 "stop": "T11 UUT BootUP"
                 },
                {"Task": "Program EFR Test FW",
                 "start": "T2 Zigbee Program FW Before Testing",
                 "stop": "T3_Measure 24V Current After Programming"
                 },
                {"Task": "Voltage/Current test",
                 "start": "[RC] Power ON GS",
                 "stop": "T3_Measure 24V Default Current"
                 },
                {"Task": "Light sensor Check(LED Light)",
                 "start": "T16-17 Light Sensor Check",
                 "stop": "T16-17 Light Sensor Check"
                 },
                {"Task": "DALI interface",
                 "start": "T12 DALI Test",
                 "stop": "T12 DALI Test"
                 },
                {"Task": "LED",
                 "start": "T15 LED Check",
                 "stop": "T15 LED Check"
                 },
                {"Task": "Watchdog/reboot",
                 "start": "T22 Watchdog Check",
                 "stop": "T22 Watchdog Check"
                 },
                # {"Task": "SPI flash",
                #  "start": "",
                #  "stop": ""
                #  },
                {"Task": "GPS search satellites",
                 "start": "T19 GPS Function Check",
                 "stop": "T19 GPS Function Check"
                 },
                {"Task": "Wi-SUN PER test",
                 "start": "T18 Zigbee Net Test",
                 "stop": "T18 Zigbee Net Test"
                 },
                {"Task": "Charge/discharge",
                 "start": "T20 SuperCap Charging Test",
                 "stop": "T21 SuperCap Discharging Test"
                 }

            ],
            "NEMA_PSU_0": [
                {"Task": "Voltage/Current test",
                 "start": "T1-7 Power ON & Voltage Measurement",
                 "stop": "T8 GS BootUP"
                 },
                {"Task": "RFID",
                 "start": "T12 RFID Check",
                 "stop": "T12 RFID Check"
                 },
                # {"Task": "Relay status",
                #  "start": "",
                #  "stop": ""
                #  },
                {"Task": "Metering calibration",
                 "start": "T15-16 Metering Calibration & Validation",
                 "stop": "T15-16 Metering Calibration & Validation"
                 },
                {"Task": "Relay calibration",
                 "start": "T13-14 Relay ON OFF Calibration",
                 "stop": "T13-14 Relay ON OFF Calibration"
                 }
            ],
            "Unit LUMA Mesh_0": [
                {"Task": "Boot up & Current test",
                 "start": "[RC] Power ON GS",
                 "stop": "T2 UUT BootUP"
                 },
                {"Task": "DALI ",
                 "start": "T3 DALI Test",
                 "stop": "T3 DALI Test"
                 },
                {"Task": "Light sensor",
                 "start": "T8 Light Sensor Check",
                 "stop": "T8 Light Sensor Check"
                 },
                # {"Task": "GPS position",
                #  "start": "",
                #  "stop": ""
                #  },
                {"Task": "Assemble related function ",
                 "start": "Scan PSU SN",
                 "stop": "T0_Unit LUMA Mesh SN Match 14EFCF18FF"
                 },
                # {"Task": "GPS/EFR32/LTE chip info collecting",
                #  "start": "",
                #  "stop": ""
                #  },
                # {"Task": "Wi-SUN Test",
                #  "start": "",
                #  "stop": ""
                #  },
                # {"Task": "Cellular Power Test",
                #  "start": "",
                #  "stop": ""
                #  },
                {"Task": "SPI flash",
                 "start": "T2x SPI Flash Test",
                 "stop": "T2x SPI Flash Test"
                 },
                {"Task": "Watchdog",
                 "start": "T2x Watchdog Check",
                 "stop": "T2x Watchdog Check"
                 },
                {"Task": "MCU program Schreder FW",
                 "start": "T22 MCU Program Schreder  FW",
                 "stop": "T24 UUT BootUP Schreder FW"
                 },
                {"Task": "Set EUI",
                 "start": "T26 Set EUI & Cert",
                 "stop": "T27 Lock Factory Configuration"
                 }
            ],
            "Unit NEMA LTE_0": [
                {"Task": "Boot up & Current test",
                 "start": "[ACPWR] Init COM",
                 "stop": "T2_Measure 220V Default Current"
                 },
                {"Task": "DALI",
                 "start": "T3 DALI Test",
                 "stop": "T3 DALI Test"
                 },
                {"Task": "Digi sensor",
                 "start": "T14 Digi Function Check",
                 "stop": "T14 Digi Function Check"
                 },
                {"Task": "Light sensor",
                 "start": "T8 Light Sensor Check",
                 "stop": "T8 Light Sensor Check"
                 },
                # {"Task": "GPS position",
                #  "start": "",
                #  "stop": ""
                #  },
                {"Task": "RFID tag read",
                 "start": "T6 RFID Check",
                 "stop": "T6 RFID Check"
                 },
                {"Task": "Assemble related function check(meter/relay/…)",
                 "start": "Scan PSU SN",
                 "stop": "T0_Unit NEMA LTE SN Match 14EFCF20FF"
                 },
                # {"Task": "GPS/EFR32/LTE chip info collecting",
                #  "start": "",
                #  "stop": ""
                #  },
                # {"Task": "Wi-SUN Test",
                #  "start": "",
                #  "stop": ""
                #  },
                {"Task": "Cellular Power Test",
                 "start": "T21 M-4G_4G_STATUS check",
                 "stop": "Txx check LTE again"
                 },
                {"Task": "MCU program Schreder FW",
                 "start": "T1 MCU Program FW Before Testing",
                 "stop": "T2 UUT BootUP"
                 },
                {"Task": "Watchdog",
                 "start": "T2x Watchdog Check",
                 "stop": "T2x Watchdog Check"
                 },
                {"Task": "SPI flash",
                 "start": "T2x SPI Flash Test",
                 "stop": "T2x SPI Flash Test"
                 },
                {"Task": "Set EUI & calibration",
                 "start": "T25 Set Calibration Parameter",
                 "stop": "T27 Lock Factory Configuration"
                 }

            ],
            "Unit NEMA Mesh_0": [
                {"Task": "Boot up & Current test",
                 "start": "[ACPWR] Init COM",
                 "stop": "T2_Measure 220V Default Current"
                 },
                {"Task": "DALI",
                 "start": "T3 DALI Test",
                 "stop": "T3 DALI Test"
                 },
                {"Task": "Digi sensor",
                 "start": "T14 Digi Function Check",
                 "stop": "T14 Digi Function Check"
                 },
                {"Task": "Light sensor",
                 "start": "T8 Light Sensor Check",
                 "stop": "T8 Light Sensor Check"
                 },
                # {"Task": "GPS position",
                #  "start": "",
                #  "stop": ""
                #  },
                {"Task": "RFID tag read",
                 "start": "T6 RFID Check",
                 "stop": "T6 RFID Check"
                 },
                {"Task": "Assemble related function check(meter/relay/…)",
                 "start": "Scan PSU SN",
                 "stop": "T0_Unit NEMA Mesh SN Match 14EFCF28FF"
                 },
                # {"Task": "GPS/EFR32/LTE chip info collecting",
                #  "start": "",
                #  "stop": ""
                #  },
                # {"Task": "Wi-SUN Test",
                #  "start": "",
                #  "stop": ""
                #  },
                # {"Task": "Cellular Power Test",
                #  "start": "",
                #  "stop": ""
                #  },
                {"Task": "Set EUI & calibration",
                 "start": "T25 Set Calibration Parameter",
                 "stop": "T27 Lock Factory Configuration"
                 },
                {"Task": "Watchdog",
                 "start": "T2x Watchdog Check",
                 "stop": "T2x Watchdog Check"
                 },
                {"Task": "MCU program Schreder FW",
                 "start": "T22 MCU Program Schreder  FW",
                 "stop": "T24 UUT BootUP Schreder FW"
                 }
            ]
        }

        task_log_names = tasks_log_names[name]

        total_task_time = 0
        for task in task_log_names:
            task_readable_name = task["Task"]
            start_name = task["start"]
            stop_name = task["stop"]
            start_idx = main_tasks[main_tasks["NAME"] == start_name].index[0]
            stop_idx = main_tasks[main_tasks["NAME"] == stop_name].index[0]

            task_series = main_tasks.loc[start_idx:stop_idx]
            print(task)

            df_times.loc[times_idx, "Board"] = name
            df_times.loc[times_idx, "Test name"] = task_readable_name
            task_time = np.sum(task_series["TOTALTIME"])
            df_times.loc[times_idx, "Test total Time [s]"] = task_time
            total_task_time += task_time

            times_idx += 1

        # calculate other times
        df_times.loc[times_idx, "Board"] = name
        df_times.loc[times_idx, "Test name"] = "Others"
        df_times.loc[times_idx, "Test total Time [s]"] = total_test_time - total_task_time
        times_idx += 1

        # add total test time
        df_times.loc[times_idx, "Board"] = name
        df_times.loc[times_idx, "Test name"] = "Total"
        df_times.loc[times_idx, "Test total Time [s]"] = total_test_time
        times_idx += 1

    df_resume.to_excel(xlsx_writer, sheet_name="Resume")
    df_times.to_excel(xlsx_writer, sheet_name="Total")
    xlsx_writer.close()


def parse_G3_log(logfile, output_dir, output_file):
    filename = time.strftime(output_file)
    data = pd.DataFrame()

    idx = 0
    start_test = False

    begin_test = True

    with open(logfile) as f:
        print(f"Log file {logfile} opened!")
        for i, line in enumerate(f):
            print(f"line {i}: {line}")
            if "START TEST" in line:
                start_test = True
                test = "START TEST"
                print("Test started!")
                continue
            if not start_test:
                continue

            test_match = re.match(r"^[a-zA-Z]", line)

            # start of a test
            if test_match:
                print(f"Test match: {test_match}")
                test = line.strip()
                begin_test = True
                start_time = datetime.datetime(1970, 1, 1)
                stop_time = datetime.datetime(1970, 1, 1)

            time_match = re.match(r"^(\d+:\d+:\d+\.\d+)", line)
            if time_match:
                print(f"Time match: {time_match}")
                time_value = datetime.datetime.strptime(time_match.group(1), "%H:%M:%S.%f")
                if begin_test:
                    start_time = time_value
                    if test == "START TEST":
                        start_test_time = start_time
                    begin_test = False
                else:
                    stop_time = time_value
                    last_stop_time = stop_time

            emptyline_match = re.match("^$", line)
            if emptyline_match:
                print("End of test")
                data.loc[idx, "Test name"] = test
                data.loc[idx, "Total time [s]"] = (stop_time - start_time).total_seconds()
                idx += 1

        # resume
        data.loc[idx, "Test name"] = "Total"
        data.loc[idx, "Total time [s]"] = (last_stop_time - start_test_time).total_seconds()

    data.to_excel(os.path.join(output_dir, filename))
    print("end!")


if __name__ == "__main__":
    dirnames = [
        r"C:\Users\fcoelho\OneDrive - Schréder SA\Documents\Schreder\Projects\G4\06_Factory\03_Test_logs\DVT\Owlet_G4_Report\Unit NEMA LTE",
        r"C:\Users\fcoelho\OneDrive - Schréder SA\Documents\Schreder\Projects\G4\06_Factory\03_Test_logs\DVT\Owlet_G4_Report\Unit NEMA Mesh"]

    # parse all files
    # parse_all_files(dirnames)
    dirname = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet FOUR\Project Folder\06_Factory\03_Test_logs\DVT\Study_files"
    # test_file = r"P_0_Fts_OWLET_AZ0311001002_20200820091829686_2020820161829707.xml"

    output_dir = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet FOUR\Project Folder\06_Factory\03_Test_logs\DVT"
    output_file = r"G4_report_%Y%m%d_%H%M.xlsx"

    G3_logfile = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet FOUR\Project Folder\06_Factory\03_Test_logs\Owlet3_FCT_CM_210315_142509_001805003571_0013A2004173159A_P_LowLevel.txt"

    G3_output_file = "g3_analysis_%Y%m%d_%H%M.xlsx"
    parse_G3_log(G3_logfile, output_dir, G3_output_file)

    print("End!")
