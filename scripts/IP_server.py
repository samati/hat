import socket

# SOCK_STREAM is TCP connection
# SOCK_DGRAM is UDP connection
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

host_ip = "0.0.0.0"
host_port = 82
# bind to open port on VM
s.bind((host_ip, host_port))

try:
    while True:
        print("Waiting connections!")

        s.listen(1)
        conn, addr = s.accept()
        print("Connection received!")

        while True:
            # receive data from socket
            data = conn.recv(1024)
            if not data:
                break
            print("Data received: %s" % data)
            # send data back
            conn.sendall(data)

except KeyboardInterrupt:
    print("Keyboard interrupt, end!")
    conn.close()

finally:
    print("finally end!")
    conn.close()
