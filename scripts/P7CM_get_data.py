# add digi xbee
import time
from digi.xbee.devices import ZigBeeDevice, RemoteZigBeeDevice, ZigBeeNetwork
from digi.xbee.exception import TimeoutException
from digi.xbee.models.address import XBee64BitAddress
from digi.xbee.models.options import DiscoveryOptions
import pandas as pd

at_file = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet IOT\Project Folder\07_Returned_Devices\AT_Commands.xlsx"


class ZigbeeInterface:
    def __init__(self, com_port):
        self._local_zb = ZigBeeDevice(com_port, 115200)
        self._local_zb.open()
        if not self._local_zb.is_open():
            raise Exception("Could not open local zigbee interface")

        self._prompt = "[CLI]>"

    def set_remote(self, zigbee_address):
        print(f"Setting remote zigbee address {zigbee_address}")
        self._remote_zb = RemoteZigBeeDevice(self._local_zb,
                                             XBee64BitAddress.from_hex_string(remote_device_addr.replace(":", "")))

    def get_parameter(self, at):
        return self._remote_zb.get_parameter(at)

    def _read_explicity_from_remote(self, timeout):
        data = self._local_zb.read_expl_data_from(self._remote_zb, timeout)
        return data.data.decode("ascii")

    def cli(self, command, timeout=10):
        res = self._local_zb.send_expl_data(self._remote_zb, command, 0xB3, 0xB3, 0x0011, 0xC105)
        print(f"Res {res}")
        starttime = time.time()
        data = ""
        while time.time() - starttime < timeout:
            data += self._read_explicity_from_remote(timeout)
            if data.endswith(self._prompt):
                data = data[:-len(self._prompt)].strip()
                return data

        raise Exception(f"Command: {command}, Could not get all data: {data}")


if __name__ == "__main__":
    serial_port = "COM77"
    remote_device_addr = "00:13:A2:00:41:BC:BF:5D"
    xb = ZigbeeInterface(serial_port)
    xb.set_remote(remote_device_addr)

    data = xb.cli("version")
    print(f"data {data}")

    res_list = []

    data = pd.read_excel(at_file)
    for idx, row in data.iterrows():
        at = row["AT"]
        description = row["Description"]
        # print(f"reading command {at}, description: {description}")
        res = xb.get_parameter(at)
        res_hex = int.from_bytes(res, byteorder="big", signed=False)

        res_list.append({
            "AT": at,
            "description": description,
            "result": res.decode("ascii"),
            "result_hex": res_hex
        })
        print(f"Result: {at}: {res}, hex: {res_hex}")

    print(res_list)

    print("end!")
