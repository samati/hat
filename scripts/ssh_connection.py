import time
import re
from paramiko import SSHClient, AutoAddPolicy

DEFAULT_TIMEOUT = 10  # default time for timeout
SLEEP_TIME = 0.1  # sleep between reads


class SshClient():
    def __init__(self, hostname, port, user, password, prompt, timeout=DEFAULT_TIMEOUT):
        # store parameters
        self._hostname = hostname
        self._port = port
        self._prompt = prompt
        self._timeout = timeout

        # create client
        self._intf = SSHClient()
        self._intf.set_missing_host_key_policy(AutoAddPolicy())
        self._intf.connect(hostname, port, user, password)

        # shell
        self._shell = self._intf.invoke_shell()

        time.sleep(3)  # wait a bit for connection shell

        # receive connection data
        data = self.read_all()
        print(f"Data: {data}")

    def read_all(self):
        data = ""
        while self._shell.recv_ready():
            data += self._shell.recv(1024).decode("ascii")
        return data

    def write(self, command):
        res = self._shell.send(command)

    def exec_command(self, command, raise_exception=True):
        self.write(command + ("\n" if command[-1] != "\n" else ""))

        prompt_found = False
        data = ""
        start_time = time.time()
        while time.time() - start_time < self._timeout:
            data += self.read_all()
            # command below search and tried to remove prompt
            data, replace = re.subn(self._prompt, "", data)  # prompt found
            if replace:
                prompt_found = True
                break
            time.sleep(SLEEP_TIME)

        if not prompt_found:
            if raise_exception:
                raise Exception(f"No prompt found for command: {command} in response data {data}")
            else:
                print(f"No prompt found for command: {command} in response data {data}")

        # clear command and new lines
        data =data.replace(command, "").strip()
        return data

    def __del__(self):
        self._intf.close()


hostname = 'hyperiontest.ddns.net'

port = 60936

user = 'pi'
password = "hyperion_rasp#"

# prompt needs to be set for your server prompt
prompt = r"pi@raspi-multidali1:.+\$"
client = SshClient(hostname, port, user, password, prompt)

folder = client.exec_command("cd Documents")
print(f"Folder: {folder}")

date = client.exec_command("date")
print(f"date: {date}")

# client.load_system_host_keys()

# client.load_host_keys('~/.ssh/known_hosts')

# client.set_missing_host_key_policy(AutoAddPolicy())

# client.connect(hostname, port, user, password)

# time.sleep(3)

# print('Connected to the server')

# stdin, stdout, stderr = client.exec_command('ls')

# print('Sending command to start wakama')

# stdin, stdout, stderr = client.exec_command('date')

# wakama_response = stdout.read()

# shell = client.invoke_shell()

# print("Wakama response is: ", wakama_response)

# error = stderr.read()

# print('Error, if exist: ', error)

# time.sleep(5)

print('Sending command to stop wakama')

# stdin, stdout, stderr = client.exec_command('wakaama stop 5662')

# wakama_response = stdout.read()

# print("Wakama response is: ", wakama_response)

# error = stderr.read()

# print('Error, if exist: ', error)

# client.close()

'''

REAL EXECUTION OF THE CODE:

oem@Trineo-AJ:~/Desktop/G4_tests/Wakama$ python3 first_connection.py

Connected to the server

Sending command to start wakama

Wakama response is: b''

Error, if exist: b'bash: wakaama: command not found\n'

Sending command to stop wakama

Wakama response is: b''

Error, if exist: b'bash: wakaama: command not found\n'

Why wakama command is not found? It looks like it is not connected to the wakama server

and tries to execute wakama command on the command line.

'''
