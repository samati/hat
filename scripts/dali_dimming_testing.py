from hat.drivers.RelayInterface import RelayInterface
from hat.drivers.AcSourceInstrument import AcSourceInstrument
from hat.comms.P7CM_FCT import P7CM_FCT
from hat.comms.P7CM import P7CM
import time

if __name__ == "__main__":
    print("Test started")
    eui = "0013A20041B472A5"
    zigbee_port = "COM2"
    ac_port = "ASRL40::INSTR"
    relay_port = "COM133"
    dali_usb = "COM114"

    # control flow of the test
    execute_power_cycle = False

    # set relay
    relay = RelayInterface(relay_port, r"C:\PythonCode\hat\scripts\robot_lib\relay_config.json")
    relay.set_position_by_label("dali_led")
    relay.set_position_by_label("dali_power")

    # open zigbee interface
    p7cm_zigbee = P7CM(zigbee_port, interface_type="ZIGBEE", eui=eui)
    p7cm_zigbee.open()

    # open dali usb
    dali_uart = P7CM_FCT(dali_usb)

    # open interface to ac souce
    ac = AcSourceInstrument(ac_port)

    print("start test")
    ac.set_voltage_AC(230)
    ac.set_frequency(50)

    print("Set output state off")
    ac.set_output_state(False)
    time.sleep(5)

    print("Set output state on")
    ac.set_output_state(True)
    time.sleep(25)

    print("Swtich ON the Luco relay")
    # Switch on relay of the luco
    relay.set_position_by_label("uart")
    time.sleep(1)
    dali_uart.switch_relay(True, True)
    time.sleep(3)

    # Change the poition to dali driver
    relay.set_position_by_label("dali")
    print("Continuing...")

    # p7cm_zigbee.set_xbee_fct_mode(True)
    p7cm_zigbee.set_dali_fct_mode(False)

    time.sleep(3)
    res = p7cm_zigbee.perform_dali_reinit()
    print(f"Dali reinit: {res}")

    res = p7cm_zigbee.set_dali_dimming_fct(1, 254)

    time.sleep(3)

    res = p7cm_zigbee.set_dali_dimming_fct(1, 170)

    time.sleep(3)

    print("Test 1-10V")
    relay.set_position_by_label("1-10v_power")
    relay.set_position_by_label("1-10v_led")
    relay.set_position_by_label("1-10v")
    time.sleep(5)
    res = p7cm_zigbee.set_1_10v_dimming_fct(100)

    time.sleep(3)
    feedback_1_10v = p7cm_zigbee.get_1_10v_feedback_fct()
    print(f"Feedback: {feedback_1_10v}")

    res = p7cm_zigbee.set_1_10v_dimming_fct(20)

    time.sleep(3)
    feedback_1_10v = p7cm_zigbee.get_1_10v_feedback_fct()
    print(f"Feedback: {feedback_1_10v}")

    print(f"Number of drivers: {res}")

    print("end!")
