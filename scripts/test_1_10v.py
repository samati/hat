import pytest
import time
from pytest_html import extras
from hat.utils.DataStorage import DataStorage
import numpy as np
from hat.comms.g4_interface_cli import G4
from hat.drivers.TesterInterface import TesterInterface
import MultimeterInstP


def setup_module():
    print("First thing")


def teardown_module():
    print("Last thing")


def pytest_html_report_title(report):
    report.title = "My very own title!"


def test_1_10v_enable(g4_interface, tester_interface, extra):
    ## parameters
    instance = 1
    min_dim_level = 15
    min_voltage = 1.5
    max_voltage = 8

    import matplotlib.pyplot as plt
    plt.ioff()  # do not plot

    # store data
    data = DataStorage("1_10v", save_file=False)

    # disable logs
    g4_interface.set_global_loglevel(4)

    # g4_interface.lwm2m_set_dali_enable(False)
    # g4_interface.lwm2m_set_1_10v_enable(True)

    # g4_interface.lwm2m_set_1_10v_value(0)

    if not g4_interface.lwm2m_get_1_10v_enable():
        raise Exception("1_10V should be enabled")

    #configure lamp
    g4_interface.set_lamp_min_level(instance, min_dim_level)
    g4_interface.set_lamp_min_voltage_level(instance, min_voltage)
    g4_interface.set_lamp_max_voltage_level(instance, max_voltage)
    g4_interface.rules_lock(4000)

    time.sleep(5)

    for value in range(0, 100 + 1, 5):
        # g4_interface.lwm2m_set_1_10v_value(value)
        g4_interface.set_lamp_level_cmd(instance, value)
        time.sleep(4)
        read_value = g4_interface.lwm2m_get_1_10v_value()
        # assert read_value == value

        feedback = g4_interface.lwm2m_get_1_10v_measured()
        measured = tester_interface.get_1_10v_feedback()
        light = tester_interface.get_light()

        data["Set value"] = value
        data["Read value"] = feedback

        # assert abs(feedback - value) < 3  # 3% max error on feedback level

        data["measured"] = measured
        data["light"] = light
        data.start_next_test(False)

    # calculate error
    data["Feedback diff"] = data["Set value"] - data["Read value"]

    light_max = np.max(data["light"])
    if light_max != 0:
        data["Light [%]"] = data["light"] * 100.0 / light_max

        data["Light diff"] = data["Set value"] - data["Light [%]"]

    # save data
    data.save("report.xlsx")

    plt.plot(data["Set value"], data["light"], '+b-')
    plt.title("Set dimming level vs light level")
    plt.xlabel("Dimming level [%]")
    plt.ylabel("Light level [lux]")

    plt.grid(True)
    # save figure
    plt.savefig("light.png")
    plt.close()
    extra.append(extras.image("light.png"))
    print(f"Data:\n{data}")


@pytest.fixture
def g4_interface():
    from hat.comms.g4_interface_cli import G4

    address = "COM69"
    interface_type = "SERIAL"

    g4 = G4(address, interface_type)

    return g4


@pytest.fixture
def tester_interface():
    # interface
    tester_type = "MULT"
    address = "USB0::0x05E6::0x6510::04472337::INSTR"

    if tester_type == "MULT":
        tester = MultimeterInstP.MultimeterInst(address)
        tester.get_1_10v_feedback = lambda: tester.get_voltage_dc()
        tester.get_light = lambda: 0
    else:
        tester = TesterInterface(address)

    return tester


if __name__ == "__main__":
    print("Executing python tests")
    # test_ip = "192.168.1.83"
    # interface_type = "WEBSOCKET"

    # interface
    tester = "MULT"
    address = "USB0::0x05E6::0x6510::04472337::INSTR"

    g4 = G4(address, interface_type)

    print("end!")
