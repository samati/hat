import time
import serial
import serial.tools.list_ports
import logging
from hat.drivers import AcSourceInstrument
from hat.utils.DataStorage import DataStorage
import numpy as np
import sys
import os

LOG_FILENAME = 'logs/P7CM_test.txt'

log = logging.getLogger(__name__)
stream_handler = logging.StreamHandler()
stream_handler.setLevel("DEBUG")
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
stream_handler.setFormatter(formatter)
# add ch to logger
log.addHandler(stream_handler)

log.setLevel("DEBUG")


def search_ports(ports_to_search):
    ports = [dev.device for dev in serial.tools.list_ports.comports()]
    ports = set(ports)

    res = ports_to_search.issubset(ports)
    log.debug(f"Ports present: {res}")
    return res


if __name__ == "__main__":
    from hat.drivers import Oscilloscope

    # data files
    directory = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet IOT\Project Folder\05_Support\20210728_HCM-240_ACDC_PSU\long_term_tests"
    time_directory = os.path.join(directory, time.strftime("%Y%m%d_%H%M"))

    filename = "P7CM.xls"

    data = DataStorage()
    data.open(filename, directory=time_directory)

    # control instruments
    use_osc = False

    # variables for AC
    ac_voltage = 0
    dc_voltage = 0
    frequency = 50
    duty_cycle = 50
    count = 1

    # number of tests
    number_of_tests = 100000
    # test type
    # test_type = "list"
    test_type = "random"

    # variables
    use_timeout = True
    timeout = 400  # time in seconds

    # control startup
    use_deferred_startup = True
    minimum_startup_delay = 11-3  # minimum time to be delay startup
    maximum_startup_delay = 11+3  # maximum time to be delay startup
    discharge_time = 30  # time to discharge capacitors

    # interval
    start_interval = 925-25  # start interval in miliseconds
    step_interval = 10  # step interval in miliseconds
    stop_interval = 925+25

    # oscilloscope address
    address = "VICP::192.168.8.154"

    # stop interval in miliseconds
    nominal_voltage = 230  # voltage at the start
    nominal_frequency = 50  # frequency at the start

    # port_to_search = {'COM52', 'COM53', 'COM54', 'COM55', 'COM56', 'COM57', 'COM58'}
    #
    # port_to_search = {'COM146', 'COM147', 'COM149', 'COM150', 'COM151', 'COM153', 'COM152'}
    # port_to_search = {'COM182', 'COM179', 'COM184', 'COM186', 'COM181', 'COM185', 'COM180'}
    port_to_search = {'COM196', 'COM197', 'COM198', 'COM199', 'COM200', 'COM201', 'COM195'}
    state = search_ports(port_to_search)
    print(f"Current state: {state}")

    # connect oscilloscope
    if use_osc:
        osc = Oscilloscope.Oscilloscope(address)
        osc.open()
        osc.arm_single_trigger()

    ac = AcSourceInstrument.AcSourceInstrument("ASRL40::INSTR", reset=False)
    # ac.reset()

    log.info("Start testing")

    ac.set_voltage_AC(nominal_voltage)
    ac.set_frequency(nominal_frequency)
    ac.set_output_state(True)
    time.sleep(5)

    if test_type == "list":
        interval_list = range(start_interval, stop_interval, step_interval)
    elif test_type == "random":
        interval_list = np.random.randint(start_interval, stop_interval, number_of_tests)
    else:
        log.error(f"Invalid tests type: {test_type}")
        sys.exit(-1)

    for cycle, interval in enumerate(interval_list, start=1):
        try:
            log.info(f"Cycle {cycle}: Testing interval {interval}")
            ac.config_pulse_mode(ac_voltage, dc_voltage, frequency, count, duty_cycle, interval * 100 / duty_cycle)
            data["Timestamp"] = time.asctime()
            data["interval"] = interval

            if use_deferred_startup:
                log.info("Using deferred startup")
                ac.set_output_state(False)
                log.info(f"Power supply off, wait {discharge_time} seconds before starting")
                time.sleep(discharge_time)

                data["Dischage [s]"] = discharge_time

                ac.set_output_state(True)

                log.info("Power supply turn on!!")

                if use_osc:
                    # check if oscilloscope was triggered and save picture
                    try:
                        log.info("Arm single trigger")

                        osc.arm_single_trigger()
                    except Exception as e:
                        log.exception(f"problem with oscilloscope, {e}")

                wait_time = np.random.randint(minimum_startup_delay, maximum_startup_delay)

                data["Wait time [s]"] = wait_time

                log.info(f"Waiting {wait_time} before making cut")
                time.sleep(wait_time)

            else:
                time.sleep(1)

            log.info("Making cut...")
            ac.set_trigger(True)
            start_time = time.time()
            log.info("Sleep 10 seconds after AC interrupt")
            time.sleep(10)

            if use_osc:
                try:
                    # check if oscilloscope was triggered, if yes, save image
                    if osc.is_acquisition_completed():
                        log.info("Saving oscilloscope after cut")
                        osc.get_screen_capture(f"try_#{cycle}_cut_{time.strftime('%H%M')}.jpg", time_directory)

                    # set trigger again for reset
                    osc.arm_single_trigger()
                except Exception as e:
                    log.exception(f"problem with oscilloscope, {e}")

            present = search_ports(port_to_search)
            elapsed_time = time.time() - start_time
            while not present and (elapsed_time < timeout or not use_timeout):
                time.sleep(5)
                present = search_ports(port_to_search)
                elapsed_time = time.time() - start_time
                log.debug(f"Present: {present}, elapsed: {elapsed_time:.02f}s")

            if not present:
                log.warning("Not present after timeout, stopping... Please check")
                sys.exit(-1)
            else:
                log.info(f"Ok after {elapsed_time} seconds")

            if use_osc:
                # check if oscilloscope was triggered and save picture
                if osc.is_acquisition_completed():
                    try:
                        log.info("Saving oscilloscope after reset")
                        osc.get_screen_capture(f"try_#{cycle}_reset_{time.strftime('%H%M')}.jpg", time_directory)

                        osc.arm_single_trigger()
                    except Exception as e:
                        log.exception(f"problem with oscilloscope, {e}")

                # set trigger again for reset
                osc.arm_single_trigger()

            data["Recover time [s]"] = elapsed_time

            data.start_next_test()

        except Exception as e:
            print(f"error: {e}, continuing...")

    log.info("End testing")
