import binascii
import re
from hat.comms.ZigbeeInterface import ZigbeeInterface
import os
import pandas as pd
from hat.utils.utils import dict_to_file
from hat.comms.P7CM import P7CM
import time


if __name__ == "__main__":
    # tests to execute

    # TODO: get pin configuration on gemalto using at commands

    # get_data_points = True
    get_data_points = False

    # get_zigbee_at = True
    get_zigbee_at = False

    get_gemalto_data = True
    # get_gemalto_data = False

    # configuration
    gemalto_usb = True

    # variables
    zigbee_com_port = "COM77"
    gemalto_port = "COM106"
    remote_device_addr = "00:13:A2:00:41:52:CE:65"

    # directory and files
    data_directory = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet IOT\Project Folder\07_Returned_Devices\data"
    datapoints_file = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet IOT\Project Folder\07_Returned_Devices\LuCo IOT datapoint commands template.xlsx"
    at_file = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet IOT\Project Folder\07_Returned_Devices\AT_Commands.xlsx"

    # START OF TESTS
    print(f"Making data directory")
    device_directory = os.path.join(data_directory, remote_device_addr.replace(":", ""))
    if not os.path.exists(device_directory):
        print(f"Creating device directory {device_directory}")
        os.mkdir(device_directory)

    print("Creating serial")

    if get_zigbee_at:
        zb = ZigbeeInterface(zigbee_com_port)
        zb.set_remote(remote_device_addr)
        res_list = []

        print("Getting zigbee at")
        at_commands = pd.read_excel(at_file)
        ILLEGAL_CHARACTERS_RE = re.compile(r'[\000-\010]|[\013-\014]|[\016-\037]')

        for idx, row in at_commands.iterrows():
            at = row["AT"]
            description = row["Description"]
            # print(f"reading command {at}, description: {description}")
            res = zb.get_at_parameter(at)
            if len(res) <= 16:
                res_int = int.from_bytes(res, byteorder="big", signed=False)
            else:
                res_int = 0

            res_dict = {}
            res_dict["AT"] = at
            res_dict["description"] = description
            try:
                res_ascii = res.decode("ascii")
                if ILLEGAL_CHARACTERS_RE.search(res_ascii):
                    res_ascii = "0x" + binascii.hexlify(res).decode("ascii")
                res_dict["result_str"] = res_ascii
            except Exception as e:
                res_dict["result_str"] = ""
            res_dict["result_int"] = res_int
            res_dict["result_hex"] = f"{res_int:#02x}"
            res_list.append(res_dict)

        dict_to_file(device_directory, "zigbee_at", res_list)
        zb.close()
        print("Get at commands from zigbee end!")


    if get_data_points:
        zb = ZigbeeInterface(zigbee_com_port)
        zb.set_remote(remote_device_addr)
        res_dict = []

        print("getting data points")
        datapoints = pd.read_excel(datapoints_file)

        for idx, row in datapoints.iterrows():
            print(f"Idx {idx}, row {row}")
            if row["Execute"] == 1:
                res = zb.execute_command(row["Command"])
                print(f"Res {res}")

                res_dict.append({
                    "Command": row["Command"],
                    "res": res
                })

        dict_to_file(device_directory, "datapoints", res_dict)
        zb.close()

    if get_gemalto_data:
        print("Get gemalto data")

        res_dict = {}

        files_dir = os.path.join(device_directory, "files")
        if not os.path.exists(files_dir):
            print(f"Create file directory: {files_dir}")
            os.mkdir(files_dir)

        if gemalto_usb:
            p7cm = P7CM(gemalto_port)
            p7cm._intf.set_prompt("\r\nOK\r\n")
        else:
            p7cm = P7CM(zigbee_com_port, "ZIGBEE")
            p7cm._intf.set_remote(remote_device_addr)

            p7cm._intf.set_prompt("\r\nOK\r\n\n[CLI]>")

        print("Global stats")
        global_stats = p7cm.get_global_storage_stats()
        res_dict["Global file statistics"] = global_stats
        dict_to_file(device_directory, "global", global_stats, save_excel=False)
        print("Copying files")
        stats_list = p7cm.copy_files(files_dir)
        res_dict["File statistics"] = stats_list

        dict_to_file(device_directory, "file_statistics", stats_list)

        print("Identification")
        gemalto_id = p7cm.get_identification()
        res_dict["Gemalto id"] = gemalto_id

        print("Saving info")

        dict_to_file(device_directory, "gemalto", gemalto_id)


    print("end!")
