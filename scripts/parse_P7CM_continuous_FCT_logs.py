import pandas as pd
import os

if __name__ == "__main__":
    base_dirname = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet IOT\Project Folder\06_Factory\Logs\continuoes logfiles LUCO P7CM+\continuoes logfiles"

    first= True
    names = ["MAC Address", "MO No.", "Operator's E.N.", "Shift	Date", "Date", "Time", "Model:", "Current Consumption", "12V Measurement", "DALI Voltage", "Serial Number", "Product Key", "Internal 5V", "I2C Status", "Calibration (Voltage)", "Calibration (Current)", "Calibration (Power)", "Calibration Factors", "Photocell Night", "Photocell Day", "No Load Bit Off", "Voltage Across Relay", "No Load Bit On", "Temperature", "MCU FW (Base PCB)", "ZigBee FCT Mode", "SIM Card IMSI", "Cellular IMEI", "EHS8", "MPU FW (MCU PCB)", "Cellular Low", "Cellular High", "Check Sensor", "RSSIt", "RSSIr", "Xbee FW", "Xbee HW", "DALI Status", "DIM 10V", "DIM 1V", "NI Parameters", "Factory Reset", "Test Time", "Pass/Fail"]
    def gemalto_classification(column):
        if isinstance(column,str) and column.startswith("04"):
            return "G3+"
        elif isinstance(column,str) and column.startswith("03"):
            return "G3"
        else:
            return "Unknown"

    def xbee_classification(column):
        if (isinstance(column,str) or isinstance(column, float))  and str(column).startswith("4"):
            return "S2C"
        elif isinstance(column,str) and column.startswith("7"):
            return "S2D"
        else:
            return "Unknown"

    for dirpath, dirname, filenames in os.walk(base_dirname):
        for filename in filenames:
            if os.path.splitext(filename)[1] != ".csv":
                continue
            print(f"Parsing file: {filename}")
            if first:
                data = pd.read_csv(os.path.join(dirpath,filename),skiprows=2, index_col=False, names=names, header=0)
                first = False
            else:
                data = data.append(pd.read_csv(os.path.join(dirpath,filename), skiprows=2, index_col=False, names=names, header=0), ignore_index=True)


    # make gemalto
    data["Gemalto"] = [gemalto_classification(x) for x in data["EHS8"]]
    data["Xbee version"] = [xbee_classification(x) for x in data["Xbee FW"]]

    partial = data[data["Pass/Fail"] == "Pass"]
    partial = partial[["MAC Address", "EHS8", "Gemalto", "Xbee FW", "Xbee version"]]

    partial.to_excel("partial.xls")

    data.to_excel("complete.xls")


    print("Parsing files")