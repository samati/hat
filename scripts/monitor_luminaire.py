"""
# Filename:     monitor_luminaire.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   29/06/2021
# Last Update:  29/06/2021
#
# Version:      1.0
# Filetype:     
# Description:  Objective is to monitor a luminaire in a 24h
# STATUS:       Stable
# Limitation:   None
"""
from hat.comms import g4_interface_cli
import time
import logging
from threading import Lock, Thread
from tb_device_mqtt import TBDeviceMqttClient, TBPublishInfo

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(levelname)s - %(module)s - %(lineno)d - %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')


class Monitor(Thread):
    def __init__(self, url, token, COM_port):
        Thread.__init__(self)

        # variables
        self._url = url
        self._token = token
        self._com_port = COM_port
        self._client = None
        self._g4 = None
        self.lock = Lock()
        self._status = False

        # dependently of request method we send different data back

    def on_server_side_rpc_request(self, client, request_id, request_body):
        print(request_id, request_body)
        if request_body["method"] == "getStatus":
            client.send_rpc_reply(request_id, int(self._status))
        elif request_body["method"] == "setStatus":
            print(f"request_body: {request_body}")
            set_status = request_body["params"]
            print(f"Setting status to {set_status}")

            with self.lock:
                self._g4.flex_set_relay(set_status)

            self._status = set_status
        client.send_rpc_reply(request_id, self._status)

    def run(self):
        # connect to G4 using UART
        self._g4 = g4_interface_cli.G4(COM_port)
        self._g4.set_flex_app_prompt()

        # create thingsboard client
        self._client = TBDeviceMqttClient(url, token)

        # Connect to ThingsBoard
        self._client.connect()

        # register RPC
        self._client.set_server_side_rpc_request_handler(self.on_server_side_rpc_request)

        # main thread loop
        while True:
            try:
                with self.lock:
                    complete_data = {"ts": int(time.time() * 1000)}
                    complete_data["values"] = self._g4.flex_get_meter_data()
                    complete_data["values"]["1-10v"] = self._g4.flex_get_1_10v_voltage()

                # Sending telemetry and checking the delivery status (QoS = 1 by default)
                result = self._client.send_telemetry(complete_data)
                # get is a blocking call that awaits delivery status
                success = result.get() == TBPublishInfo.TB_ERR_SUCCESS
                time.sleep(1)
            except Exception as e:
                print(f"Error: {e}")
                time.sleep(1)


if __name__ == "__main__":
    # variables
    # ThingsBoard  URL
    url = "192.168.8.141"
    COM_port = "COM29"
    token = "oWqs5KgBj5vPu32PBOyh"

    # create objects

    start_time = time.time()

    thread = Monitor(url, token, COM_port)
    thread.start()
    thread.join()
    print("end!")
