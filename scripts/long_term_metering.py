
import time
import logging

import hat.utils.DataStorage
from hat.comms import g4_interface_cli
from hat.utils import utils

log = logging.getLogger(__name__)
stream_handler = logging.StreamHandler()
stream_handler.setLevel("DEBUG")
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
stream_handler.setFormatter(formatter)
# add ch to logger
log.addHandler(stream_handler)

log.setLevel("DEBUG")

if __name__ == "__main__":
    log.info("Starting test")

    ### test parameters
    time_sleep = 10
    filename = "data_2.xlsx"

    #### connection strings
    g4_com_port = "COM72"

    #### connect to instruments and G4
    g4 = g4_interface_cli.G4(g4_com_port)
    g4.set_global_loglevel(4)
    ## create data structure
    data = hat.utils.DataStorage.DataStorage(filename)

    idx = 1
    while True:
        print(f"Cycle {idx}")
        idx += 1

        # read from G4
        g4_voltage = g4.lwm2m_get_supply_voltage()
        g4_current = g4.lwm2m_get_supply_current()
        g4_active_power = g4.lwm2m_get_active_power()
        g4_freq = g4.lwm2m_get_frequency()
        g4_pf = g4.lwm2m_get_power_factor()
        g4_energy = g4.lwm2m_get_active_energy()

        data["G4 voltage [V]"] = g4_voltage
        data["G4 current [A]"] = g4_current
        data["G4 active power [W]"] = g4_active_power
        data["G4 active energy [kWh]"] = g4_energy
        data["G4 frequency [Hz]"] = g4_freq
        data["G4 power factor"] = g4_pf

        data.start_next_test()
        print
        time.sleep(time_sleep)

    data.save()

    # teardown
    log.info("End!")
