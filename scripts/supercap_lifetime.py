from hat.drivers.AcPowerMeterInstrument import AcPowerMeterInstrument
import serial
import time
import logging
from logging.handlers import RotatingFileHandler
import numpy as np
import platform
from functools import wraps

from hat.utils.DataStorage import DataStorage

LOG_FILENAME = 'console.txt'

log = logging.getLogger("Supercap")
stream_handler = logging.StreamHandler()
stream_handler.setLevel("DEBUG")
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
stream_handler.setFormatter(formatter)
# add ch to logger
log.addHandler(stream_handler)

# create handler for file logging
# Add the log message handler to the logger
file_handler = RotatingFileHandler(
              LOG_FILENAME, maxBytes=10000000, backupCount=5)

# set formatter for file_handler
file_handler.setFormatter(formatter)
log.addHandler(file_handler)

log.setLevel("INFO")

#### Defaults #####
DEFAULT_RETRIES = 3
DEFAULT_TIMEOUT = 5
DEFAULT_DELAY = 3
DEFAULT_BACKOFF = 2

# function to implement a retry mechanism
def retry(ExceptionToCheck, tries=4, delay=3, backoff=2, logger=None):
    """Retry calling the decorated function using an exponential backoff.

    http://www.saltycrane.com/blog/2009/11/trying-out-retry-decorator-python/
    original from: http://wiki.python.org/moin/PythonDecoratorLibrary#Retry

    :param ExceptionToCheck: the exception to check. may be a tuple of
        exceptions to check
    :type ExceptionToCheck: Exception or tuple
    :param tries: number of times to try (not retry) before giving up
    :type tries: int
    :param delay: initial delay between retries in seconds
    :type delay: int
    :param backoff: backoff multiplier e.g. value of 2 will double the delay
        each retry
    :type backoff: int
    :param logger: logger to use. If None, print
    :type logger: logging.Logger instance
    """

    def deco_retry(f):

        @wraps(f)
        def f_retry(*args, **kwargs):
            mtries, mdelay = tries, delay
            while mtries > 1:
                try:
                    return f(*args, **kwargs)
                except ExceptionToCheck as e:
                    msg = "%s, Retrying in %d seconds..." % (str(e), mdelay)
                    if logger:
                        logger.warning(msg)
                    else:
                        print(msg)
                    time.sleep(mdelay)
                    mtries -= 1
                    mdelay *= backoff
            return f(*args, **kwargs)

        return f_retry  # true decorator

    return deco_retry


class RemoteControl:
    def __init__(self, serial_port, baudrate=115200, timeout=DEFAULT_TIMEOUT, n_retries=DEFAULT_RETRIES):
        log.info(f"Connecting to remote control on port {serial_port}")
        self._intf = serial.Serial(serial_port, baudrate=baudrate, timeout=timeout)
        self._intf.read_all()
        self.n_retries = n_retries

    @retry(Exception, DEFAULT_RETRIES, DEFAULT_DELAY, DEFAULT_BACKOFF)
    def set_relay(self, index, state):
        log.info(f"Setting relay {index} to {state}")
        self._intf.read_all()
        cmd = f"relay {index} {state}\n".encode("ASCII")
        self._intf.write(cmd)
        resp = self._intf.read_until("\n".encode("ASCII"))
        resp += self._intf.read_until("\n".encode("ASCII"))
        log.debug(f"response: {resp}")

    @retry(Exception, DEFAULT_RETRIES, DEFAULT_DELAY, DEFAULT_BACKOFF)
    def read_voltage(self):
        # log.debug(f"Read voltage")
        self._intf.read_all()
        cmd = f"read\n".encode("ASCII")
        self._intf.write(cmd)
        resp = self._intf.read_until("\n".encode("ASCII"))
        resp = self._intf.read_until("\n".encode("ASCII"))
        # log.debug(f"Voltage: {resp}")
        try:
            res = float(resp)
            log.debug(f"Voltage: {res}V")
            return res
        except Exception as e:
            log.exception(f"Exception while reading from remote, {e}")
        return

    def clear(self):
        self._intf.flush()
        self._intf.flushInput()
        self._intf.flushOutput()
        self._intf.read_all()


if __name__ == "__main__":
    ############
    # variables
    test_type = "PM"
    log.info("Created Driver for PM1000+")

    USB_connection_string = "USB0::0xABCD::0x03E8::100008201733::INSTR"



    filename = time.strftime("data/results_%Y%m%d_%H%M%S.xls")
    test_remote = False
    sleep_between_analog_read = 0.05

    cycle = 1
    max_cycles = 100

    # for simple utilization in windows and linux
    if platform.system() == "Windows":
        serial_port = "COM5"
    else:
        serial_port = "/dev/ttyUSB0"

    charging_timeout = 120  # max number of seconds to charge
    charge_voltage_threshold = 4.1  # voltage to consider the supercap charged
    sleep_after_charging = 10  # seconds to wait after voltage pass the charge_voltage_threshold

    # discharging
    discharge_voltage_threshold = 0.2  # voltage to consider the supercaps discharged
    discharging_timeout = 120  # max number of seconds to discharge
    tau_percentage = np.exp(-1)  # factor to multiply the charged voltage

    # resistor value
    resistor = 4.5  # value for the resistor to calculate the capacitance

    # after discharging
    time_between_cycles = 10  # number of seconds to wait between cycles


    ## connect to devices
    if test_type == "PM":
        pm = AcPowerMeterInstrument(USB_connection_string, reset=False, clear=False)

        # pm.reset()
        # time.sleep(10)

        pm.clear_measurements()


    ## create data structure
    data = DataStorage(filename)

    #####################################
    log.info("Executing supercap lifetime")
    # initialization
    remote = RemoteControl(serial_port)
    time.sleep(3)
    remote.clear()

    # create helper functions
    set_24v_on = lambda: remote.set_relay(1, 1)
    set_24v_off = lambda: remote.set_relay(1, 0)

    set_load_on = lambda: remote.set_relay(2, 1)
    set_load_off = lambda: remote.set_relay(2, 0)

    read_voltage = lambda: remote.read_voltage()

    # test
    if test_remote:
        set_24v_on()
        set_24v_off()

        set_load_on()
        set_load_off()

        log.info(f"Supercap voltage: {read_voltage()}")

    ## set initial conditions
    set_24v_off()
    set_load_off()

    ##################################
    # start of measurement loop
    for i in range(max_cycles):
        data["Cycle"] = cycle
        log.info(f"Starting cycle: #{cycle}")
        cycle += 1

        log.info("Start charging cycle")
        set_24v_on()
        start_time = time.time()

        actual_voltage = read_voltage()

        while actual_voltage < charge_voltage_threshold and time.time() - start_time < charging_timeout:
            time.sleep(sleep_between_analog_read)
            actual_voltage = read_voltage()

        charging_time = time.time() - start_time

        data["Charging time [s]"] = charging_time
        log.info(f"Charged finished in {charging_time} seconds with voltage {actual_voltage}")

        log.info(f"Sleeping for {sleep_after_charging} seconds to complete charging")
        time.sleep(sleep_after_charging)

        charging_voltage = read_voltage()
        log.info(f"Charged to {charging_voltage}")
        data["Charging voltage [V]"] = charging_voltage

        tau_voltage_threshold = charging_voltage * tau_percentage
        log.info(f"Voltage to calculate capacitance {tau_voltage_threshold}V")
        # start discharging
        set_24v_off()
        log.info("Starting discharging")

        time.sleep(3)  # just to make the relay switchs in different times

        start_discharge_time = time.time()
        set_load_on()

        tau_found = False  # store if Tau voltage was already found

        actual_voltage = read_voltage()
        while actual_voltage > discharge_voltage_threshold and time.time() - start_discharge_time < discharging_timeout:
            time.sleep(sleep_between_analog_read)
            actual_voltage = read_voltage()
            if not tau_found and actual_voltage <= tau_voltage_threshold:
                tau_found = True
                discharge_tau_time = time.time() - start_discharge_time
                log.info(f"tau discharge time {discharge_tau_time}")
                data["Tau voltage [V]"] = tau_voltage_threshold
                data["Tau time [s]"] = discharge_tau_time
                capacitance = discharge_tau_time / resistor
                data["Estimated Capacitance [F]"] = capacitance

        complete_discharge_time = time.time() - start_discharge_time
        data["Discharge time [s]"] = complete_discharge_time

        log.info(f"complete discharge time {complete_discharge_time}")

        log.info(f"Sleeping {time_between_cycles} seconds between cycles")
        time.sleep(time_between_cycles)

        log.info(f"Set load off")
        set_load_off()

        # sleep a bit
        time.sleep(3)

        log.info(f"Last measurement:\n{data.last_meas_str()}")
        data.start_next_test()

    data.close()
    # end
    log.info("END!!")
