import hat.utils.DataStorage
import numpy as np
import time
import logging
from hat.comms import g4_interface_cli
from hat.drivers import AcPowerMeterInstrument
from hat.drivers import AcSourceInstrument
from hat.drivers import ACDC_load
from hat.utils import utils
import matplotlib.pyplot as plt

log = logging.getLogger(__name__)
stream_handler = logging.StreamHandler()
stream_handler.setLevel("DEBUG")
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
stream_handler.setFormatter(formatter)
# add ch to logger
log.addHandler(stream_handler)

log.setLevel("DEBUG")

if __name__ == "__main__":
    log.info("Starting test")

    # variables
    current_test = False  # current
    plot = True    # make plot


    ### voltage under test
    # voltages = range(90, 281, 10)  # voltage to test
    voltages = range(110, 240, 50)
    # currents = np.arange(0, 3 + 0.01, 0.5)  # current to test
    if current_test:
        currents = np.arange(0, 3 + 0.1, 0.1)
    else:
        currents = [0]
    max_current_peak = 5  # needs to account with crest factor

    workarround_dvt = True  # set true if doing workarround for DVT

    ### test parameters
    time_sleep = 4
    filename = "data.xls"

    #### connection strings
    USB_connection_string = "USB0::0xABCD::0x03E8::100008201733::INSTR"
    g4_com_port = "COM29"
    ac_com_port = "ASRL40::INSTR"
    load_com_port = "ASRL209::INSTR"

    #### connect to instruments and G4
    ac = AcSourceInstrument.AcSourceInstrument(ac_com_port, reset=False)

    if current_test:
        load = ACDC_load.ACDCLoad(load_com_port, reset=False)

    pm = AcPowerMeterInstrument.AcPowerMeterInstrument(USB_connection_string)

    g4 = g4_interface_cli.G4(g4_com_port)
    g4.set_global_loglevel(4)
    # g4.set_relay_state(False if workarround_dvt else True)

    ## create data structure
    data = hat.utils.DataStorage.DataStorage()
    data.open(filename)

    #### turn on ac
    ac.set_voltage_AC(voltages[0])
    ac.set_output_state(True)

    ### configure load
    if current_test:
        load.set_load_mode("CURR")
        load.clear_protection_status()
        load.set_current_AC(0)
        load.set_peak_current_AC(max_current_peak)
        load.set_output_state(True)

    ## clear
    g4.set_global_loglevel(4)

    for voltage in voltages:
        log.info(f"Testing voltage {voltage}")
        ac.set_voltage_AC(voltage)
        time.sleep(time_sleep)

        # set current level
        for current in currents:
            log.info(f"Testing voltage {voltage}V, current {current}")

            if current_test:
                load.set_current_AC(current)

            time.sleep(time_sleep)

            # read from G4
            g4_voltage = g4.lwm2m_get_supply_voltage()

            if current_test:
                g4_current = g4.lwm2m_get_supply_current()

            g4_active_power = g4.lwm2m_get_active_power()
            g4_freq = g4.lwm2m_get_frequency()
            g4_pf = g4.lwm2m_get_power_factor()

            # PM1000
            meas = pm.get_measurement()

            data["Set Voltage [V]"] = voltage
            data["G4 voltage [V]"] = g4_voltage

            if current_test:
                data["Set Current [A]"] = current
                data["G4 current [A]"] = g4_current

            data["G4 active power [W]"] = g4_active_power
            data["G4 frequency [Hz]"] = g4_freq
            data["G4 power factor"] = g4_pf

            for key, value in meas.items():
                data[f"PM {key}"] = value

            data.start_next_test()


    data.save()

    if plot:
        plt.plot(data["PM Vrms"], data["G4 voltage [V]"], "-go")
        plt.legend("Voltage")
        plt.xlabel("Reference Voltage [V]")
        plt.ylabel("G4 Voltage [V]")
        plt.title("G4 Voltage test vs Reference Voltage Meter[V]")
        plt.show()

    # teardown
    if current_test:
        load.set_output_state(False)
    log.info("End!")