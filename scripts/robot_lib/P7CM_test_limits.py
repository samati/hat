"""
# Filename:     P7CM_test_limits.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   23/04/2021
# Last Update:  23/04/2021
#
# Version:      1.0
# Filetype:     
# Description:
# STATUS:       Stable
# Limitation:   None
"""

# dict on the beginig force Robot to interpret as a Dictionary
DICT__resistance_limits = {
    "LineInToNeutral": {"min": 0,
                        "max": 10e6
                        },
    "LineOutToNeutral": {"min": 0,
                         "max": 1e6
                         },
    "LineInToLineOut": {"min": 0,
                        "max": 1e6
                        },
    "LsiToGnd": {"min": 0,
                 "max": 10e6
                 },
    "DaliToGnd": {"min": 500,
                  "max": 1e6
                  },
    "PsuToGnd": {"min": 0,
                 "max": 10e6
                 }
}

# Keys cannot start with numbers because they are interpreted as variables

DICT__voltage_limits = {
    "LineInToNeutral": {"min": 220,
                        "max": 250
                        },
    "LineOutToNeutral": {"min": 0,
                         "max": 250
                         },
    "LineOutToNeutralRelayOn": {"min": 220,
                                "max": 250
                                },
    "LineOutToNeutralRelayOff": {"min": 0,
                                 "max": 20
                                 },
    "LsiToGnd": {"min": 10,
                 "max": 13
                 },
    "DaliToGnd": {"min": 10,
                  "max": 16
                  },
    "PsuToGnd": {"min": 10,
                 "max": 13
                 },

    "PowerConsumption": {"min": -5,
                         "max": -0.5},

    "Set_1_10v_100": {"min": 9.7,
                      "max": 10.3},

    "Set_1_10v_20": {"min": 1.7,
                     "max": 2.3},

    "Feedback_1_10v_diff": {"min": -0.3,
                            "max": 0.3}
}

# LIST__ prefix used to mark as a List
LIST__zigbee_ats = [
    {'AT': 'ID', 'Description': 'Extended PAN ID', "type": "hex", "check": 0x0000000000001337},
    {'AT': 'SC', 'Description': 'Scan Channels', "type": "hex", "check": 0xffff},
    {'AT': 'SD', 'Description': 'Scan Duration', "type": "hex", "check": 0x03},
    {'AT': 'ZS', 'Description': 'Zigbee Stack Profile', "type": "hex", "check": 0x00},
    {'AT': 'NJ', 'Description': 'Node Join Time', "type": "hex", "check": 0xff},
    {'AT': 'NW', 'Description': 'Network Watchdog Timeout', "type": "hex", "check": 0x0000},
    {'AT': 'JV', 'Description': 'Coordinator Join Verification', "type": "hex", "check": 0x00},
    {'AT': 'JN', 'Description': 'Join Notification', "type": "hex", "check": 0x01},
    {'AT': 'OP', 'Description': 'Operating Extended PAN ID', "type": "hex", "check": 0x0000000000001337},
    {'AT': 'OI', 'Description': 'Operating 16-bit PAN ID', "type": "hex", "check": 0xdb30},
    {'AT': 'CH', 'Description': 'Operating Channel', "type": "hex", "check": 0x17},
    {'AT': 'NC', 'Description': 'Number of Remaining Children', "type": "hex", "check": None},
    {'AT': 'CE', 'Description': 'Coordinator Enable', "type": "hex", "check": 0x00},
    {'AT': 'DO', 'Description': 'Miscellaneous Device Options', "type": "hex", "check": 0x01},
    {'AT': 'DC', 'Description': 'Joining Device Controls', "type": "hex", "check": 0x00000000},
    {'AT': 'II', 'Description': 'Initial 16-bit PAN ID', "type": "hex", "check": 0xffff},
    {'AT': 'SH', 'Description': 'Serial Number High', "type": "hex", "check": None},
    {'AT': 'SL', 'Description': 'Serial Number Low', "type": "hex", "check": None},
    {'AT': 'MY', 'Description': '16-bit Network Address', "type": "hex", "check": None},
    {'AT': 'MP', 'Description': '16-bit Parent Network Address', "type": "hex", "check": None},
    {'AT': 'DH', 'Description': 'Destination Address High', "type": "hex", "check": None},
    {'AT': 'DL', 'Description': 'Destination Address Low', "type": "hex", "check": None},
    {'AT': 'NI', 'Description': 'Node Identifier', "type": "hex", "check": None},
    {'AT': 'NH', 'Description': 'Maximum Unicast Hops', "type": "hex", "check": 0x1e},
    {'AT': 'BH', 'Description': 'Broadcast Hops', "type": "hex", "check": 0x01},
    {'AT': 'AR', 'Description': 'Aggregate Routing Notification', "type": "hex", "check": 0xff},
    {'AT': 'DD', 'Description': 'Device Type Identifier', "type": "hex", "check": 0x00030010},
    {'AT': 'NT', 'Description': 'Node Discover Timeout', "type": "hex", "check": 0x003c},
    {'AT': 'NO', 'Description': 'Network Discovery Options', "type": "hex", "check": 0x02},
    {'AT': 'NP', 'Description': 'Maximum Packet Payload Bytes', "type": "hex", "check": 0x00ff},
    {'AT': 'CR', 'Description': 'Conflict Report', "type": "hex", "check": None},
    {'AT': 'SE', 'Description': 'Source Endpoint', "type": "hex", "check": None},
    {'AT': 'DE', 'Description': 'Destination Endpoint', "type": "hex", "check": None},
    {'AT': 'CI', 'Description': 'Cluster ID', "type": "hex", "check": None},
    {'AT': 'TO', 'Description': 'Transmit Options', "type": "hex", "check": 0x00},
    {'AT': 'PL', 'Description': 'TX Power Level', "type": "hex", "check": 0x04},
    {'AT': 'PP', 'Description': 'Power at PL4', "type": "hex", "check": 0x08},
    {'AT': 'PM', 'Description': 'Power Mode', "type": "hex", "check": 0x01},
    {'AT': 'EE', 'Description': 'Encryption Enable', "type": "hex", "check": 0x01},
    {'AT': 'EO', 'Description': 'Encryption Options', "type": "hex", "check": 0x00},
    {'AT': 'KY', 'Description': 'Link Key', "type": "hex", "check": None},
    {'AT': 'NK', 'Description': 'Trust Center Network Key', "type": "hex", "check": None},
    {'AT': 'BD', 'Description': 'Interface Data Rate', "type": "hex", "check": 0x00000007},
    {'AT': 'NB', 'Description': 'Parity', "type": "hex", "check": 0x00},
    {'AT': 'SB', 'Description': 'Stop Bits', "type": "hex", "check": 0x01},
    {'AT': 'RO', 'Description': 'Packetization Timeout', "type": "hex", "check": 0x03},
    {'AT': 'D6', 'Description': 'DIO6/RTS', "type": "hex", "check": 0x01},
    {'AT': 'D7', 'Description': 'DIO7/CTS', "type": "hex", "check": 0x01},
    {'AT': 'AP', 'Description': 'API Enable', "type": "hex", "check": 0x01},
    {'AT': 'AO', 'Description': 'API Options', "type": "hex", "check": 0x01},
    {'AT': 'CT', 'Description': 'Command Mode Timeout', "type": "hex", "check": 0x0064},
    {'AT': 'GT', 'Description': 'Guard Times', "type": "hex", "check": 0x03e8},
    {'AT': 'CC', 'Description': 'Command Character', "type": "hex", "check": 0x2b},
    {'AT': 'CN', 'Description': 'Exit Command mode', "type": "hex", "check": None},
    {'AT': 'SP', 'Description': 'Sleep Period', "type": "hex", "check": 0x0020},
    {'AT': 'SN', 'Description': 'Number of Cycles Between ON_SLEEP', "type": "hex", "check": 0x0001},
    {'AT': 'SM', 'Description': 'Sleep Mode', "type": "hex", "check": 0x00},
    {'AT': 'ST', 'Description': 'Time before Sleep', "type": "hex", "check": 0x1388},
    {'AT': 'SO', 'Description': 'Sleep Options', "type": "hex", "check": 0x00},
    {'AT': 'WH', 'Description': 'Wake Host Delay', "type": "hex", "check": 0x0000},
    {'AT': 'PO', 'Description': 'Polling Rate', "type": "hex", "check": 0x0000},
    {'AT': 'D0', 'Description': 'AD0/DIO0 Configuration', "type": "hex", "check": 0x01},
    {'AT': 'D1', 'Description': 'AD1/DIO1/PTI_En Configuration', "type": "hex", "check": 0x00},
    {'AT': 'D2', 'Description': 'AD2/DIO2 Configuration', "type": "hex", "check": 0x00},
    {'AT': 'D3', 'Description': 'AD3/DIO3 Configuration', "type": "hex", "check": 0x00},
    {'AT': 'D4', 'Description': 'DIO4 Configuration', "type": "hex", "check": 0x00},
    {'AT': 'D5', 'Description': 'DIO5/Associate Configuration', "type": "hex", "check": 0x01},
    {'AT': 'D8', 'Description': 'DIO8/DTR/SLP_RQ', "type": "hex", "check": 0x01},
    {'AT': 'D9', 'Description': 'DIO9/ON_SLEEP', "type": "hex", "check": 0x01},
    {'AT': 'P0', 'Description': 'RSSI/PWM0 Configuration', "type": "hex", "check": 0x01},
    {'AT': 'P1', 'Description': 'DIO11/PWM1 Configuration', "type": "hex", "check": 0x00},
    {'AT': 'P2', 'Description': 'DIO12 Configuration', "type": "hex", "check": 0x00},
    {'AT': 'P3', 'Description': 'DIO13/DOUT Configuration', "type": "hex", "check": 0x01},
    {'AT': 'P4', 'Description': 'DIO14/DIN', "type": "hex", "check": 0x01},
    {'AT': 'P5', 'Description': 'DIO15/SPI_MISO', "type": "hex", "check": 0x01},
    {'AT': 'P6', 'Description': 'SPI_MOSI Configuration', "type": "hex", "check": 0x01},
    {'AT': 'P7', 'Description': 'DIO17/SPI_SSEL', "type": "hex", "check": 0x01},
    {'AT': 'P8', 'Description': 'DIO18/SPI_SCLK', "type": "hex", "check": 0x01},
    {'AT': 'P9', 'Description': 'DIO19/SPI_ATTN/PTI_DATA', "type": "hex", "check": 0x01},
    {'AT': 'PR', 'Description': 'Pull-up/Down Resistor Enable', "type": "hex", "check": 0x1fff},
    {'AT': 'PD', 'Description': 'Pull Up/Down Direction', "type": "hex", "check": 0x1fff},
    {'AT': 'LT', 'Description': 'Associate LED Blink Time', "type": "hex", "check": 0x00},
    {'AT': 'RP', 'Description': 'RSSI PWM Timer', "type": "hex", "check": 0x28},
    {'AT': 'IR', 'Description': 'I/O Sample Rate', "type": "hex", "check": 0x0000},
    {'AT': 'IC', 'Description': 'Digital Change Detection', "type": "hex", "check": 0x0000},
    {'AT': 'V+', 'Description': 'Voltage Supply Monitoring', "type": "hex", "check": None},
    {'AT': 'VR', 'Description': 'Firmware Version', "type": "hex", "check": None},
    {'AT': 'HV', 'Description': 'Hardware Version', "type": "hex", "check": None},
    {'AT': 'AI', 'Description': 'Association Indication', "type": "hex", "check": None},
    {'AT': '%V', 'Description': 'Voltage Supply Monitoring', "type": "hex", "check": None},
    {'AT': 'DB', 'Description': 'Received Signal Strength', "type": "hex", "check": None},
    {'AT': 'VL', 'Description': 'Version Long', "type": "str", "check": None}
]

# Gemalto AT
LIST__gemalto_ats = [
    {"AT": "ATI1", "Description": "Product identification information"},
    {"AT": "AT+CGMI", "Description": "Manufacturer identification"},
    {"AT": "AT+CGMM", "Description": "Model identification"},
    {"AT": "AT+CGMR", "Description": "Revision identification of software status"},
    {"AT": "AT+CGSN", "Description": "IMEI"},
    {"AT": "AT+CIMI", "Description": "IMSI"}
]

# Photocell values
Photocell_Max_value_at_dark = 100
Photocell_Min_value_at_light = 200

# GPS configuration and limits
#GPS_com_port = "COM198"
GPS_timeout = 120
