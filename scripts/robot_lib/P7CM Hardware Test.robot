*** Settings ***
Documentation     *P7CM Hardware test:*
...
...               Can be used as test for return devices.
...               Mostly prepared to test hardware functions and retreive data from P7CM.
Suite Setup       open data    ${test_output_directory}    ${UUT}    ${UUT}_data.xlsx
Suite Teardown    Close data
Library           ../../hat/drivers/MultimeterInstrument.py    ${Multimeter_address}    ${mult_config_file}    WITH NAME    Mult
Library           ../../hat/drivers/AcSourceInstrument.py    ${AcSource_address}    WITH NAME    AcSource
Library           ../../hat/drivers/AcPowerMeterInstrument.py    ${PM_address}    WITH NAME    PM
Variables         P7CM_test_limits.py    # list of limits for test evaluation
Library           ../../hat/utils/DataStorage.py    WITH NAME    data_utils
Library           ../../hat/comms/P7CM_FCT.py    ${Dali_controller_com}    WITH NAME    Dali_uart
Library           ../../hat/drivers/RelayInterface.py    ${relay_address}    ${relay_config}    WITH NAME    relay
Library           ../../hat/comms/P7CM.py    ${Zigbee_USB_Port}    ZIGBEE    ${UUT}    WITH NAME    p7cm
Library           ../../hat/comms/P7CM.py    ${P7CM_USB_Port}    SERIAL    ${UUT}    True    WITH NAME    p7cm_usb
Library           OperatingSystem
Library           Collections
Library           Dialogs
Library           String
Library           DateTime

*** Variables ***
${UUT}            0013A20041B472A5
${mult_config_file}    C:\\PythonCode\\hat\\scripts\\robot_lib\\mult_config.json    # Configuration file for multimeter
${test_output_directory}    C:\\TestResults\\P7CM
${relay_config}    C:\\PythonCode\\hat\\scripts\\robot_lib\\relay_config.json
${PM_address}     USB0::0xABCD::0x03E8::100008201733::INSTR    # Ac power meter address
${Multimeter_address}    USB0::0x05E6::0x6510::04472337::INSTR
${Dali_controller_com}    COM114
${AcSource_address}    ASRL40::INSTR    # Address as in visa for AC source
${relay_address}    COM133    # Serial port where the relay is connected
${Zigbee_USB_port}    COM2
${Copy_files_zigbee}    True
${Ac_voltage}     230    # Test voltage that will be used
${Ac_frequency}    50
@{RFID_DATA}      ${224}    ${4}    ${1}    ${0}    ${159}    ${90}    ${36}    ${201}    ${70}    ${67}    ${84}    ${48}    ${49}
${RFID_LEN}       13
${P7CM_USB_port}    COM2
${GPS_com_port}    COM135

*** Test Cases ***
Resistance
    [Documentation]    With P7CM device off, measure the resistance between the AC pins and between the Low voltage pins.
    Set Test Variable    ${all_pass}    ${true}
    relay.Set Position By Label    no_load
    relay.Set Position By Label    lsi_active
    log    Check resistance to about to start
    AcSource.Set Output State    False
    Sleep    10s    Lets the cap discharge
    relay.Set Position By Label    no_load
    Sleep    3s
    Check Resistance Step    LineOutToNeutral
    Check Resistance Step    LineInToNeutral
    Check Resistance Step    LineInToLineOut
    Check Resistance Step    LsiToGnd
    Check Resistance Step    DaliToGnd
    Check Resistance Step    PsuToGnd
    IF    not ${all_pass}
    Fail    Some value outside the range
    END

Voltage
    [Documentation]    Measure voltage on P7CM after startup without configurations in FCT mode.
    [Setup]    # data_utils.open | ${UUT}_voltage.xlsx | ${test_output_directory}
    Set Test Variable    ${all_pass}    ${true}
    relay.Set Position By Label    no_load
    Startup P7CM
    p7cm.Execute Command    dali 1
    ${ac_voltage}    PM.Get Voltage Rms
    Check Voltage Step    LineInToNeutral    ${ac_voltage}
    ${active_power}    PM.Get Active Power
    Check Voltage Step    PowerConsumption    ${active_power}
    ${LineInToNeutral_voltage}    Mult.Get Voltage Ac By Name    LineInToNeutral
    Check Voltage Step    LineInToNeutral    ${LineInToNeutral_voltage}
    ${voltage}    Mult.Get Voltage Dc By Name    DaliToGnd
    Check Voltage Step    DaliToGnd    ${voltage}
    ${voltage}    Mult.Get Voltage Dc By Name    LsiToGnd
    Check Voltage Step    LsiToGnd    ${voltage}
    ${voltage}    Mult.Get Voltage Dc By Name    PsuToGnd
    Check Voltage Step    PsuToGnd    ${voltage}
    IF    not ${all_pass}
    Fail    Some value outside the range
    END
    [Teardown]

Zigbee
    [Documentation]    Check communication zigbee.
    [Setup]
    Complete Startup P7CM
    ${node_discover}    p7cm.Discover Node    ${UUT}
    data_utils.Add Item    Parameter    Device on ZB Network?    Value    ${node_discover}    Pass/Fail    ${node_discover}    new_line=True
    Should Be True    ${node_discover}    Device ${UUT} should be on network
    [Teardown]

Zigbee At configuration
    [Documentation]    Check Zigbee AT configuration.
    ...
    ...    To read AT configuration over zigbee is not necessary to have the owlet application running.
    [Setup]
    Set Test Variable    ${all_pass}    ${true}
    Startup P7CM
    FOR    ${at}    IN    @{zigbee_ats}
        Log    ${at}
        ${value}    p7cm.Get AT Parameter    ${at}[AT]    ${at}[type]
        data_utils.Add Item    Parameter    ${at}[Description]    Value    ${value}
        Set Test Variable    ${step_pass}    ${true}
        IF    ${at}[check] is not None
        ${value_hex}    Convert To Hex    ${at}[check]    prefix=0x    lowercase=True
        data_utils.Add Item    Max    ${value_hex}
        IF    ${value} != ${at}[check]
        Log    Zigbee item: ${at}[AT], ${at}[Description]: \ Expected: ${at}[check] != Read: ${value}    Warn
        Set Test Variable    ${step_pass}    ${false}
        Set Test Variable    ${all_pass}    ${false}
    END
    END
    data_utils.Add Item    Pass/Fail    ${step_pass}    new_line=True
    END
    IF    not ${all_pass}
    Fail    Some value outside the range, check log
    END
    [Teardown]

Inventory
    [Documentation]    Get data from P7CM.
    ...
    ...    Try to copy all the files from the memory.
    ...
    ...    Get AT parameters from zigbee.
    ...
    ...    Get At parameters from Gemalto.
    ...
    ...    Get some datapoints.
    ...
    ...    Note:
    ...    It is not possible to read some Datapoints in FCT mode.
    ...    It may require to remove RFID tag and repeat test.
    Startup P7CM
    Start Dali uart
    ${mcu_version}    Dali_uart.Get Mcu Version
    data_utils.Add Item    Parameter    MCU version    Value    ${mcu_version}    new_line=True
    ${MCU bootloader}    Dali_uart.get_mcu_bootloader_version
    data_utils.Add Item    Parameter    MCU bootloader    Value    ${MCU bootloader}    new_line=True
    ${mpu_version}    Dali_uart.Get Mpu Version
    data_utils.Add Item    Parameter    MPU version    Value    ${mpu_version}    new_line=True
    ${product_key}    Dali_uart.get_product_key
    data_utils.Add Item    Parameter    Product key    Value    ${product_key}    new_line=True
    ${production_serial_number}    Dali_uart.get_production_serial_number
    data_utils.Add Item    Parameter    Product Serial Number    Value    ${production_serial_number}    new_line=True
    ${gemalto_version}    Dali_uart.get_gemalto_version
    data_utils.Add Item    Parameter    Gemalto version    Value    ${gemalto_version}    new_line=True
    ${imei}    Dali_uart.get_imei
    data_utils.Add Item    Parameter    IMEI    Value    ${imei}    new_line=True
    ${imsi}    Dali_uart.get_imsi
    data_utils.Add Item    Parameter    IMSI    Value    ${imsi}    new_line=True
    ${xbee_version}    Dali_uart.get_xbee_version
    data_utils.Add Item    Parameter    XBEE version    Value    ${xbee_version}    new_line=True
    ${eui}    Dali_uart.get_eui
    data_utils.Add Item    Parameter    EUI    Value    ${eui}    new_line=True
    ${res}    p7cm.get_global_storage_stats
    data_utils.Add Item    Parameter    Global space    Value    ${res}[storage]    new_line=True
    data_utils.Add Item    Parameter    Global free space    Value    ${res}[freespace]    new_line=True
    [Teardown]

Gemalto At configuration
    [Documentation]    Take configurations from Gemalto.
    [Setup]
    Startup P7CM
    ${gemalto_configs}    p7cm.Get Gemalto At SCFG
    FOR    ${config}    IN    @{gemalto_configs}
        data_utils.Add Item    Parameter    ${config}[Description]    Value    ${config}[Value]    new_line=True
        Log    ${config}
    END
    log    ${gemalto_configs}
    FOR    ${at}    IN    @{gemalto_ats}
        ${value}    p7cm.Execute At    ${at}[AT]
        data_utils.Add Item    Parameter    ${at}[Description]    Value    ${value}    new_line=True
    END
    [Teardown]

Get Data from Flash
    [Documentation]    Copy files from File system
    [Timeout]    5 minutes
    Startup P7CM
    Log    Copy files from Gemalto
    ${path} =    Join Path    ${output_directory}    Luco_data
    Create Directory    ${path}
    p7cm_usb.Copy Files    ${path}
    data_utils.Add Item    Parameter    Files    Value    ${path}    new_line=True

RFID
    [Documentation]    Check functionality of reading RFID tag.
    ...
    ...    It checks the read RFID EUI with expected data from tester.
    [Timeout]    1 minute
    Startup P7CM
    Start Dali uart
    ${rfid}    Dali_uart.Get Rfid Data
    ${read_rfid_data}    Get Slice From List    ${rfid}    0    ${RFID_LEN}
    ${expected_str}    Evaluate    " ".join(map(hex,${RFID_DATA}))
    ${read_str}    Evaluate    " ".join(map(hex,${read_rfid_data}))
    Log List    ${read_rfid_data}
    Lists Should Be Equal    ${read_rfid_data}    ${RFID_DATA}
    data_utils.Add Item    Parameter    RFID data    Max    ${expected_str}    Value    ${read_str}    Pass/Fail    ${true}    new_line=True
    [Teardown]

Relay test
    [Documentation]    Test relay function.
    ...
    ...    Modification triggered by Dali UART.
    ...
    ...    Measure voltage across relay.
    ...
    ...    Measure resistance of relay (may require 4 wire measurement.
    Set Test Variable    ${all_pass}    ${true}
    Startup P7CM
    Start Dali uart
    # Test relay off
    Dali_uart.Switch Relay    False    True
    Sleep    5    Wait switching
    ${LineOutToNeutral}    Mult.Get Voltage Ac By Name    LineOutToNeutral
    Check Voltage Step    LineOutToNeutralRelayOff    ${LineOutToNeutral}
    # Test relay on
    Dali_uart.Switch Relay    True    True
    Sleep    5    Wait switching
    ${LineOutToNeutral}    Mult.Get Voltage Ac By Name    LineOutToNeutral
    Check Voltage Step    LineOutToNeutralRelayOn    ${LineOutToNeutral}
    IF    not ${all_pass}
    Fail    Some value outside the range
    data_utils.Add Item    Parameter    Relay    Pass/Fail    False    new_line=True
    END
    [Teardown]

Sensor input
    [Documentation]    Check behaviour of sensor input.
    ...
    ...    Use relay to change value of sensor input.
    Startup P7CM
    Start Dali uart
    Dali_uart.set_sensor_input_test    true
    Sleep    1
    relay.Set Position By Label    lsi_active
    Sleep    2
    Execute manual Step    Is LED active (turn on)?
    relay.Set Position By Label    lsi_inactive
    Sleep    2
    Execute manual Step    Is LED inactive (turn off)?
    relay.Set Position By Label    lsi_active
    Sleep    2
    Execute manual Step    Is LED active (turn on)?
    data_utils.Add Item    Parameter    Sensor input    Value    Ok    new_line=True
    Dali_uart.set_sensor_input_test    false
    [Teardown]

Photocell
    [Documentation]    Test photocell behaviour.
    ...
    ...    It is necessary to have a controlled LED by software.
    ...    It is necessary to have a fixture/box to block direct light from ambient.
    ...
    ...    Read photocell value from MCU or using MPU commands.
    Startup P7CM
    Start Dali uart
    Pause Execution    Cover the photocell. Press ok to continue.
    ${photocell_dark}    Dali_uart.Get Photocell
    data_utils.Add Item    Parameter    Photocell at Dark    Value    ${photocell_dark}    Pass/Fail    ${${photocell_dark} < ${Photocell_Max_value_at_dark }}    new_line=True
    Pause Execution    Take cover from the photocell. Press ok to continue.
    ${photocell_light}    Dali_uart.Get Photocell
    data_utils.Add Item    Parameter    Photocell at Light    Value    ${photocell_light}    Pass/Fail    ${${photocell_light} > ${Photocell_Min_value_at_light}}    new_line=True
    Run Keyword If    ${photocell_dark} > ${Photocell_Max_value_at_dark }    Fail    Photocell value measure: ${photocell_dark} is bigger than maximum value at dark: ${Photocell_Max_value_at_dark }
    Run Keyword If    ${photocell_light} < ${Photocell_Min_value_at_light}    Fail    Photocell value measure: ${photocell_light} is less than minimum value at light: ${Photocell_Min_value_at_light}
    [Teardown]

Dali init
    [Documentation]    Test Dali interface.
    ...
    ...    Execute Dali reinit.
    ...
    ...    Confirm the dimming level with the powermeter.
    relay.Set Position By Label    dali_power
    relay.Set Position By Label    dali_led
    Complete Startup P7CM
    relay.Set Position By Label    uart
    sleep    1    Wait for Tester relay switch
    Dali_uart.Switch Relay    True    True
    sleep    3    Wait for Luco relay switch
    relay.Set Position By Label    dali
    p7cm.Set Dali Fct Mode    False
    Sleep    5
    ${n_drivers}    p7cm.perform_dali_reinit
    Log    ${n_drivers}
    ${pass}    Evaluate    ${n_drivers} == ${1}
    data_utils.Add Item    Parameter    Dali Init    Max    1    Value    ${n_drivers}    Pass/Fail    ${pass}    new_line=True
    sleep    10    Wait finishing of dali commissioning
    Should Be Equal As Integers    ${n_drivers}    ${1}
    ${dimming}=    Set Variable    254
    p7cm.set_dali_dimming_fct    1    ${dimming}
    sleep    10
    ${dimming}=    Set Variable    170
    p7cm.set_dali_dimming_fct    1    ${dimming}
    [Teardown]

1-10V dimming
    [Documentation]    Test 1-10V dimming.
    ...
    ...    Confirm in the Dimming interface with multimeter.
    ...
    ...    Confirm with powermeter that power consume follows the dimming.
    Set Test Variable    ${all_pass}    ${true}
    relay.Set Position By Label    1-10v_power
    relay.Set Position By Label    1-10v_led
    Complete Startup P7CM
    relay.Set Position By Label    uart
    sleep    1    Wait for Tester relay switch
    Dali_uart.Switch Relay    True    True
    sleep    3    Wait for Luco relay switch
    relay.Set Position By Label    1-10v
    p7cm.Set Dali Fct Mode    False
    Sleep    5
    Check 1-10V setting    100
    Check 1-10V setting    20
    IF    not ${all_pass}
    Fail    Some value outside the range
    END

GPS
    [Documentation]    Test GPS functionality.
    ...
    ...    *TODO:* check if GPS is initialized during FCT mode.
    ...
    ...    Test requires to have GPS signal available.
    Startup P7CM
    ${data}    p7cm.get_gps_position_fct    ${GPS_com_port}    ${GPS_timeout}
    Log    ${data}
    data_utils.Add Item    Parameter    GPS fix    Pass/Fail    ${data}[fix]    new_line=True
    Run Keyword Unless    ${data}[fix]    Fail    Could not get a fix in ${GPS_timeout}
    [Teardown]

Network registration
    [Documentation]    This test tries to register in a network.
    ...
    ...    This takes a bit of time to connect up to 2 minutes.
    Startup P7CM
    p7cm.Set At Full
    p7cm.Force Network Search
    ${network}    p7cm.get_at_network
    log    ${network}
    ${creg}    p7cm.get_at_creg
    log    ${creg}
    ${cgreg}    p7cm.get_at_greg
    log    ${cgreg}
    data_utils.Add Item    Parameter    Network reg [CReg]    Value    ${creg}[status]    Pass/Fail    ${creg}[registered]    new_line=True
    data_utils.Add Item    Parameter    Network reg [CGeg]    Value    ${cgreg}[status]    Pass/Fail    ${cgreg}[registered]    new_line=True
    Should Contain    ${creg}[status]    Registered    Not registered in GSM
    Should Contain    ${cgreg}[status]    Registered    Not registered in Data
    [Teardown]

*** Keywords ***
Open data
    [Arguments]    ${directory}    ${folder}    ${filename}
    [Documentation]    Open file to save all data from tests.
    ...
    ...    Used as Test Suite Setup Keyword,
    ${date} =    Get Current Date
    ${date_formatted} =    Convert Date    ${date}    result_format=%Y%m%d_%H%M    exclude_millis=yes
    ${path} =    Join Path    ${directory}    ${folder}    ${date_formatted}
    Log    Directory + ${path}
    Create Directory    ${path}
    data_utils.open    ${filename}    ${path}
    Set Suite Variable    ${output_directory}    ${path}
    Log    Checking instruments
    AcSource.Open
    PM.Open
    Mult.Open
    p7cm.Open
    Comment    AcSource.Close
    Comment    PM.Close
    Comment    Mult.Close
    Comment    p7cm.Close
    Log    Finish initialization

Check Resistance Step
    [Arguments]    ${signal_name}
    [Documentation]    Helper keyword for measure resistance test.
    ${resistance}    Mult.get resistance by name    ${signal_name}
    Log    Signal: ${signal_name}, resistance: ${resistance} ohm.
    ${min}    Set Variable    ${resistance_limits.${signal_name}.min}
    ${max}    Set Variable    ${resistance_limits.${signal_name}.max}
    data_utils.add_item    Parameter    Resistance ${signal_name}
    data_utils.add_item    Min    ${min}
    data_utils.add_item    Max    ${max}
    data_utils.add_item    Value    ${resistance}
    Set Test Variable    ${step_pass}    ${true}
    IF    ${resistance} > ${max}
    Set Test Variable    ${step_pass}    ${false}
    Log    ${signal_name} resistance higher than max ${max}    Warn
    END
    IF    ${resistance} < ${min}
    Set Test Variable    ${step_pass}    ${false}
    Log    ${signal_name} resistance lower than min ${min}    Warn
    END
    data_utils.add_item    Pass/Fail    ${step_pass}
    Run Keyword Unless    ${step_pass}    Set Test Variable    ${all_pass}    False
    data_utils.Start Next Test

Check Voltage Step
    [Arguments]    ${signal_name}    ${voltage}
    [Documentation]    Helper function to measure voltage step.
    ${min}    Set Variable    ${voltage_limits.${signal_name}.min}
    ${max}    Set Variable    ${voltage_limits.${signal_name}.max}
    data_utils.Add Item    Parameter    ${signal_name}
    data_utils.Add Item    Min    ${min}
    data_utils.Add Item    Max    ${max}
    data_utils.Add Item    Value    ${voltage}
    Set Test Variable    ${step_pass}    ${true}
    IF    ${voltage} > ${max}
    Set Test Variable    ${step_pass}    ${false}
    Log    ${signal_name} voltage higher than max ${max}    Warn
    END
    IF    ${voltage} < ${min}
    Set Test Variable    ${step_pass}    ${false}
    Log    ${signal_name} voltage lower than min ${min}    Warn
    END
    data_utils.add_item    Pass/Fail    ${step_pass}
    Run Keyword Unless    ${step_pass}    Set Test Variable    ${all_pass}    False
    data_utils.Start Next Test

Close data
    [Documentation]    Close data and save values to a file.
    ...
    ...    Use as Test suite teardown.
    data_utils.Close

Start Dali uart
    [Documentation]    Start Dali Uart
    relay.Set Position By Label    uart
    Comment    Workarround for devices that don't have Dali power supply enabled in FCT. this requires Zigbee to be working.
    Comment    p7cm.Execute Command    dali 1
    sleep    2
    Comment    End of workarround

Startup P7CM
    [Documentation]    Start P7CM.
    ...    Wait until it is ready.
    ...
    ...    On future, it can be used to make tests faster by using some sort of state.
    ${state}    AcSource.Get Output State
    IF    not ${state}
    Log    Turning ON AC source
    AcSource.Set Ac Configuration    ${Ac_voltage}    ${Ac_frequency}    True
    Sleep    25
    ELSE
    Log    AC source is already ON
    END

Complete Startup P7CM
    [Documentation]    Start P7CM.
    ...    Wait until it is ready.
    ...
    ...    On future, it can be used to make tests faster by using some sort of state.
    AcSource.Set Output State    False
    Sleep    10
    AcSource.Set Ac Configuration    ${Ac_voltage}    ${Ac_frequency}    True
    Sleep    25

Check 1-10V setting
    [Arguments]    ${setting}
    ${min}    Set Variable    ${voltage_limits.Set_1_10v_${setting}.min}
    ${max}    Set Variable    ${voltage_limits.Set_1_10v_${setting}.max}
    p7cm.Set 1 10v Dimming Fct    ${setting}
    sleep    10
    ${feedback}    p7cm.get_1_10v_feedback_fct
    Log    ${feedback}
    ${voltage}    Mult.Get Voltage Dc By Name    DaliToGnd
    Set Test Variable    ${step_pass}    ${true}
    IF    ${voltage} > ${max}
    Set Test Variable    ${step_pass}    ${false}
    Log    1-10V at ${setting}% voltage higher than max ${max}    Warn
    END
    IF    ${voltage} < ${min}
    Set Test Variable    ${step_pass}    ${false}
    Log    1-10V at ${setting}% \ voltage lower than min ${min}    Warn
    END
    data_utils.Add Item    Parameter    1-10V ${setting}% V
    data_utils.Add Item    Min    ${min}
    data_utils.Add Item    Max    ${max}
    data_utils.Add Item    Value    ${voltage}
    data_utils.add_item    Pass/Fail    ${step_pass}
    Run Keyword Unless    ${step_pass}    Set Test Variable    ${all_pass}    False
    data_utils.Start Next Test
    ${diff}    Evaluate    ${voltage} - ${feedback}
    ${min}    Set Variable    ${voltage_limits.Feedback_1_10v_diff.min}
    ${max}    Set Variable    ${voltage_limits.Feedback_1_10v_diff.max}
    IF    ${diff} > ${max}
    Set Test Variable    ${step_pass}    ${false}
    Log    Diff at 1-10V at ${setting}% voltage higher than max ${max}    Warn
    END
    IF    ${diff} < ${min}
    Set Test Variable    ${step_pass}    ${false}
    Log    Diff at 1-10V at ${setting}% \ voltage lower than min ${min}    Warn
    END
    data_utils.Add Item    Parameter    Feedback diff @ 1-10V ${setting}% V
    data_utils.Add Item    Min    ${min}
    data_utils.Add Item    Max    ${max}
    data_utils.Add Item    Value    ${diff}
    data_utils.add_item    Pass/Fail    ${step_pass}
    Run Keyword Unless    ${step_pass}    Set Test Variable    ${all_pass}    False
    data_utils.Start Next Test
